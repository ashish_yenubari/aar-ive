var isChecked = false;
function enableSubmitButton() {
   isChecked = document.getElementById('check1').checked;
}

function colorToggle () {
    document.getElementById('saveData').className = 'button';
}

function highlightSubmit () {
    document.getElementById('saveData').className = 'selected button';
}

function submitData() {
    var data = JSON.stringify({
        "prefix": document.getElementById("prefix").value,
        "firstName": document.getElementById("firstName").value,
        "lastName": document.getElementById("lastName").value,
        "jobTitle": document.getElementById("jobTitle").value,
        "organisation": document.getElementById("organisation").value,
        "primaryPhoneType": document.getElementById("primaryPhoneType").value,
        "primaryPhone": document.getElementById("primaryPhone").value,
        "altPrimaryPhoneType": document.getElementById("altPrimaryPhoneType").value,
        "altPrimaryPhone": document.getElementById("altPrimaryPhone").value,
        "email": document.getElementById("email").value,
        "address1": document.getElementById("address1").value,
        "address2": document.getElementById("address2").value,
        "address3": document.getElementById("address3").value,
        "address4": document.getElementById("address4").value,
        "city": document.getElementById("city").value,
        "state": document.getElementById("state").value,
        "postal": document.getElementById("postal").value,
        "country": document.getElementById("country").value
    });
    
    var fields = document.getElementsByClassName("reg");
    var arr = [];
    for( var i=0; i < fields.length; i++) {
        var field = fields[i];
        if ((field.attributes.required && field.value.trim().length === 0) || isChecked == false) {
            arr.push("Enter required fields.");
        }
        else if (field.value.trim().length !== 0) {
            if (field.attributes.isPhone && !field.value.match(/\d{3}(?:[\.\-]?\d)\d{4,}/)) {
                arr.push("Enter minimum 8 digits in "+field.name);
            } else if (field.attributes.isPhone && field.value.match(/\d{5}(?:[\.\-]?\d)\d{5}/)) {
                arr.push("Enter maximum 10 digits in "+field.name);
            } else if (field.attributes.isCaptcha && field.value.trim().toUpperCase() != textToCompare) {
              arr.push("Please provide valid captcha code");
            }
        }
        if (field.attributes.isEmail && !field.value.match(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)) {
            arr.push("Enter valid email address.");
        }
        if (field.value.trim().length !== 0) {
            if (field.attributes.isChar && !field.value.match(/^[A-Za-z ]+$/)) {
                arr.push(field.name+" must contain only characters.");
            }
        } 
    }
    if(arr.length > 0) {
        document.getElementById("popupContent").innerHTML = arr[0];
        document.getElementById("regMsgBox").style.display = "block";
    } else {
        document.getElementById("regMsgBox").style.display = "none";
        document.getElementById('saveData').style.pointerEvents="none";
        document.getElementById('saveData').style.cursor="default";
        waitingDialog.show('Please wait...');

        var xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
                if(xhr.status == '200') {
                    clearData();
                    waitingDialog.hide('Please wait...');
                    document.getElementById("msgIcon").className = "fa fa-check";
                    document.getElementById("popupTitle").innerHTML = "Thank You for registering";
                    document.getElementById("popupContent").innerHTML = "You will receive an email within 5 days with your credentials to sign into AARive portal.";
                    document.getElementById("regMsgBox").style.display = "block";
                    var elem = document.getElementById("closePopup");
                    elem.addEventListener("click",function() {
                        window.location = window.location.protocol + "//" + document.location.hostname;});
                }else{
                    waitingDialog.hide('Please wait...');
                    document.getElementById("msgIcon").className = "fa fa-times";
                    document.getElementById("popupTitle").innerHTML = "Error!";
                    document.getElementById("popupContent").innerHTML = "Registration Failed";
                    document.getElementById("regMsgBox").style.display = "block";
                    var elem = document.getElementById("closePopup");
                    elem.addEventListener("click",function() {
                        document.getElementById("regMsgBox").style.display = "none";
                    });
                    document.getElementById('saveData').style.pointerEvents="auto";
                    document.getElementById('saveData').style.cursor="pointer";
                }
            }
        });

        xhr.open("POST", "ws/api/v1/register");
        xhr.setRequestHeader("content-type", "application/json");
        xhr.setRequestHeader("cache-control", "no-cache");
        xhr.send(data);
    }
}

function clearData() {
    var clearPart = document.getElementsByClassName("reg");
    for( var i=0; i < clearPart.length; i++) {
        clearPart[i].value = "";
    }
    document.getElementById("check1").checked = false;
}

$(function() {
  $('#defaultReal').realperson();
});