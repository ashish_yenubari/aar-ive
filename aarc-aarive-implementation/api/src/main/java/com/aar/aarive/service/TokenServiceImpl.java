package com.aar.aarive.service;

import java.text.ParseException;

import org.jose4j.jwk.HttpsJwks;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.resolvers.HttpsJwksVerificationKeyResolver;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import com.aar.aarive.constant.Constants;
import com.aar.aarive.service.model.JwtUserProfile;
import com.nimbusds.jwt.JWTParser;

@Service()
public class TokenServiceImpl implements TokenService {

  @Override
  public JwtUserProfile getUserFromToken(String token) throws ParseException {
    // Parse Token
    JwtUserProfile profile = AzureJwtTokenHandler
        .convertClaimsToUser(JWTParser.parse(token).getJWTClaimsSet().getAllClaims());
    profile.setToken(token);
    return profile;
  }

  @Override
  public void validateToken(String token) throws BadCredentialsException, InvalidJwtException {
    HttpsJwksVerificationKeyResolver httpsJwksKeyResolver =
        new HttpsJwksVerificationKeyResolver(new HttpsJwks(Constants.JSON_WEB_KEY));
    JwtConsumer jwtConsumer = new JwtConsumerBuilder().setRequireExpirationTime()
        .setAllowedClockSkewInSeconds(Constants.CLOCK_SKEW).setRequireSubject()
        .setExpectedIssuer(Constants.JWT_ISSUER).setExpectedAudience(Constants.JWT_AUDIENCE)
        .setVerificationKeyResolver(httpsJwksKeyResolver).build();
    jwtConsumer.processToClaims(token);
  }
}
