package com.aar.aarive.service;

import java.text.ParseException;

import org.jose4j.jwt.consumer.InvalidJwtException;
import org.springframework.security.authentication.BadCredentialsException;

import com.aar.aarive.service.model.JwtUserProfile;

public interface TokenService {

  void validateToken(String token)
      throws BadCredentialsException, ParseException, InvalidJwtException;

  JwtUserProfile getUserFromToken(String token) throws ParseException;
}
