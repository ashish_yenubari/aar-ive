
package com.aar.aarive.spring;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import com.aar.aarive.constant.Constants;
import com.aar.aarive.service.MediaDownloadService;
import com.aar.aarive.service.MediaUploadService;
import com.aar.aarive.service.media.CloudDownloadService;
import com.aar.aarive.service.media.CloudUploadService;
import com.aar.aarive.service.media.GoogleDownloadService;
import com.aar.aarive.service.media.GoogleUploadService;
import com.aar.aarive.service.media.LocalDownloadService;
import com.aar.aarive.service.media.LocalUploadService;
import com.aar.aarive.service.media.config.CloudStorageCondition;
import com.aar.aarive.service.media.config.GoogleStorageCondition;
import com.aar.aarive.service.media.config.LocalStorageCondition;

@Configuration
@EnableWebMvc
@EnableAsync
@ComponentScan(basePackages = {"com.aar.aarive"})
@PropertySources({@PropertySource("classpath:mail.properties"),
    @PropertySource("classpath:config.properties")})

public class AppConfig extends WebMvcConfigurerAdapter {
  @Value("${mail.host}")
  private String host;
  @Value("${mail.port}")
  private int port;
  @Value("${mail.username}")
  private String username;
  @Value("${mail.password}")
  private String password;
  @Value("${mail.smtp.starttls.enable}")
  private String starttls;
  @Value("${mail.smtp.auth}")
  private String auth;
  @Value("${mail.transport.protocol}")
  private String transProtocol;

  // ========= Overrides ===========

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new LocaleChangeInterceptor());
  }

  // ========= Beans ===========

  @Bean(name = "localeResolver")
  public LocaleResolver getLocaleResolver() {
    return new CookieLocaleResolver();
  }

  @Bean(name = "messageSource")
  public MessageSource getMessageSources() {
    ReloadableResourceBundleMessageSource messageSource =
        new ReloadableResourceBundleMessageSource();
    messageSource.setBasenames("/WEB-INF/resources/properties/clientMessages");
    messageSource.setCacheSeconds(0);

    return messageSource;
  }

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
    return new PropertySourcesPlaceholderConfigurer();
  }
  /*
   * use MultipartResolver to handle MultiPart requests
   */

  @Bean
  MultipartResolver multipartResolver() {
    CommonsMultipartResolver resolver = new CommonsMultipartResolver();
    resolver.setMaxInMemorySize(Constants.MAX_IN_MEMORY_SIZE);
    return resolver;
  }

  /*
   * based on our conditional classes LocalStorageCondition, GoogleStorageCondition,
   * CloudStorageCondition which reads property file config.properties it gives only that specific
   * bean
   */
  @Bean(name = "mediaUploadService")
  @Conditional(LocalStorageCondition.class)
  public MediaUploadService getLocalMediaStorage() {
    return new LocalUploadService();
  }

  @Bean(name = "mediaUploadService")
  @Conditional(GoogleStorageCondition.class)
  public MediaUploadService getGoogleMediaStorage() {
    return new GoogleUploadService();
  }

  @Bean(name = "mediaUploadService")
  @Conditional(CloudStorageCondition.class)
  public MediaUploadService getCloudMediaService() {
    return new CloudUploadService();
  }

  @Bean(name = "mediaDownloadService")
  @Conditional(LocalStorageCondition.class)
  public MediaDownloadService getLocalMediaService() {
    return new LocalDownloadService();
  }

  @Bean(name = "mediaDownloadService")
  @Conditional(GoogleStorageCondition.class)
  public MediaDownloadService getGoogleMediaService() {
    return new GoogleDownloadService();
  }

  @Bean(name = "mediaDownloadService")
  @Conditional(CloudStorageCondition.class)
  public MediaDownloadService getCloudMediaStorage() {
    return new CloudDownloadService();
  }

  @Bean(name = "javaMailSender")
  public JavaMailSender getJavaMailSender() {
    JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
    javaMailSenderImpl.setHost(host);
    javaMailSenderImpl.setPort(port);
    javaMailSenderImpl.setUsername(username);
    javaMailSenderImpl.setPassword(password);
    Properties javaMailProperties = new Properties();
    javaMailProperties.put("mail.smtp.auth", auth);
    javaMailProperties.put("mail.smtp.starttls.enable", starttls);
    javaMailSenderImpl.setJavaMailProperties(javaMailProperties);
    return javaMailSenderImpl;
  }

  @Bean(name = "taskExecutor")
  public ThreadPoolTaskExecutor getThreadPoolTaskExecutor() {
    ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
    taskExecutor.setCorePoolSize(Constants.CORE_POOL_SIZE);
    taskExecutor.setMaxPoolSize(Constants.MAX_POOL_SIZE);
    taskExecutor.setQueueCapacity(Constants.QUEUE_CAPACITY);
    taskExecutor.setWaitForTasksToCompleteOnShutdown(true);
    return taskExecutor;
  }
}
