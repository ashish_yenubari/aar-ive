package com.aar.aarive.service.media;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.aar.aarive.service.MediaDownloadService;

@Service
@PropertySource("classpath:config.properties")
public class LocalDownloadService implements MediaDownloadService {

  /*
   * sourcePath is the location from where you can download the content. Based on config properties
   * we read sourcePath which is configurable
   */
  @Value("${download.sourcePath}")
  private String sourcePath;

  // this method returns a file if the file exist in specified location
  @Override
  public File mediaDownload(String fileName) throws IOException {
    File file = new File(sourcePath + fileName);
    if (!file.exists()) {
      throw new IOException("file doesnt exist:" + fileName);
    }
    return file;
  }

  /*
   * setting the httpheaders properties like specifying Mimetype and content-disposition
   */
  @Override
  public HttpHeaders getHeaderResp(String fileName) {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add("Content-Type", URLConnection.guessContentTypeFromName(fileName));
    httpHeaders.add("content-disposition", "attachment; filename=" + fileName);
    return httpHeaders;
  }

}
