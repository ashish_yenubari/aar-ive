package com.aar.aarive.service.media;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.aar.aarive.constant.Constants;
import com.aar.aarive.service.MediaUploadService;

/* this is a service which is specifically responsible to upload a file to local storage */
@Service
@PropertySource("classpath:config.properties")
public class LocalUploadService implements MediaUploadService {

  @Value("${config.upload.path}")
  private String path;

  /*
   * we save the file to a location based on helper class which returns Boolean value that is true
   * if file is saved and false if there is some exception
   */
  @Override
  public ResponseEntity<?> mediaUpload(MultipartFile file)
      throws IllegalStateException, IOException {
    if (path == null) {
      throw new IllegalStateException("Upload location cant be null");
    }
    File uploadDir = new File(path);
    if (!uploadDir.exists()) {
      boolean mkdir = uploadDir.mkdirs();
      if (!mkdir) {
        throw new IllegalStateException("Unable to create" + path);
      }
    }
    file.transferTo(new File(path + file.getOriginalFilename()));
    return new ResponseEntity<>(Constants.MEDIA_RESPONSE_SUCCESS, HttpStatus.OK);
  }
}
