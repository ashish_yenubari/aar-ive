package com.aar.aarive.service.model;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.aar.aarive.spring.security.AARiveUserAuthority;

public class JwtUserProfile {

  private String token;
  private String name;
  private String username;
  private String onPremId;
  private ArrayList<AARiveUserAuthority> authorities;

  public void setToken(String token) {
    this.token = token;
  }

  public String getToken() {
    return this.token;
  }

  public Collection<? extends GrantedAuthority> getAuthorities() {
    return this.authorities;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getUsername() {
    return this.username;
  }

  public void setOnPremId(String onPremId) {
    this.onPremId = onPremId;
  }

  public String getOnPremId() {
    return this.onPremId;
  }

  public void addAuthority(String role) {
    if (authorities == null) {
      authorities = new ArrayList<AARiveUserAuthority>();
    }
    authorities.add(new AARiveUserAuthority(role));
  }
}
