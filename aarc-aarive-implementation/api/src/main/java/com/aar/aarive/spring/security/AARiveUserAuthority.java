package com.aar.aarive.spring.security;

import org.springframework.security.core.GrantedAuthority;

public class AARiveUserAuthority implements GrantedAuthority {

  public AARiveUserAuthority(String authority) {
    setAuthority(authority);
  }

  private String authority;

  @Override
  public String getAuthority() {
    return authority;
  }

  public void setAuthority(String authority) {
    this.authority = authority;
  }

}
