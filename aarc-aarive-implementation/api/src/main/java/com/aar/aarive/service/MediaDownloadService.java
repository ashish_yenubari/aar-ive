package com.aar.aarive.service;

import java.io.File;
import java.io.IOException;

import org.springframework.http.HttpHeaders;

/*
 * MediaDownloadService is a base service with multiple implementations based on config.properties
 * we autowire this to the specified implementation
 */
public interface MediaDownloadService {
  public File mediaDownload(String fileName) throws IOException;

  public HttpHeaders getHeaderResp(String fileName);
}
