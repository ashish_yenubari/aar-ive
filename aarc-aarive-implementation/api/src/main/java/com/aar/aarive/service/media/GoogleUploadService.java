package com.aar.aarive.service.media;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.aar.aarive.constant.Constants;
import com.aar.aarive.service.MediaUploadService;

/*
 * this is a service which is specifically responsible to upload a file to Google Drive
 */
@Service
public class GoogleUploadService implements MediaUploadService {
  /* this method is specifically responsible to save the uploaded file in specified location */
  @Override
  public ResponseEntity<?> mediaUpload(MultipartFile file) {
    // logic to upload to a Google Drive
    return new ResponseEntity<>(Constants.MEDIA_RESPONSE_SUCCESS, HttpStatus.OK);
  }

}
