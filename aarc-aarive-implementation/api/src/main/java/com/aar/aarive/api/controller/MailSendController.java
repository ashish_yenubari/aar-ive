package com.aar.aarive.api.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aar.aarive.api.service.model.MailData;
import com.aar.aarive.constant.Constants;
import com.aar.aarive.service.MailService;
import com.aar.aarive.service.ValidateMailData;

/*
 * this controller is specifically used for mailing service
 */
@RestController
public class MailSendController {

  @Autowired
  private MailService mailService;

  @Autowired
  private ValidateMailData validateMailData;

  private static Logger log = LoggerFactory.getLogger(MailSendController.class);

  /*
   * this method is specifically responsible to forward the obtained Json object via E-mail
   */

  @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = "application/json",
      produces = "application/json")
  public ResponseEntity<?> emailUserRegistration(@RequestBody String data) {
    try {
      mailService.sendMail(data);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(Constants.MAIL_RESPONSE_SUCCESS, HttpStatus.OK);
  }

  @PreAuthorize("@authenticator.getAuth(isAuthenticated())")
  @RequestMapping(value = "/mail/send", method = RequestMethod.POST,
      consumes = {"multipart/mixed", "multipart/form-data"}, produces = "application/json")
  public ResponseEntity<?> sendEmail(@Valid MailData mailData, BindingResult result) {
    if (result.hasErrors()) {
      log.error(result.getAllErrors().toString());
      return new ResponseEntity<>(Constants.INVALID_MAIL_INPUT, HttpStatus.EXPECTATION_FAILED);
    }
    MailData data = null;
    try {
      data = validateMailData.validate(mailData);
      if (data == null) {
        throw new Exception("Bad Request");
      }
      mailService.sendMail(data);
    } catch (Exception e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity<>(Constants.MAIL_RESPONSE_SUCCESS, HttpStatus.OK);
  }
}
