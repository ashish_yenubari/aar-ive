package com.aar.aarive.api.controller;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aar.aarive.constant.Constants;
import com.aar.aarive.service.MediaDownloadService;
import com.aar.aarive.service.MediaUploadService;

/* this controller is specifically responsible for uploading and downloading a file */
@RestController
public class MediaController {

  @Autowired
  private MediaUploadService mediaUploadService;

  @Autowired
  private MediaDownloadService mediaDownloadService;

  private static Logger log = LoggerFactory.getLogger(MediaController.class);

  /*
   * this method is specifically responsible for uploading file of various types using MultipartFile
   * Class
   */
  @RequestMapping(value = "/media", method = RequestMethod.POST, produces = "application/json")
  public ResponseEntity<?> mediaUpload(
      @RequestParam(value = "media", required = false) MultipartFile file) {
    try {
      mediaUploadService.mediaUpload(file);
      return new ResponseEntity<>(Constants.MEDIA_RESPONSE_SUCCESS, HttpStatus.OK);
    } catch (IllegalStateException | IOException e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(Constants.MEDIA_RESPONSE_FAILURE, HttpStatus.BAD_REQUEST);
    }
  }

  /*
   * this method is specifically responsible to download data of various format
   */
  @RequestMapping(value = "/media/{media:.+}", method = RequestMethod.GET)

  public ResponseEntity<?> fileDownload(@PathVariable("media") String fileName) {
    try {
      // return a file if the file exists
      File mediaDownload = mediaDownloadService.mediaDownload(fileName);
      // httpheaders specifies ContentType(mimetype) and
      // content-disposition
      HttpHeaders headerResp = mediaDownloadService.getHeaderResp(fileName);
      return new ResponseEntity<>(FileUtils.readFileToByteArray(mediaDownload), headerResp,
          HttpStatus.CREATED);
    } catch (IOException e) {
      log.error(e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }
}
