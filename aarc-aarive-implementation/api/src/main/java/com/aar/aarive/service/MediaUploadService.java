package com.aar.aarive.service;

import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

/*
 * MediaUploadService is a base service with multiple implementations based on config.properties we
 * autowire this to the specified implementation
 */
public interface MediaUploadService {
  public ResponseEntity<?> mediaUpload(MultipartFile file)
      throws IllegalStateException, IOException;
}
