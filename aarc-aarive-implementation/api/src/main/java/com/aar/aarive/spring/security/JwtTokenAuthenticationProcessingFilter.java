package com.aar.aarive.spring.security;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.aar.aarive.constant.Constants;
import com.aar.aarive.service.TokenService;
import com.aar.aarive.service.model.JwtUserProfile;

@Component
public class JwtTokenAuthenticationProcessingFilter extends GenericFilterBean {

  @Autowired
  private TokenService tokenAuthenticationService;

  private static Logger log = LoggerFactory.getLogger(JwtTokenAuthenticationProcessingFilter.class);

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest servletRequest = (HttpServletRequest) request;
    String token = servletRequest.getHeader(Constants.JWT_TOKEN_HEADER);
    if (token != null) {
      // Token Service to convert token to AuthenticationImpl ( TokenUserDetails)
      JwtUserProfile unAuthenticatedUser;
      try {
        unAuthenticatedUser = tokenAuthenticationService.getUserFromToken(token);
        UsernamePasswordAuthenticationToken unAuthenticatedUserToken =
            new UsernamePasswordAuthenticationToken(unAuthenticatedUser, null);
        SecurityContextHolder.getContext().setAuthentication(unAuthenticatedUserToken);
      } catch (ParseException e) {
        log.error(e.getMessage());
      }
    }
    chain.doFilter(request, response);
  }
}
