package com.aar.aarive.constant;

public class Constants {

  // === Security ===
  public static final String SECURITY_UNAUTH_ENTRY_POINT = "unauthorisedEntryPoint";
  public static final String SECURITY_STATELESS_AUTH_FILTER = "statelessAuthFilter";
  public static final String SECURITY_STATELESS_TOKEN_AUTH_FILTER = "statelessTokenAuthFilter";
  public static final String JWT_TOKEN_HEADER = "AUTH-TOKEN";

  // === Media ===
  public static final String MEDIA_RESPONSE_SUCCESS="SUCCESS"; 
  public static final String MEDIA_RESPONSE_FAILURE="FAILURE";
  
//=== Task Executor ===
 public static final Integer CORE_POOL_SIZE = 5;
 public static final Integer MAX_POOL_SIZE = 10;
 public static final Integer QUEUE_CAPACITY = 40;
  
  // === JWT AUTHENTICATION ===
  public static final String JSON_WEB_KEY="https://login.microsoftonline.com/aarcorp.com/discovery/v2.0/keys";
  public static final String JWT_ISSUER="https://sts.windows.net/72b16188-e1e4-4f24-a7d7-e9dee5f2265d/";
  public static final String JWT_AUDIENCE="9d0d6328-3799-4202-9e92-a69af561e8ab";
  public static final Integer CLOCK_SKEW=3600;
  
  // === Mail ===
  public static final String MAIL_RESPONSE_SUCCESS ="SUCCESS";
  public static final Integer MAX_IN_MEMORY_SIZE =1024*1024*30;
  public static final String  INVALID_MAIL_INPUT = "INVALID INPUT";
  public static final Long MAX_FILESIZE_LIMIT = 15000000L;

  // === Markers ===
  public static final String ALL = "all";
  
  // === Version ===
  public static final String VERSION_NAME = "r1";
}