package com.aar.aarive.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.aar.aarive.spring.security.JwtAuthenticationProvider;
import com.aar.aarive.spring.security.JwtTokenAuthenticationProcessingFilter;
import com.aar.aarive.spring.security.UnauthorisedEntryPoint;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan(basePackages = {"com.aar.aarive.service", "com.aar.aarive.spring.security"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private static final String LOGOUT_URL = "/auth/logout";
  private static final String LOGOUT_SUCCESS_URL = "/auth/logout/validate?status=success";

  @Autowired
  private UnauthorisedEntryPoint unauthorisedEntryPoint;

  @Autowired
  private JwtTokenAuthenticationProcessingFilter jwtAuthenticationProcessingFilter;

  @Autowired
  private JwtAuthenticationProvider jwtAuthenticationProvider;

  // ========= Overrides ===========

  @Override
  protected void configure(AuthenticationManagerBuilder auth) {
    auth.authenticationProvider(jwtAuthenticationProvider);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable().httpBasic().authenticationEntryPoint(unauthorisedEntryPoint).and()
        .authorizeRequests().antMatchers("/**").permitAll().and().sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().logout().logoutUrl(LOGOUT_URL)
        .logoutSuccessUrl(LOGOUT_SUCCESS_URL).invalidateHttpSession(true)
        .deleteCookies("JSESSIONID").and()
        .addFilterBefore(jwtAuthenticationProcessingFilter, BasicAuthenticationFilter.class);
  }

  // =========== Beans ============

  @Bean(name = "passwordEncoder")
  public PasswordEncoder getPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Override
  @Bean
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }
}
