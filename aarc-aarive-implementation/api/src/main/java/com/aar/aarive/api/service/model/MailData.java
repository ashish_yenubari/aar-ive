package com.aar.aarive.api.service.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.springframework.web.multipart.MultipartFile;

public class MailData {
  @Email
  private String senderInfo;
  private String receiverInfo;
  @NotNull
  private String data;
  private String cc;
  private String bcc;
  private String subject;
  private List<MultipartFile> file;

  public String getSenderInfo() {
    return senderInfo;
  }

  public void setSenderInfo(String senderInfo) {
    this.senderInfo = senderInfo;
  }

  public String getReceiverInfo() {
    return receiverInfo;
  }

  public void setReceiverInfo(String receiverInfo) {
    this.receiverInfo = receiverInfo;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public String getCc() {
    return cc;
  }

  public void setCc(String cc) {
    this.cc = cc;
  }

  public String getBcc() {
    return bcc;
  }

  public void setBcc(String bcc) {
    this.bcc = bcc;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public List<MultipartFile> getFile() {
    return file;
  }

  public void setFile(List<MultipartFile> file) {
    this.file = file;
  }
}
