package com.aar.aarive.service.media;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.aar.aarive.service.MediaDownloadService;

@Service
public class CloudDownloadService implements MediaDownloadService {

  @Override
  public File mediaDownload(String fileName) throws IOException {
    // logic to check if file exists ,if file doesnt exist throw exception
    return new File(fileName);
  }

  @Override
  public HttpHeaders getHeaderResp(String fileName) {
    // logic to set mime ,and content-disposition
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add("Content-Type", URLConnection.guessContentTypeFromName(fileName));
    httpHeaders.add("content-disposition", "attachment; filename=" + fileName);
    return httpHeaders;
  }

}
