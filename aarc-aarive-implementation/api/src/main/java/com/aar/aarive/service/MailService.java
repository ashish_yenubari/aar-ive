package com.aar.aarive.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.aar.aarive.api.service.model.MailData;
import com.aar.aarive.constant.Constants;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/*
 * consume the User record and convert it into a standard E-mail structure
 */
@Service()
@PropertySource("classpath:sender.properties")
public class MailService {

  @Autowired
  private JavaMailSender javaMailSender;

  @Value("${mail.from}")
  private String sender;
  @Value("${mail.registration.subject}")
  private String regSubject;
  @Value("${mail.registration.receiver}")
  private String regReciever;

  private static Logger log = LoggerFactory.getLogger(MailService.class);

  private MimeMessage buildMail(String from, String receiverInfo, String body, String cc,
      String bcc, String subject, List<MultipartFile> files) throws MessagingException {
    MimeMessage message = javaMailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message, true);
    helper.setFrom(from);
    helper.setSubject(subject);
    helper.setTo(receiverInfo.split(";"));
    if (cc != null)
      helper.setCc(cc.split(";"));
    if (bcc != null)
      helper.setBcc(bcc.split(";"));
    helper.setText(body, true);
    if (files != null) {
      for (MultipartFile file : files) {
        helper.addAttachment(file.getOriginalFilename(), file);
      }
    }
    return message;
  }

  // note for register data we need a method to write data
  public ResponseEntity<?> sendMail(String data) throws Exception {
    return sendMail(sender, regReciever, data, null, null, regSubject, null);
  }

  @Async
  public ResponseEntity<?> sendMail(MailData data) throws Exception {
    return sendMail(data.getSenderInfo(), data.getReceiverInfo(), data.getData(), data.getCc(),
        data.getBcc(), data.getSubject(), data.getFile());
  }

  // This method is used to provide details about body ,sender and reciever
  public ResponseEntity<?> sendMail(String senderInfo, String receiverInfo, String body, String cc,
      String bcc, String subject, List<MultipartFile> files) throws Exception {
    log.info("Sending mail ...");
    try {
      if (receiverInfo == null) {
        throw new AddressException("empty recipient list");
      }
      javaMailSender
          .send(buildMail(senderInfo, receiverInfo, mailBody(body), cc, bcc, subject, files));
    } catch (MailException | MessagingException | IOException e) {
      log.error(e.getMessage());
      throw new Exception(e.getMessage());
    }
    log.info("Mail sent");
    return new ResponseEntity<>(Constants.MAIL_RESPONSE_SUCCESS, HttpStatus.OK);
  }

  private String mailBody(String body)
      throws JsonParseException, JsonMappingException, IOException {
    StringBuilder emailBody = new StringBuilder("<html><body>");
    Map<String, String> map =
        new ObjectMapper().readValue(body, new TypeReference<Map<String, String>>() {});
    Set<Entry<String, String>> entrySet = map.entrySet();
    for (Entry<String, String> entry : entrySet) {
      emailBody.append("<br>" + entry.getKey() + "\t:\t" + entry.getValue() + "<br>");
    }
    emailBody.append("</body></html>");
    return emailBody.toString();
  }
}

