package com.aar.aarive.spring.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.aar.aarive.constant.Constants;

/**
 * Just return 401-unauthorized for every unauthorized request. The client side catches this and
 * handles login itself.
 */
@Component(value = Constants.SECURITY_UNAUTH_ENTRY_POINT)
public class UnauthorisedEntryPoint implements AuthenticationEntryPoint {

  private static Logger log = LoggerFactory.getLogger(UnauthorisedEntryPoint.class);

  public void commence(HttpServletRequest request, HttpServletResponse response,
      AuthenticationException exception) throws IOException, ServletException {
    log.error(exception.getMessage());
    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, exception.getMessage());
  }
}
