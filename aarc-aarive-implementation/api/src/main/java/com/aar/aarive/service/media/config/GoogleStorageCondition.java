
package com.aar.aarive.service.media.config;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class GoogleStorageCondition implements Condition {

  @Override
  public boolean matches(ConditionContext context, AnnotatedTypeMetadata annoMetadata) {
    Environment env = context.getEnvironment();
    String localstorage = env.getProperty("filestorage");
    if (localstorage != null && localstorage.equals("googleService"))
      return true;

    return false;
  }

}
