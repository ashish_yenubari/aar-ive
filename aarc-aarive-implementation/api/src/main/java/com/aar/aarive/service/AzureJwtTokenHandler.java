package com.aar.aarive.service;

import java.util.ArrayList;
import java.util.Map;

import com.aar.aarive.service.model.JwtUserProfile;

public class AzureJwtTokenHandler {

  public static JwtUserProfile convertClaimsToUser(Map<String, Object> jwtClaims) {
    JwtUserProfile newUserProfile = new JwtUserProfile();
    newUserProfile.setName(jwtClaims.get("name").toString());
    newUserProfile.setUsername(jwtClaims.get("unique_name").toString());
    newUserProfile.setOnPremId(jwtClaims.get("onprem_sid").toString());
    ArrayList<String> userRoles = (ArrayList<String>) jwtClaims.get("roles");
    if (userRoles != null) {
      for (String role : userRoles) {
        newUserProfile.addAuthority(role);
      }
    }
    return newUserProfile;
  }
}

