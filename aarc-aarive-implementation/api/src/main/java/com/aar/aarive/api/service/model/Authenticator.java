package com.aar.aarive.api.service.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component(value = "authenticator")
@PropertySource("classpath:sender.properties")
public class Authenticator {

  @Value("${authentication.enabled}")
  private String authEnabled;

  public Boolean getAuth(Boolean isAuthenticated) {
    if (authEnabled.equals("false")) {
      return true;
    }
    return isAuthenticated;
  }
}
