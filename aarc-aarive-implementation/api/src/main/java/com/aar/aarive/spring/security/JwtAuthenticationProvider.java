package com.aar.aarive.spring.security;

import java.text.ParseException;

import org.jose4j.jwt.consumer.InvalidJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.aar.aarive.service.TokenService;
import com.aar.aarive.service.model.JwtUserProfile;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

  @Autowired
  private TokenService tokenAuthenticationService;

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    JwtUserProfile userProfile = (JwtUserProfile) authentication.getPrincipal();
    if (userProfile != null) {
      try {
        tokenAuthenticationService.validateToken(userProfile.getToken());
      } catch (ParseException | InvalidJwtException e) {
        throw new BadCredentialsException("JWT Token is not Valid");
      }
      return new UsernamePasswordAuthenticationToken(userProfile, null,
          userProfile.getAuthorities());
    }
    throw new BadCredentialsException("JWT Token is not Valid");
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
  }
}
