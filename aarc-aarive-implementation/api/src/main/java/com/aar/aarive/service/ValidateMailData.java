package com.aar.aarive.service;

import java.util.Arrays;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.aar.aarive.api.service.model.MailData;
import com.aar.aarive.constant.Constants;

/*
 * consume the User record and convert it into a standard E-mail structure
 */
@Service()
@PropertySource("classpath:sender.properties")
public class ValidateMailData {

  @Value("${mail.from}")
  private String sender;
  @Value("${mail.default.subject}")
  private String defSubject;
  @Value("${mail.default.receiver}")
  private String defReciever;

  private static Logger log = LoggerFactory.getLogger(ValidateMailData.class);

  public MailData validate(MailData mailData) throws Exception {

    log.info("Processing ...");
    if (mailData.getSenderInfo() == null) {
      mailData.setSenderInfo(sender);
    }
    String receiverInfo = getValidMailIds(mailData.getReceiverInfo(), defReciever);
    if (receiverInfo == null) {
      log.error("Recipient list null");
      throw new Exception("Recipient list null");
    }
    if (receiverInfo.length() < 1) {
      log.error("Recipient list empty");
      throw new Exception("Recipient list empty");
    }
    mailData.setReceiverInfo(receiverInfo);
    mailData.setBcc(getValidMailIds(mailData.getCc(), null));
    mailData.setCc(getValidMailIds(mailData.getBcc(), null));
    if (mailData.getSubject() == null) {
      mailData.setSubject(defSubject);
    }
    validateFileSize(mailData.getFile());
    return mailData;
  }

  private void validateFileSize(List<MultipartFile> file) throws Exception {
    Long fileSize = 0l;
    if (file != null) {
      for (MultipartFile multipartFile : file) {
        Long size = multipartFile.getSize();
        fileSize += size.intValue();
        if (fileSize > Constants.MAX_FILESIZE_LIMIT) {
          throw new Exception(
              "File Size is beyond Max Filesize Limit:" + Constants.MAX_FILESIZE_LIMIT + "MB");
        }
      }
    }
  }

  // this method is used to validate the entered email
  private InternetAddress getValidMailId(String mail) {
    if (mail != null) {
      try {
        InternetAddress internetAddress = new InternetAddress(mail);
        internetAddress.validate();
        return internetAddress;
      } catch (AddressException e) {
        log.warn("Invalid Address");
      }
    }
    return null;
  }

  private String getValidMailIds(String reciever, String defaultReciver) {
    List<String> mails;
    if (reciever == null) {
      if (defaultReciver == null) {
        return null;
      }
      mails = Arrays.asList(defaultReciver.split(";"));
    } else {
      mails = Arrays.asList(reciever.split(";"));
    }
    StringBuilder to = new StringBuilder();
    for (String mail : mails) {
      InternetAddress internetAddress = getValidMailId(mail);
      if (internetAddress != null) {
        to.append(internetAddress.getAddress() + ";");
      }
    }
    return to.toString();
  }
}
