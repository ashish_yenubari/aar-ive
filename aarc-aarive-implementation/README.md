AAR Commercial AARive
=====================


![Software Stack](doc/AAR_Aarive_Components.svg)

### Angular JS 1.5 with Bootstrap and Spring 4 and Spring Security:  


The API can run on any web server, but it has been tested against Tomcat 8, the server required http DELETE and PUT, so ensure your web server can support those http methods.

The CLIENT currently is run via gulp, for a production release you could extract the .zip artifact and run the static client via Apache.

Ensure that you proxy the API using reverse-proxy setup as explained below so that you have the same domain otherwise you will experience CORS related issues. (deployed artifacts only)

### Gulp:
Used as the build tool for the client, this has been written using ES6

### Spring 4:
Used to create RESTful controller interfaces which in turn gets called through ajax requests.
	
### Spring Security 4:
Used for a stateless api that allows authentication via basic authentication or token authentication.


### Recommendations:

IDE:  
IntelliJ 16+  
Eclipse STS 3.*  
Webstorm 2016+  

### IntelliJ/Webstorm configuration:

Enable live linting:

* `Preferences > Languages & Frameworks > JavaScript > Code Quality Tools > ESLint`:
  Check the box to enable ESLint.

Enable ES6+ (ES2015) JS syntax highlighting:

* `Preferences > Languages & Frameworks > JavaScript`:
  Select JavaScript language version: ECMAScript 6

### Eclipse STS/IntelliJ java coding  guidelines configuration:

This link specifies Google-Java coding Guidelines: `https://google.github.io/styleguide/javaguide.html`

Enable google-java-codelines: 

* Eclipse : 
Download `eclipse-java-google-style.xml` file from `http://code.google.com/p/google-styleguide/` repo. 
Under `Window > Preferences > Java > Code Style > Formatter`.Import the settings file by selecting Import.

* IntelliJ :
Download `intellij-java-google-style.xml` file from `http://code.google.com/p/google-styleguide/` repo. 

Windows

Under Settings -> Editor -> Code Style -> manage -> import -> select the google-styleguide for intelliJ as current code style for the  project.

Macintosh

Download it and go into Preferences -> Editor -> Code Style. Click on Manage and import the downloaded Style Setting file. Select GoogleStyle as new coding style.

Testing
====================
Simply run on the parent pom to have node and modules auto install and execute all tests. **(REQUIRED FOR FIRST RUN)**

Ensure you have Maven 3.2.0+ in the path  
Ensure you have NodeJS v4.4.7+ and npm in the path  

To run specific profiles please run mvn clean install and simple pass the profile you wish to execute.

`$ mvn clean install -P<your-environment>` 

There are 5 environments dev,prod,stage,release and qa

This will execute Java and Jasmine tests that will test both java classes and angular js files.

You can also run jasmine only tests if you wish via the front end:

**http://localhost:4444/test**

Running
====================

### Run the API via maven Tomcat plugin (Optional, when using internal APIs):
`$ cd api`  
`$ mvn tomcat:run`  

### Run the CLIENT via gulp.babel.js:
`$ cd client`  
`$ npm install`  
`$ npm start`  
or  
`$ node ./node_modules/gulp/bin/gulp.js run --enable-source-maps --enable-mock-data`

### Run the reverse proxy server using docker:
`$ cd reverse-proxy`  
`$ bash ./start.sh <your-local-ip-address>`  
For more details refer to reverse-proxy/README.md  
You can change the PITSS_API_ADDRESS in reverse-proxy/setup.sh  

### The landing page can be accessed at  

**http://localhost:5555**

### The client application can be accessed at

**http://192.168.99.100**  
or  
**http://docker-ip-address**  


### Run the test harness via SoapUI plugin:
`$ cd api`  
`$ mvn integration-test`  


Authentication
==============
JWT is used as authentication mechanism in AARive. The responsibility of passing a valid JWT lies
with the client application. The standard mechanism for obtaining JWT from Azure is following:
https://azure.microsoft.com/en-in/documentation/articles/active-directory-authentication-scenarios/#single-page-application-spa

As depicted in the software component stack in above diagram, the wirestone service (api) and the PITSS service makes use
of the public signing key obtained from the azure to validate the authentic request. If the request is not authenticated the onus
is on the service to redirect to Azure url so that the same can be authenticated.

Authorization
=============
In the Azure AD it is possible to create security groups and assign some access permissions to that group for accessing resources.
These resources can be internal or external. For more details please refer to following:
https://azure.microsoft.com/en-us/documentation/articles/active-directory-accessmanagement-manage-groups/

The assignment of users to group can be done using on premise AD or Azure AD. The JWT token created has a field by name roles from
where the group information can be extracted.
 `"roles": [
   "Admin"
 ],`
 
Using ACLs in the application the permission could be granted now for a user having a certain role.
 
Multi-tenancy
=============
TBD
 