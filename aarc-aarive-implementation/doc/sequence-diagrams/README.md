Sequence Diagrams
=================


![User registration send email](UserRegistrationEmail.png)  

The email sending should be a seamless experience for user.
For flexibility it is envisioned that sending email should be responsibility of backend service exposed through RESTful interface.
Backend email sending service - WS API should take care of validating the input form.
This service will also be able to provide security and rate limit in future.
WS API uses standard [JavaMail API](https://javamail.java.net/nonav/docs/api/) to hook it up with AAR exchange SMTP server in Azure cloud.
This is also flexible and can easily be changed from backend without affecting the frontend client.