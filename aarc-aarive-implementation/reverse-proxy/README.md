Install docker
==============
### Download and install from
https://www.docker.com/products/docker-toolbox

### Create virtualbox default machine (Use Git bash for Windows)
`$ docker-machine create --driver virtualbox default`


Run reverse-proxy docker container
==================================
### Use Git bash for Windows and Terminal for MacOSX  
Find your local IP address for below command and replace with \<your-local-ip-address>

`$ bash ./start.sh <your-local-ip-address>`

Troubleshooting
===============
On some Windows machines there are issues observed when reverse proxy just
does not come up. The reason for this was found to be conversion of line
endings from LF to CRLF on such systems. To avoid this please enable git
autocrlf to false with below command and clone the repo again

`$ git config --global core.autocrlf false`  

