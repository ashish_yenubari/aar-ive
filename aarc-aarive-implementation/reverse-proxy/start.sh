#!/usr/bin/env bash

set -e

DOCKER_IMAGE_NAME=aar-quote-acceleration-reverse-proxy
DOCKER_CONTAINER_NAME=aar

print_usage_and_exit() {
    echo "Usage:"
    echo "$ bash ./start.sh <local-ip-address>"
    exit -1
}

init() {
    echo "Starting docker machine"
    docker-machine start default || :

    echo "Setting up the environment variables"
    eval $(docker-machine env default)
}

cleanup() {
    echo "Stopping and deleting the container"
    docker stop $DOCKER_CONTAINER_NAME 2> /dev/null && docker rm $DOCKER_CONTAINER_NAME 2> /dev/null

    echo "Deleting the Docker image"
    docker rmi $DOCKER_IMAGE_NAME 2> /dev/null || :
}

build() {
    echo "Building the docker image"
    docker build -t $DOCKER_IMAGE_NAME .
}

run() {
    echo "Running the docker container"
    docker run --net="host" -e LOCAL_IP=$1 --name $DOCKER_CONTAINER_NAME -d $DOCKER_IMAGE_NAME
}

main() {
    echo "Setting up docker container for IP address: $1"
    init
    cleanup
    build
    run $1
    echo "Setup completed successfully!"
    echo ""
    echo "You may access the application at `docker-machine ip`"
    echo ""
}

if [ "$#" -ne 1 ]; then
    print_usage_and_exit
else
    main $1
fi
