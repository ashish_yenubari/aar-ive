#!/bin/bash

set -e

LOCAL_IP_ADDRESS=`printenv LOCAL_IP`

# Complete host address for angular SPA
CLIENT_ADDRESS="${LOCAL_IP_ADDRESS}:4444"
WS_API_ADDRESS="${LOCAL_IP_ADDRESS}:8080"
REGISTER_APP_ADDRESS="${LOCAL_IP_ADDRESS}:5555"
PITSS_API_ADDRESS="devws.aarcorp.com"


sed -i "s/client-address/${CLIENT_ADDRESS}/g" conf.d/default.conf
sed -i "s/ws-api-address/${WS_API_ADDRESS}/g" conf.d/default.conf
sed -i "s/pitss-api-address/${PITSS_API_ADDRESS}/g" conf.d/default.conf
sed -i "s/register-app-address/${REGISTER_APP_ADDRESS}/g" conf.d/default.conf