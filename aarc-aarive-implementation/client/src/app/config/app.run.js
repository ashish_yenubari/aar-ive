
angular.module('app').run([
  '$rootScope',
  '$state',
  function(
    $rootScope,
    $state
  ) {
    $rootScope.currentStateClass = function() {
      var stateName = $state.current.name;
      stateName = stateName.toLowerCase().replace(/[^a-z0-9]/g, '-');
      return stateName;
    };

    $rootScope.$on('$destroy', $rootScope.$on('unauthorized', function() {
      $state.go('root.home');
    }));
  }
]);
