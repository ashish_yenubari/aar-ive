
angular.module('app.components', [
  'app.templates',
  'app.services',
  'app.directives',
  'app.filters',
  'fixed.table.header',
  'ngSanitize',
  'autoCompleteModule'
]);
