angular.module('app.directives').directive('columnSort', [function() {
  return {
    restrict: 'A',
    require: '^^columnSorting',
    link: function(scope, element, attrs, controller) {
      // Handle clicks on this column, and send to the parent column sort controller
      var self = this;
      element.bind('click', function() {
        // Remove elements before sorting to solve issue with flashing of duplicates via ngRepeat
        element.closest('table').next().find('table tbody tr').remove();

        self.itemClicked = true;
        controller.toggleSort(attrs.columnSort);
      });

      // Update all the column classes for any column sorting changes
      scope.$watch(function() {
        return controller.sortCol + controller.sortAsc;
      }, function() {
        element.toggleClass('sort-active', controller.sortCol === attrs.columnSort);
        var sortAsc = (controller.sortCol !== attrs.columnSort) || controller.sortAsc;
        element.toggleClass('sort-asc', sortAsc);
        element.toggleClass('sort-desc', !sortAsc);
      });
    }
  };
}]);
