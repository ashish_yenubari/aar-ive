angular.module('app.directives').directive('clickOut', [
  '$window', '$parse', '$timeout',
  function($window, $parse, $timeout) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        var clickOutHandler = $parse(attrs.clickOut);

        var windowClick = function(event) {
          if (!element[0].contains(event.target)) {
            event.stopPropagation();
            clickOutHandler(scope, {$event: event});
            scope.$apply();
          }
        };

        // Timeout to prevent the click that caused rendering of this directive from immediately removing it
        $timeout(function() {
          angular.element($window).on('click', windowClick);
        });

        scope.$on('$destroy', function() {
          angular.element($window).off('click', windowClick);
        });
      }
    };
  }
]);
