angular.module('app.directives').directive('fixedHeader', ['$compile', '$timeout', function($compile, $timeout) {
  return {
    restrict: 'A',
    terminal: true,
    priority: 1000,
    compile: function(element, attrs) {
      return function(scope, element, attrs) {
        // Add the fixHead directive after a timeout, and remove this directive to avoid an infinite loop
        $timeout(function() {
          element.attr('fix-head', 'true');
          element.removeAttr('fixed-header');
          element.removeAttr('data-fixed-header');
          $compile(element)(scope);
        });
      };
    }
  };
}]);
