angular.module('app.directives').directive('delayShow', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    scope: false,
    link: function(scope, element, attr, controller) {
      element.addClass('delay-hide');
      var timeout = attr.delayTime || 0;
      var timeoutPromise;

      scope.$watch(attr.delayShow, function(value) {
        if (value) {
          timeoutPromise = $timeout(function() {
            element.addClass('delay-show');
          }, timeout);
        } else {
          if (timeoutPromise) $timeout.cancel(timeoutPromise);
          element.removeClass('delay-show');
        }
      });
    }
  };
}]);
