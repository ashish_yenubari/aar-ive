angular.module('app.directives').directive('columnSorting', [function() {
  return {
    restrict: 'A',
    scope: {
      handleSort: '=columnSorting',
      defaultSortCol: '=defaultSortCol',
      defaultSortAsc: '=defaultSortAsc'
    },
    bindToController: true,
    controllerAs: "$ctrl",
    controller: [function() {
      // Handle clicks on this column, and send to the parent column sort
      this.toggleSort = function(col) {
        this.sortAsc = (col === this.sortCol) ? !this.sortAsc : true;
        this.sortCol = col;
        this.handleSort(this.sortCol, this.sortAsc);
      };

      this.sortCol = this.defaultSortCol;
      this.sortAsc = this.defaultSortAsc;
      this.handleSort(this.sortCol, this.sortAsc);
    }]
  };
}]);
