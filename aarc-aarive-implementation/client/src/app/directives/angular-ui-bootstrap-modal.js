angular.module('app.directives', [])
  .constant('modalConfig', {
    backdrop: true,
    escape: true
  })
  .directive('modal', ['$parse', 'modalConfig',
    function($parse, modalConfig) {
      var backdropEl;
      // TODO: Use Angular $document service and remove eslint-disable-line
      var body = angular.element(document.getElementsByTagName('body')[0]); // eslint-disable-line
      return {
        restrict: 'EA', // eslint-disable-line
        link: function(scope, elm, attrs) {
          var opts = angular.extend({}, modalConfig, scope.$eval(attrs.uiOptions || attrs.bsOptions || attrs.options));
          var shownExpr = attrs.modal || attrs.show;
          // TODO: Remove eslint-disable-line, and remove this variable if it's set below but never used
          var setClosed; // eslint-disable-line

          if (attrs.close) {
            setClosed = function() {
              scope.$apply(attrs.close);
            };
          } else {
            setClosed = function() {
              scope.$apply(function() {
                $parse(shownExpr).assign(scope, false);
              });
            };
          }
          elm.addClass('modal-confirmation');

          if (opts.backdrop && !backdropEl) {
            backdropEl = angular.element('<div class="modal-backdrop"></div>');
            backdropEl.css('display', 'none');
            body.append(backdropEl);
          }

          // TODO: Remove this function if it is not used, or remove the eslint-disable-line comments
          function setShown(shown) { // eslint-disable-line
            scope.$apply(function() {
              model.assign(scope, shown); // eslint-disable-line
            });
          }

          function escapeClose(evt) {
            // if (evt.which === 27) { setClosed(); }
          }

          function clickClose() {
            // setClosed();
          }

          function close() {
            if (opts.escape) {
              body.unbind('keyup', escapeClose);
            }
            if (opts.backdrop) {
              backdropEl.css('display', 'none').removeClass('in');
              backdropEl.unbind('click', clickClose);
            }
            elm.css('display', 'none').removeClass('in');
            body.removeClass('modal-open');
          }

          function open() {
            if (opts.escape) {
              body.bind('keyup', escapeClose);
            }
            if (opts.backdrop) {
              backdropEl.css('display', 'block').addClass('in');
              // TODO: See if != can be replaced with !== and eslint re-enabled
              if (opts.backdrop != "static") {  // eslint-disable-line
                backdropEl.bind('click', clickClose);
              }
            }
            elm.css('display', 'block').addClass('in');
            body.addClass('modal-open');
          }

          scope.$watch(shownExpr, function(isShown, oldShown) {
            if (isShown) {
              open();
            } else {
              close();
            }
          });
        }
      };
    }
  ]
);
