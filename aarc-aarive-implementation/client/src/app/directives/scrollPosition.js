
angular.module('app.directives').directive('scrollPosition', [function() {
  return {
    scope: {
      scrollPosition: '='
    },
    link: function(scope, elem, attrs) {
      return elem.bind('scroll', function() {
        var pos = Math.round(elem.scrollTop());
        // There's nothing to do if a valid callback function hasn't been specified
        if (scope.scrollPosition) scope.scrollPosition(pos);
      });
    }
  };
}]);
