angular.module('app.directives').directive('fileread', [function() {
  return {
    scope: {
      fileread: "="
    },
    link: function(scope, element, attributes) {
      element.bind("change", function(changeEvt) {
        scope.$apply(function() {
          var files = changeEvt.target.files;
          scope.fileread.push(files[0]);
        });
      });
    }
  };
}]);
