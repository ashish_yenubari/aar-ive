
angular.module('app.filters').filter('google', ['$sce', function($sce) {
  return function(input) {
    return (input) ?
      $sce.trustAsHtml('<a target="_blank" href="https://www.google.com/search?q=' + input + '">' + input + '</a>') : '';
  };
}]);
