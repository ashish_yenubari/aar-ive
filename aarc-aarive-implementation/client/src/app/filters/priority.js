
angular.module('app.filters').filter('priority', ['Store', function(Store) {
  var labels = {};
  // TODO: This is assuming the filter priorities config is already loaded.
  // It should be available, but it would ideal to preload it here.
  var filterTabs = Store.get('filterTabs') || { items: [] };
  angular.forEach(filterTabs.items, function(priority) {
    labels[priority.key] = priority.label;
  });

  return function(input) {
    return (input && labels[input.toLowerCase()]) || input;
  };
}]);
