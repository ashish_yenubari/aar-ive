angular.module('app',
  [
    'ui.router',
    'ngAnimate',
    'ngCookies',
    '720kb.datepicker',
    'app.constants',
    'app.services',
    'app.components',
    'app.utilityConstants',
    'mm.acl'
  ]
);

angular.module('app').config([
  '$locationProvider',
  '$stateProvider',
  '$urlRouterProvider',
  '$httpProvider',
  'defaultConstant',
  function(
      $locationProvider,
      $stateProvider,
      $urlRouterProvider,
      $httpProvider,
      defaultConstant
  ) {
    $locationProvider.html5Mode(true).hashPrefix('!');
    $urlRouterProvider.otherwise('/');

    $stateProvider.state({
      abstract: true,
      name: 'root',
      url: '',
      views: {
        layout: { component: 'layout' }
      },
      resolve: {
        setUserId: ['AzureAuth', function(AzureAuth) {
          return AzureAuth.setUserId();
        }],
        setUserAbilities: ['AuthorizationMatrix', function(AuthorizationMatrix) {
          return AuthorizationMatrix.setUserAbilities();
        }]
      }
    });
    $stateProvider.state({
      name: 'root.home',
      url: '/',
      views: {
        main: { component: 'home'}
      }
    });
    $stateProvider.state({
      name: 'root.newPoolRequest',
      url: '/newPoolRequest',
      views: {
        main: { component: 'newPoolRequest'}
      },
      resolve: {
        acl: ['$q', 'AclService', '$rootScope', 'defaultConstant', 'Store',
          function($q, AclService, $rootScope, defaultConstant, Store) {
            return $q(function(resolve, reject) {
              if (!AclService.can(defaultConstant.ACTION_NEW_POOL_REQ)) {
                Store.set('appError', 'Your user account is not authorized to access this page');
                reject('Unauthorized');
                $rootScope.$broadcast('unauthorized');
              } else {
                resolve();
              }
            });
          }]
      }
    });

    $stateProvider.state({
      name: 'root.dashboard',
      url: '/dashboard',
      views: {
        main: { component: 'dashboard'} // TODO: Consider renaming "dashboard" to "poolOrderDashboard"
      }
    });
    $stateProvider.state({
      name: 'root.dashboard.tab',
      url: '/:tabName',
      params: {
        tabName: 'all',
        searchType: null,
        searchValue: null
      }
    });
    $stateProvider.state({
      name: 'root.dashboard.tab.search',
      url: '/:searchType/:searchValue'
    });

    $httpProvider.interceptors.push('$q', '$location', '$cookies', function($q, $location, $cookies) {
      return {
        request: function(config) {
          if (config.url.indexOf(defaultConstant.ENDPOINT_URL) === 0) {
            var email = $cookies.get(defaultConstant.USERID_KEY) || defaultConstant.USER_EMAIL;
            if (config.method === 'POST') {
              config.data.parameters = config.data.parameters || [];
              config.data.parameters.push({ email: email });
            } else {
              config.params = config.params || {};
              // Specific API requests may override Email, or exclude it by setting it to null
              if (typeof config.params.Email === 'undefined') {
                config.params.q = (config.params.q) ? (config.params.q + ';') : '';
                config.params.q += 'Email=' + email;
              }
            }
          }
          config.headers = config.headers || {};
          return config;
        },
        responseError: function(response) {
          if (response.status === 401 || response.status === 403) {
            $location.path('/');
          }

          // Return a promise rejection so that $http catch works correctly.
          return $q.reject(response);
        }
      };
    });
  }
]);

