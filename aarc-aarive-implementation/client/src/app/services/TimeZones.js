'use strict';

angular.module('app.utilityConstants').constant('TimeZones',
  {
    timeZones: [
      {
        offset: -12,
        text: "UTC-12:00 International Date Line West"
      },
      {
        offset: -11,
        text: "UTC-11:00 Coordinated Universal Time"
      },
      {
        offset: -10,
        text: "UTC-10:00 Hawaiian Standard Time",
        isDST: false
      },
      {
        offset: -9,
        text: "UTC-9:00 Hawaiian Daylight Time",
        isDST: true
      },
      {
        offset: -9,
        text: "UTC-09:00 Alaskan Standard Time",
        isDST: false
      },
      {
        offset: -8,
        text: "UTC-08:00 Alaskan Daylight Time",
        isDST: true
      },
      {
        offset: -8,
        text: "UTC-08:00 Pacific Standard Time",
        isDST: false
      },
      {
        offset: -7,
        text: "UTC-07:00 Pacific Daylight Time",
        isDST: true
      },
      {
        offset: -7,
        text: "UTC-07:00 US Mountain Standard Time",
        isDST: false
      },
      {
        offset: -6,
        text: "UTC-06:00 US Mountain Daylight Time",
        isDST: true
      },
      {
        offset: -6,
        text: "UTC-06:00 Central US Standard Time",
        isDST: false
      },
      {
        offset: -5,
        text: "UTC-05:00 Central US Daylight Time",
        isDST: true
      },
      {
        offset: -5,
        text: "UTC-05:00 Eastern Standard Time",
        isDST: false
      },
      {
        offset: -4.5,
        text: "UTC-04:30 Venezuela Standard Time"
      },
      {
        offset: -4,
        text: "UTC-04:00 Eastern Daylight Time",
        isDST: true
      },
      {
        offset: -4,
        text: "UTC-04:00 Atlantic Standard Time",
        isDST: false
      },
      {
        offset: -3.5,
        text: "UTC-03:30 Newfoundland Time"
      },
      {
        offset: -3,
        text: "UTC-03:00 Atlantic Daylight Time",
        isDST: true
      },
      {
        offset: -3,
        text: "UTC-03:00 Brasilia Time"
      },
      {
        offset: -2,
        text: "UTC-02:00 Coordinated Universal Time"
      },
      {
        offset: -1,
        text: "UTC-01:00 Cape Verde Standard Time"
      },
      {
        offset: 0,
        text: "UTC+00:00 Western European Time, Greenwich Mean Time"
      },
      {
        offset: 1,
        text: "UTC+01:00 Central European Time Zone"
      },
      {
        offset: 2,
        text: "UTC+02:00 South African Standard Time Zone"
      },
      {
        offset: 3,
        text: "UTC+03:00 East African Time Zone"
      },
      {
        offset: 3.5,
        text: "UTC+03:30 Iran Standard Time"
      },
      {
        offset: 4,
        text: "UTC+04:00 Arabian Standard Time, Russian Standard Time"
      },
      {
        offset: 4.5,
        text: "UTC+04:30 Afghanistan Standard Time"
      },
      {
        offset: 5,
        text: "UTC+05:00 Pakistan Standard Time"
      },
      {
        offset: 5.5,
        text: "UTC+05:30 India Standard Time, Sri Lanka Standard Time"
      },
      {
        offset: 5.75,
        text: "UTC+05:45 Nepal Standard Time"
      },
      {
        offset: 6,
        text: "UTC+06:00 Central Asia Standard Time"
      },
      {
        offset: 6.5,
        text: "UTC+06:30 Myanmar Standard Time"
      },
      {
        offset: 7,
        text: "UTC+07:00 SE Asia Standard Time"
      },
      {
        offset: 8,
        text: "UTC+08:00 China Standard Time, W. Australia Standard Time"
      },
      {
        offset: 8.5,
        text: "UTC+08:30 North Korea Standard Time"
      },
      {
        offset: 8.75,
        text: "UTC+08:45 Central Western Australia Time"
      },
      {
        offset: 9,
        text: "UTC+09:00 Tokyo Standard Time"
      },
      {
        offset: 9.5,
        text: "UTC+09:30 AUS Central Standard Time"
      },
      {
        offset: 10,
        text: "UTC+10:00 AUS Eastern Standard Time"
      },
      {
        offset: 11,
        text: "UTC+11:00 Central Pacific Standard Time"
      },
      {
        offset: 12,
        text: "UTC+12:00 New Zealand Standard Time"
      },
      {
        offset: 13,
        text: "UTC+13:00 Samoa Standard Time"
      },
      {
        offset: 14,
        text: "UTC+14:00 Kiribati Standard Time"
      }
    ]
  });
