angular.module('app.services').factory('PoolOrderList', [
  '$http',
  '$q',
  'defaultConstant',
  'Store',
  function(
    $http,
    $q,
    defaultConstant,
    Store
  ) {
    var canceler;

    var tabImpl = function(name, params, query) {
      query.q = query.q || [];
      if (params && params.priority) {
        switch (params.priority) {
          // Pool History is the list of U/S Shipped records within the last six months, minus those
          // records which were shipped within the last three days (and should still be under Core)
          case 'poolHistory':
            var orderDate = new Date();
            var currentMonth = orderDate.getMonth();
            orderDate.setMonth(currentMonth - 6);
            var shipDate = new Date();
            shipDate.setDate(shipDate.getDate() - 3);
            query.q.push('OrderDate>' + orderDate.toISOString() +
              ';ShipDate<' + shipDate.toISOString() +
              ';UsStatus=U/S Shipped');
            break;
          case 'core':
            query.finder = 'UsShippedRowFinder';
            query.q.push('Priority=' + params.priority.toUpperCase());
            // query.q = 'Core=Y';
            break;
          case 'actionRequired':
            query.q.push('ActionRequired=Y');
            break;
          default:
            if (params.priority !== 'all')
              query.q.push('Priority=' + params.priority.toUpperCase());
            break;
        }
        return query;
      }
    };
    var searchImpl = function(name, params, query) {
      query.q = query.q || [];
      switch (params.search.type) {
        case 'SerialNumber':
          query.finder = 'PoolOrderListRowFinder;serialNum=*' + params.search.value.toUpperCase() + '*';
          break;
        default:
          query.q.push(params.search.type + ' LIKE *' + params.search.value.toUpperCase() + '*');
      }
    };

    return Store.createHandler({
      findAll: function(name, params, reset) {
        if (canceler) {
          if (!reset) return;
          canceler.resolve();
        }

        // Setup the query params used by the async request
        var perPage = 20;
        // Set default sorting options to be used if no sort params are specified
        var sortCol = defaultConstant.DEFAULT_SORT_COL;
        var sortAsc = defaultConstant.DEFAULT_SORT_ASC;
        if (angular.isDefined(params.sortAsc)) {
          sortAsc = params.sortAsc;
        }
        if (params.sortCol) {
          sortCol = params.sortCol;
          // Reverse the natural sort order for date fields like ShipDate and OrderDate
          if (sortCol.toLowerCase().match(/(date)$/)) sortAsc = !sortAsc;
        }
        var query = {
          limit: params.limit || perPage,
          offset: 0,
          orderBy: sortCol + ':' + ((sortAsc) ? 'asc' : 'desc')
        };
        if (params.priority !== 'poolHistory') {
          query.UsStatus = ' U/S Requested OR RO Created OR IS NULL ';
        }

        if (params.priority) {
          tabImpl(name, params, query);
        }
        if (params.search) {
          searchImpl(name, params, query);
        }
        if (query.q) {
          query.q = query.q.join(";");
        }
        //  Pagination: determine how many additional items to load, and where to start
        if (!reset) {
          var targetLimit = Math.max(perPage, Math.ceil(query.limit / perPage) * perPage);
          var targetOffset = targetLimit - perPage;
          var existing = Store.get(name) || {};
          if (targetOffset > (existing.offset || -1)) {
            query.offset = (existing.offset || 0) + (existing.limit || perPage);
            query.limit = targetLimit - query.offset;
          } else {
            //  There's nothing more to load until the user scrolls down more
            return;
          }
        }
        //  Set the initial values before the async load
        var initialValue = {
          isLoading: true,
          limit: query.limit,
          offset: query.offset,
          orderBy: query.orderBy
        };
        if (reset) initialValue.items = [];
        Store.set({
          name: name,
          value: initialValue
        });

        canceler = $q.defer(); // eslint-disable-line angular/deferred
        return $http(
          {
            url: defaultConstant.ENDPOINT_URL + defaultConstant.POOL_ORDER_LIST_URL,
            params: query,
            timeout: canceler.promise
          })
          .then(function(response) {
            if (response.status <= 0) return; // Exit if the request was cancelled
            canceler = null;
            var data = response.data || {};
            var items = (!reset && Store.get(name).items) || [];
            angular.forEach(data.items, function(item) {
              items.push(item);
            });
            if (items.length > 0) {
              Store.set({
                name: name,
                value: { items: items }
              });
            } else {
              Store.set({
                name: name,
                value: { message: 'No results found' }
              });
            }
            return data;
          }).catch(function() {
            if (!canceler) {
              Store.set({
                name: name,
                value: {message: 'Error loading results'}
              });
            }
          })
          .finally(function() {
            if (!canceler) {
              Store.set({
                name: name,
                value: { isLoading: false }
              });
            }
            canceler = null;
          });
      },
      init: function(name, params) {
        return this.findAll(name, params, true);
      },
      update: function(name, params, oldParams) {
        //  See if the update only changes pagination
        var updatedKeys = this.findUpdatedKeys(params, oldParams);
        var hasChanges = (updatedKeys.length && (updatedKeys.length > 1 || updatedKeys[0] !== 'limit'));
        return this.findAll(name, params, hasChanges);
      },
      findUpdatedKeys: function(newObj, oldObj) {
        var keys = [];
        var self = this;
        angular.forEach(newObj, function(val, key) {
          if (angular.isObject(val)) {
            keys = keys.concat(self.findUpdatedKeys(val, oldObj[key]));
          } else if (!oldObj || !oldObj.hasOwnProperty(key) || (newObj[key] !== oldObj[key])) {
            keys.push(key);
          }
        });
        return keys;
      }
    });
  }
]);
