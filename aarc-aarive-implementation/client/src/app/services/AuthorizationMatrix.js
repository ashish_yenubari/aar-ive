'use strict';

angular.module('app.services').factory('AuthorizationMatrix', [
  '$http',
  'defaultConstant',
  'AclService',
  function($http,
           defaultConstant,
           AclService) {
    return {
      setUserAbilities: function() {
        var query = {};
        var url = defaultConstant.AUTH_MATRIX;
        var data = {};
        return $http(
          {
            url: defaultConstant.ENDPOINT_URL + url,
            params: query
          })
          .then(function(response) {
            data.items = response.data;
            // TODO: Need to parse and set the user abilities as per response
            AclService.setAbilities(data.items);
          }).catch(function() {
          }).finally(function() {
            // TODO: Need to remove after integrating with actual service, setting this in case if its
            // not Integrated with service and not running on Mock Data
            if (!data.items) {
              AclService.setAbilities({
                admin: [
                  defaultConstant.ACTION_NEW_POOL_REQ,
                  defaultConstant.ACTION_ACTION_REQUIRED,
                  defaultConstant.ACTION_ADD_CUSTOMER_DOCUMENTS],
                creator: [
                  defaultConstant.ACTION_NEW_POOL_REQ,
                  defaultConstant.ACTION_ACTION_REQUIRED,
                  defaultConstant.ACTION_ADD_CUSTOMER_DOCUMENTS],
                reader: []
              });
            }
          });
      }
    };
  }
]);
