
angular.module('app.services').factory('PoolOrderFilters', [
  '$injector', '$timeout', 'Store',
  function($injector, $timeout, Store) {
    var filters;
    // Use mock data configuration when it is available, otherwise use production config
    if ($injector.has('PoolOrderFiltersData')) {
      filters = $injector.get('PoolOrderFiltersData').getData();
    } else {
      // The keys are identifiers used in the URL, used internally for filter/tabs, and changed to
      // uppercase to use for consuming the PoolOrderCount and PoolOrderList APIs.
      // TODO: Get this data from the backend somehow, perhaps from PoolOrderCount if/when this API
      // starts to return the correct priorities in the correct order.
      filters = [
        {
          key: 'all',
          label: 'Pool'
        },
        {
          key: 'aog',
          label: 'AOG'
        },
        {
          key: 'cri',
          label: 'CRI'
        },
        {
          key: 'sch',
          label: 'SCH'
        },
        {
          key: 'rpl',
          label: 'RPL'
        },
        {
          key: 'dpa',
          label: 'DPA'
        },
        {
          key: 'core',
          label: 'Core'
        },
        {
          key: 'actionRequired',
          label: 'Action Required'
        },
        {
          key: 'poolHistory',
          label: 'Pool History'
        }
      ];
    }
    return Store.createHandler({
      findAll: function(name) {
        Store.set({
          name: name,
          value: { isLoading: true }
        });
        return $timeout(function() {
          Store.set({
            name: name,
            value: { items: filters, isLoading: false }
          });
        });
      },

      init: function(name) {
        return this.findAll(name);
      }
    });
  }
]);
