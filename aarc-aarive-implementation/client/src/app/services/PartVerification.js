angular.module('app.services').factory('PartVerification', [
  '$http',
  'defaultConstant',
  function($http, defaultConstant) {
    return {
      verify: function(params) {
        var query = {};
        var url = defaultConstant.PART_VERIFY;
        query.q = 'Part LIKE *' + params.partNumber + '*;Tail=' + params.acTail;
        return $http(
          {
            url: defaultConstant.ENDPOINT_URL + url,
            params: query
          })
          .then(function(response) {
            var data = response.data;
            return data.items;
          });
      }
    };
  }
]);
