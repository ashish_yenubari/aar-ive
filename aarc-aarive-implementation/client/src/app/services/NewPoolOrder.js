angular.module('app.services').factory('NewPoolOrder', [
  '$http',
  'defaultConstant',
  function(
    $http,
    defaultConstant
  ) {
    return {
      createPool: function(params) {
        var data = {
          name: 'createPoolOrder',
          /* eslint-disable camelcase */
          parameters: [
            { customer_order: params.customer_order },
            { part: params.part },
            { warehouse: params.warehouse },
            { priority: params.priority },
            { quantity: '1' },
            { serial_no_installed: params.serial_no_installed },
            { tail: params.tail },
            { notes: params.notes },
            { narrative: params.narrative },
            { required_date: params.required_date },
            { required_time: params.required_time },
            { required_time_zone: params.required_time_zone },
            { imops_customer_no: '12022' },
            { imops_div_no: '1' },
            { uom: params.uom }
          ]
        };
          /* eslint-enable camelcase */
        return $http(
          {
            method: 'POST',
            url: defaultConstant.ENDPOINT_URL + defaultConstant.POOL_ORDER_LIST_URL,
            data: data
          })
          .then(function(response) {
            var data = response.data.result;
            return data;
          });
      }
    };
  }
]);
