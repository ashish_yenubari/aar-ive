
// JavaScript implementation of Java's String.hashCode() method, from:
// http://werxltd.com/wp/2010/05/13/javascript-implementation-of-javas-string-hashcode-method/
angular.module('app.services').factory('hashCode', [function() {
  return function(params) {
    var str = (angular.isString(params)) ?
      params : angular.toJson(params);
    var hash = 0;
    var c;
    if (!str) return hash;
    var strlen = str.length;
    if (strlen === 0) return hash;
    for (var i = 0; i < strlen; i++) {
      c = str.charCodeAt(i);
      hash = ((hash << 5) - hash) + c;
      hash &= hash; // Convert to 32bit integer
    }
    return hash;
  };
}]);
