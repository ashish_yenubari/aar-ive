angular.module('app.services').factory('DocList', [
  '$http',
  'defaultConstant',
  'Store',
  function(
    $http,
    defaultConstant,
    Store
  ) {
    return Store.createHandler({
      findAll: function(name, params) {
        var query = {};
        query.limit = defaultConstant.DEFAULT_PAGE_SIZE_LIMIT;
        query.q = 'OrditmId=' + params.OrditmId + ';Type=' + params.Type;
        Store.set({
          name: name,
          value: { isLoading: true }
        });
        return $http(
          {
            url: defaultConstant.ENDPOINT_URL + defaultConstant.DOC_LIST_URL,
            params: query
          })
          .then(function(response) {
            var data = response.data.items;
            Store.set({
              name: name,
              value: { items: data }
            });
            return data;
          }).catch(function() {
            Store.set({
              name: name,
              value: { message: 'Error loading results' }
            });
          })
          .finally(function() {
            Store.set({
              name: name,
              value: { isLoading: false }
            });
          });
      },
      init: function(name, params) {
        // Empty doc list ACAI-546
        Store.set({
          name: name,
          value: { items: [] }
        });
        return this.findAll(name, params);
      },
      update: function(name, params) {
        // Empty doc list ACAI-546
        Store.set({
          name: name,
          value: { items: [] }
        });
        return this.findAll(name, params);
      }
    });
  }
]);
