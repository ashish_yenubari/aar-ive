
angular.module('app.services').factory('ActionRequiredDetail', [
  '$http',
  'defaultConstant',
  'Store',
  function(
    $http,
    defaultConstant,
    Store
  ) {
    return Store.createHandler({
      findAll: function(name, params) {
        var query = {};
        var url = '';
        switch (params.action) {
          case 'Airway_Bill':
            url = defaultConstant.ADD_AIRWAY_BILL;
            query.q = 'OrdschId=' + params.OrdschId;
            break;
          case 'Off_Core':
            url = defaultConstant.OFF_CORE_RETURN;
            query.q = 'OrdschId=' + params.OrdschId;
            break;
          default:
            break;
        }
        Store.set({
          name: name,
          value: { isLoading: true }
        });
        return $http(
          {
            url: defaultConstant.ENDPOINT_URL + url,
            params: query
          })
          .then(function(response) {
            var data = response.data;
            Store.set({
              name: name,
              value: { items: data }
            });
            return data;
          }).catch(function() {
            Store.set({
              name: name,
              value: { message: 'Error loading results' }
            });
          })
          .finally(function() {
            Store.set({
              name: name,
              value: { isLoading: false }
            });
          });
      },
      init: function(name, params) {
        return this.findAll(name, params);
      },
      update: function(name, params) {
        return this.findAll(name, params);
      }
    });
  }
]);
