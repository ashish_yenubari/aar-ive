
angular.module('app.services').factory('ListOfValues', [
  '$http',
  'defaultConstant',
  'Store',
  function(
    $http,
    defaultConstant,
    Store
  ) {
    return Store.createHandler({
      findAll: function(name, params) {
        var query = {
          limit: defaultConstant.DEFAULT_PAGE_SIZE_LIMIT
        };
        var url = '';
        switch (params.lov) {
          case 'Tail_Lov':
            url = defaultConstant.TAIL_NUM_LOV;
            break;
          case 'Inst_Serial_Lov':
            url = defaultConstant.INST_SERIAL_NUM_LOV;
            query.q = 'Part=' + params.part;
            break;
          case 'Priority_Lov':
            url = defaultConstant.PRIORITY_LOV;
            query.q = (params.PspecKnown === 'Y') ? 'PspecKnown=' + params.PspecKnown + ';Tail=' + params.Tail +
              ';PspecId=' + params.PspecId : 'PspecKnown=' + params.PspecKnown;
            break;
          case 'Dest_Lov':
            url = defaultConstant.DEST_LOV;
            query.q = 'Mbk=' + params.Mbk;
            break;
          case 'Ata_Lov':
            url = defaultConstant.ATA_LOV;
            break;
          case 'CarrierMethod_Lov':
            url = defaultConstant.CARRIER_METHOD_LOV;
            query.Email = null;
            break;
          default:
            break;
        }
        Store.set({
          name: name,
          value: { isLoading: true }
        });
        return $http(
          {
            url: defaultConstant.ENDPOINT_URL + url,
            params: query
          })
          .then(function(response) {
            var data = response.data;
            var lov = [];
            switch (params.lov) {
              case 'Tail_Lov':
                angular.forEach(data.items, function(item) {
                  lov.push(item.Tail);
                });
                break;
              case 'Inst_Serial_Lov':
                angular.forEach(data.items, function(item) {
                  lov.push(item.SerialNumber);
                });
                break;
              case 'Priority_Lov':
                angular.forEach(data.items, function(item) {
                  lov.push({priorityCode: item.PriorCode, hoursToDeliver: item.HoursToDeliver, Mbk: item.Mbk});
                });
                break;
              case 'Dest_Lov':
                // Move "Other" (Id === 0) to the bottom
                if (data.items) {
                  data.items.sort(function(a, b) {
                    if (a.Id === 0) return 1;
                    if (b.Id === 0) return -1;
                    return 0;
                  });
                }
                angular.forEach(data.items, function(item) {
                  if (item.Name === item.AddressName) {
                    lov.push(item.Name);
                  } else {
                    lov.push(item.Name + ' - ' + item.AddressName);
                  }
                });
                break;
              case 'Ata_Lov':
                angular.forEach(data.items, function(item) {
                  lov.push(item.AtaChapter);
                });
                break;
              case 'CarrierMethod_Lov':
                angular.forEach(data.items, function(item) {
                  lov.push({CarrmethName: item.CarrmethName, CarrName: item.CarrName});
                });
                break;
              default:break;
            }
            Store.set({
              name: name,
              value: { items: lov }
            });
            return lov;
          }).catch(function() {
            Store.set({
              name: name,
              value: { message: 'Error loading results' }
            });
          })
          .finally(function() {
            Store.set({
              name: name,
              value: { isLoading: false }
            });
          });
      },
      init: function(name, params) {
        return this.findAll(name, params);
      },
      update: function(name, params) {
        return this.findAll(name, params);
      }
    });
  }
]);
