angular.module('app.services').factory('PoolOrderDetails', [
  '$http',
  'defaultConstant',
  'Store',
  function(
    $http,
    defaultConstant,
    Store
  ) {
    return Store.createHandler({
      findAll: function(name, params) {
        var url;
        if (params.index === "2") {
          url = defaultConstant.POOL_ORDER_ACTIVITY_URL;
        } else if (params.index === "1") {
          url = defaultConstant.POOL_ORDER_DETAIL_US_URL;
        } else {
          url = defaultConstant.POOL_ORDER_DETAIL_SV_URL;
        }
        var query = {};
        query.q = 'OrdschId=' + params.ordValue;
        Store.set({
          name: name,
          value: { isLoading: true }
        });
        return $http(
          {
            url: defaultConstant.ENDPOINT_URL + url,
            params: query
          })
        .then(function(response) {
          var data = response.data;
          Store.set({
            name: name,
            value: { items: data.items }
          });
          return data.items;
        }).catch(function() {
          Store.set({
            name: name,
            value: { message: 'Error loading results' }
          });
        })
        .finally(function() {
          Store.set({
            name: name,
            value: { isLoading: false }
          });
        });
      },
      init: function(name, params) {
        return this.findAll(name, params);
      },
      update: function(name, params) {
        return this.findAll(name, params);
      }
    });
  }
]);
