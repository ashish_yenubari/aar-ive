'use strict';

angular.module('app.services').factory('AzureAuth', [
  '$http',
  'defaultConstant',
  '$cookies',
  '$window',
  '$document',
  'AclService',
  function(
    $http,
    defaultConstant,
    $cookies,
    $window,
    $document,
    AclService
  ) {
    return {
      setUserId: function() {
        var hostUrl = $window.location.protocol + "//" + $window.location.host;
        var reqUrl = hostUrl + "/.auth/me";
        return $http.get(reqUrl, {
          headers: $document.cookie
        }).then(function(response) {
          if (response.status === 200) {
            var userId = angular.fromJson(response.data)[0].user_id;
            var jwtToken = angular.fromJson(response.data)[0].id_token;
            $cookies.put(defaultConstant.USERID_KEY, userId);
            $cookies.put(defaultConstant.AUTH_KEY, jwtToken);
            // TODO: Need to set the user roles from jwtToken
            AclService.attachRole('creator');
          }
        });
      },

      unsetUserId: function() {
        $cookies.remove(defaultConstant.USERID_KEY);
        $cookies.remove(defaultConstant.AUTH_KEY);
      }
    };
  }
]);
