angular.module('app.services').factory('EmailApi', [
  '$http',
  'defaultConstant',
  '$cookies',
  function($http,
           defaultConstant,
           $cookies) {
    return {
      send: function(params) {
        var formData = new FormData();
        formData.append("data", angular.toJson(params.data));

        if (params.files) {
          for (var i = 0; i < params.files.length; i++) {
            if (params.files[i]) formData.append("file", params.files[i]);
          }
        }

        // Specify receiver in the defaultConstant or send params
        if (params.senderInfo) {
          this.senderInfo = params.senderInfo;
        } else {
          this.senderInfo = defaultConstant.MAIL.SENDER_INFO;
        }
        formData.append("senderInfo", this.senderInfo);

        if (params.receiverInfo) {
          this.receiverInfo = params.receiverInfo;
        } else {
          this.receiverInfo = defaultConstant.MAIL.RECEIVER_INFO;
        }
        formData.append("receiverInfo", this.receiverInfo);

        if (params.emailSubject) {
          this.emailSubject = params.emailSubject;
        } else {
          this.emailSubject = defaultConstant.MAIL.EMAIL_SUBJECT;
        }
        formData.append("subject", this.emailSubject);

        return $http(
          {
            method: 'POST',
            headers: {
              'Content-Type': undefined,
              'AUTH-TOKEN': $cookies.get(defaultConstant.AUTH_KEY)
            },
            transformRequest: angular.identity,
            transformResponse: angular.identity,
            url: defaultConstant.ENDPOINT_WS_URL + defaultConstant.NEW_POOL_ORDER_URL,
            data: formData
          })
          .then(function(response) {
            return response;
          });
      }
    };
  }
]);
