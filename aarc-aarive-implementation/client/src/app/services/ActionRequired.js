angular.module('app.services').factory('ActionRequired', [
  '$http',
  'defaultConstant',
  function(
    $http,
    defaultConstant
  ) {
    return {
      submitAction: function(params) {
        var data = {};
        switch (params.action) {
          case 'Airway_Bill':
            data = {
              /* eslint-disable camelcase */
              name: 'procUnserviceableShipment',
              parameters: [
                {ordsch_id: params.data.ordsch_id},
                {imops_customer_no: "12022"},
                {imops_div_no: "1"},
                {ship_date: params.data.ship_date},
                {waybill: params.data.waybill},
                {carrier: params.data.carrier},
                {carrier_method: params.data.carrier_method}
              ]
            };
            break;
          case 'Off_Core':
            data = {
              name: 'procReceiveCore',
              parameters: [
                {ordsch_id: params.data.ordsch_id},
                {imops_customer_no: "12022"},
                {imops_div_no: "1"},
                {part: params.data.part},
                {serial_no: params.data.serial_no},
                {tail_number: params.data.tail_number},
                {quantity: "1"},
                {removal_reason: params.data.removal_reason},
                {time_since_new: params.data.time_since_new},
                {cycles_since_new: params.data.cycles_since_new},
                {days_since_new: params.data.days_since_new},
                {time_since_repair: params.data.time_since_repair},
                {cycles_since_repair: params.data.cycles_since_repair},
                {days_since_repair: params.data.days_since_repair},
                {time_since_overhaul: params.data.time_since_overhaul},
                {cycles_since_overhaul: params.data.cycles_since_overhaul},
                {days_since_overhaul: params.data.days_since_overhaul},
                {time_since_install: params.data.time_since_install},
                {cycles_since_install: params.data.cycles_since_install},
                {days_since_install: params.data.days_since_install},
                {time_remaining: params.data.time_remaining},
                {cycles_remaining: params.data.cycles_remaining}
              ]
            };/* eslint-enable camelcase */
            break;
          default :break;
        }
        return $http(
          {
            method: 'POST',
            url: defaultConstant.ENDPOINT_URL + defaultConstant.POOL_ORDER_LIST_URL,
            data: data
          })
          .then(function(response) {
            var data = response.data;
            return data;
          });
      }
    };
  }
]);
