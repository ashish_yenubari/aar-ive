/**
 * @ngdoc service
 * @name Store
 * @description
 *
 * The `Store` service provides a way to manage global state, with a single source of authority,
 * similar to Redux but without many features, such as immutability. However, Store is also simpler
 * to configure and use, especially for simple use-cases.
 */

angular.module('app.services').factory('Store', ['$timeout', 'hashCode', function($timeout, hashCode) {
  var store = {};
  var bindings = {};
  var debouncing = {};

  return {
    /**
     * Sets/updates data in the global store for a single key.
     *
     * This also triggers an asynchronous update for all scopes with subscriptions/bindings to this key.
     *
     * @param {string|object} name|options - If name, an arbitrary assigned globally unique key for a state variable.
     * @param {mixed} [val] - a mixed type variable to store and publish, required unless "options" is the first argument
     * @param {object} [options] The keys which can be included are:<br>
     *   - name: Required and used only if "options" is the first argument<br>
     *   - val: Required and used only if "options" is the first argument
     */
    set: function(name, val, options) {
      options = options || {};
      if (angular.isObject(name)) {
        options = name;
        name = options.name;
        val = options.value;
      }

      // If existing data is an object, it should not be overwritten with anything else
      var existingVal = this.get(name);
      if (angular.isObject(existingVal)) {
        if (!angular.isObject(val)) {
          console.warn('Store value for ' + name + ' is an object, and cannot not be overwritten by a non-object');
          return;
        } else if (!angular.isArray(existingVal) !== !angular.isArray(val)) {
          console.warn('Store value for ' + name + ' cannot not be changed between a hash and an array');
          return;
        }

        if (!angular.isArray(existingVal)) {
          var data = angular.copy(existingVal);
          angular.forEach(val, function(item, i) {
            data[i] = item;
          });
          store[name] = data;
          this.publish(name);
          return;
        }
      }

      store[name] = val;
      this.publish(name);
    },

    /**
     * Gets data from the global store.
     *
     * Note that this should be used sparingly. The data is retrieved synchronously, with no data
     * binding. Most components should use Store.subscribe to continuously bind to the store.
     *
     * @param {string} name An arbitrary assigned globally unique key for a state variable.
     * @return {mixed} The current state value for this store key.
     */
    get: function(name) {
      return store[name];
    },

    /**
     * Updates all the scopes (components, services, etc) which are subscribed to a store key.
     *
     * This normally called internally when data is set, and when a new subscription is initiated.
     * The update is debounced (currently with 0 ms delay) so that multiple calls to publish within
     * the current digest are not processed more than once, however this also means that publish is
     * asynchronous, as are methods which rely on it such as `set` and `subscribe`.
     *
     * @param {string} name An arbitrary assigned globally unique key for a state variable.
     */
    publish: function(name) {
      // Exit if there are no bindings for this name
      if (!bindings[name]) return;

      // Debounce
      if (debouncing[name]) {
        $timeout.cancel(debouncing[name]);
      }
      debouncing[name] = $timeout(function() {
        angular.forEach(bindings[name].bindings, function(binding, i) {
          var val = angular.copy(store[name]);
          binding.scope[name] = val;

          if (binding.onDataChanged) {
            binding.onDataChanged(val);
          }
        });
      }, 0);
    },

    /**
     * Initializes or updates a data binding between an object (component scope, service, etc) and a
     * global store variable.
     *
     * In almost all cases, this will be the method to call when you need data from the Store.
     *
     * This also triggers an asynchronous update for all scopes with subscriptions/bindings to this key.
     *
     * @param {string|object} name|options - If name, an arbitrary assigned globally unique key for a state variable.
     * @param {object} [scope] - an object to publish changes to the store value to, required unless "options" is the first argument
     * @param {object} [options] The keys which can be included are:<br>
     *   - name: Required and used only if "options" is the first argument<br>
     *   - scope: Required and used only if "options" is the first argument<br>
     *   - params: parameters to be passed to the handler<br>
     *   - onBindInit: a method to call after a new subscription has been established<br>
     *   - onBindUpdate: a method to call when a subscription is updated<br>
     *   - onDataChanged: a method to call anytime the store value changes. Normally this isn't
     *     necessary with a nested component tree and one-way bindings, because Angular takes care
     *     of updates and calls $onChanges in the child component where the value is used.
     */
    subscribe: function(name, scope, options) {
      options = options || {};
      if (angular.isObject(name)) {
        options = name;
        name = options.name;
        scope = options.scope;
      }

      // Validate required arguments
      if (!angular.isString(name)) {
        console.warn('Cannot call subscribe without a string name to bind data to');
        return;
      } else if (!angular.isObject(scope)) {
        console.warn('Cannot call subscribe without a object scope to bind data to');
        return;
      }

      // Add an $onDestroy handler to the subscribing controller, wrapping the existing one, if any
      if (!scope.$origOnDestroy) {
        scope.$origOnDestroy = (scope.$onDestroy) ? scope.$onDestroy : angular.noop;
        var self = this;
        scope.$onDestroy = function() {
          self.unsubscribe(this);
          scope.$origOnDestroy();
        };
      }

      var params = options.params;

      var isNewBinding = false;
      var hasNewParams = false;
      var oldParams = false;

      if (!bindings[name]) {
        bindings[name] = {
          params: params,
          onBindInit: options.onBindInit,
          onBindUpdate: options.onBindUpdate,
          bindings: []
        };
        isNewBinding = true;
      } else {
        var oldParamHash = hashCode(bindings[name].params);
        var newParamHash = hashCode(params);
        if (oldParamHash !== newParamHash) {
          oldParams = angular.copy(bindings[name].params);
          bindings[name].params = params;
          bindings[name].onBindInit = options.onBindInit;
          bindings[name].onBindUpdate = options.onBindUpdate;
          hasNewParams = true;
        }
      }

      // Is there already a binding for this scope?
      angular.forEach(bindings[name].bindings, function(existingBinding, i) {
        if (scope === existingBinding.scope) {
          bindings[name].bindings.splice(i, 1);
        }
      });

      // Add this binding
      bindings[name].bindings.push({
        scope: scope,
        onDataChanged: options.onDataChanged
      });

      // Initialize via an onBindInit subscription lifecycle callback if this is a new subscription,
      // or via onBindUpdate if these are new params for an existing subscription. The parameters
      // are copied so that these callbacks cannot accidentally change the parameters that were
      // registered via subscribe().
      var newParams = angular.copy(params);
      if (isNewBinding && options.onBindInit) {
        options.onBindInit(name, newParams);
      } else if (hasNewParams && options.onBindUpdate) {
        options.onBindUpdate(name, newParams, oldParams);
      }

      // Publish to this scope so it will have the current value immediately
      this.publish(name);
    },

    /**
     * Removes one or all bindings for a given scope.
     *
     * This should be called from a component's $onDestroy lifecycle method for any component that
     * calls Store.subscribe.
     *
     * @param {object} scope The scope that has (or may have) Store bindings to remove.
     * @param {string} [name] An arbitrary assigned globally unique key for a state variable.
     */
    unsubscribe: function(scope, name) {
      angular.forEach(bindings, function(bindingsByName, i) {
        if (!name || i === name) {
          var j = bindingsByName.bindings.length;
          while (j--) {
            if (bindingsByName.bindings[j].scope === scope) {
              bindings[i].bindings.splice(j, 1);
            }
          }
        }
      });
    },

    /**
     * Configures a factory service to use a standardized implementation of the Store.
     *
     * Handlers can be used to add async data loading and binding functionality.
     *
     * Handlers should be factory services with any/all of the following methods:
     *
     *   - init: called the first time the subscription <-> param set is subscribed to.<br>
     *
     *   - update: called when subscriptions are updated with new parameters.<br>
     *     (init and update are the places to handle asynchronous behaviors, i.e. side effects)<br>
     *
     * @param {object} handler The handler object definition, as returned in a factory service.
     * @return {object} handler An extended handler object definition, with added Store methods.
     */
    createHandler: function(handler) {
      var self = this;
      handler.subscribe = function() {
        // The options are passed to Store.subscribe as first (and only) or third argument. But
        // options may also be ommited, so we might have to add it
        var args = Array.prototype.slice.call(arguments);
        var index = (angular.isString(args[0])) ? 2 : 0;
        while (index > (args.length - 1)) {
          args.push({});
        }
        angular.extend(args[index], {
          onBindInit: (handler.init) ? handler.init.bind(handler) : angular.noop,
          onBindUpdate: (handler.update) ? handler.update.bind(handler) : angular.noop
        });
        self.subscribe.apply(self, args);
      };
      handler.unsubscribe = function() {
        self.unsubscribe.apply(self, arguments);
      };
      return handler;
    }
  };
}]);
