
angular.module('app.services').factory('PoolOrderCount', [
  '$http',
  'defaultConstant',
  'PoolOrderFilters',
  'Store',
  function(
    $http,
    defaultConstant,
    PoolOrderFilters,
    Store
  ) {
    return Store.createHandler({
      findAll: function(name, params) {
        var countType = 'ALL';
        switch (params.countType) {
          case 'Tail':
            countType = 'TAIL';
            break;
          case 'CustPo':
            countType = 'CUSTOMER_PO';
            break;
          case 'Part':
            countType = 'PART';
            break;
          case 'SerialNumber':
            countType = 'SERIAL_NO';
            break;
          case 'PartDest':
            countType = 'PART_DEST';
            break;
          default:
            countType = 'ALL';
            break;
        }
        if (!params.countValue) {
          params.countValue = '*';
        }
        var data = {
          name: 'executeCount',
          parameters: [
            {
              countTypeVar: countType
            }, {
              countValueVar: '*' + params.countValue.toUpperCase() + '*'
            }
          ]
        };
        Store.set({
          name: name,
          value: { isLoading: true }
        });
        return $http(
          {
            method: 'POST',
            headers: {'Content-Type': 'application/vnd.oracle.adf.action+json'},
            url: defaultConstant.ENDPOINT_URL + defaultConstant.POOL_ORDER_COUNT_URL,
            data: data
          })
          .then(function(response) {
            var data = response.data.result;
            var priorities = data.replace(/ /g, '').split('|');
            var self = this;

            PoolOrderFilters.subscribe('filterTabs', self, {
              onDataChanged: function(filters) {
                // TODO: Refactor away the unsubscribe here, or document why it's necessary
                Store.unsubscribe(self, 'filterTabs');

                // Initialize counts for all filter priorities
                var counts = {};
                angular.forEach(filters.items, function(item) {
                  counts[item.key] = {
                    total: 0,
                    actionRequired: 0
                  };
                });

                // Set the counts for each of the matching priorities returned from the count API
                angular.forEach(priorities, function(priority) {
                  var parts = priority.split(',');
                  if (parts[0]) {
                    if (counts[parts[0].toLowerCase()]) {
                      counts[parts[0].toLowerCase()] = {
                        total: Number(parts[1]),
                        actionRequired: Number(parts[2])
                      };
                    }
                  }
                });

                // Create count summaries for "all" and "action required" tabs
                angular.forEach([
                  ['all', defaultConstant.EXCLUDE_FROM_POOL],
                  ['actionRequired', []]
                ], function(summarize) {
                  // Compile items to total
                  var items = [];
                  angular.forEach(counts, function(count, key) {
                    if (summarize[1].indexOf(key) === -1) items.push(counts[key]);
                  });

                  // Add up total counts
                  if (items.length > 0) {
                    // 'ALL' needs to have zero action required as it is getting added up in the total of 'Action Required'
                    if (summarize[0].toLowerCase() === 'all') {
                      counts[summarize[0]] = items.reduce(function(last, next) {
                        return {
                          total: last.total + next.total,
                          actionRequired: 0
                        };
                      });
                    } else {
                      counts[summarize[0]] = items.reduce(function(last, next) {
                        return {
                          total: last.total + next.total,
                          actionRequired: last.actionRequired + next.actionRequired
                        };
                      });
                    }
                  } else {
                    counts[summarize[0]] = { actionRequired: 0, total: 0 };
                  }
                });

                // For action required, it's the actionRequired count we want to see
                if (counts.actionRequired) {
                  counts.actionRequired.total = counts.actionRequired.actionRequired;
                }

                // Pool history shouldn't have a count
                if (counts.poolHistory) {
                  delete counts.poolHistory;
                }

                Store.set({
                  name: name,
                  value: counts
                });
              }
            });
          }).catch(function() {
            Store.set({
              name: name,
              value: { message: 'Error loading results' }
            });
          })
          .finally(function() {
            Store.set({
              name: name,
              value: { isLoading: false }
            });
          });
      },
      init: function(name, params) {
        return this.findAll(name, params);
      },
      update: function(name, params) {
        return this.findAll(name, params);
      }
    });
  }
]);
