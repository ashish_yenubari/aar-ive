angular.module('app.services').factory('DocUpload', [
  '$http',
  'defaultConstant',
  function(
    $http,
    defaultConstant
  ) {
    return {
      upload: function(params) {
        var file = params.media;
        var data = {
          name: "uploadDocument",
          parameters: [
            {
              id: params.orderId
            },
            {
              docType: params.docType
            },
            {
              file: file
            },
            {
              fileName: params.fileName
            }
          ]
        };

        return $http(
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/vnd.oracle.adf.action+json'
            },
            url: defaultConstant.ENDPOINT_URL + defaultConstant.DOC_UPLOAD_URL,
            data: data
          })
          .then(function(response) {
            var data = response.data.result;
            return data;
          });
      }
    };
  }
]);
