/* global download:true */

angular.module('app.services').factory('BlobDownload', [
  '$http',
  'defaultConstant',
  function(
    $http,
    defaultConstant
  ) {
    return {
      download: function(item) {
        var query = {};
        query.q = 'BlobId=' + item.BlobId;
        return $http(
          {
            url: defaultConstant.ENDPOINT_URL + defaultConstant.BLOB_DOWNLOAD_URL,
            params: query
          })
          .then(function(response) {
            var data = response.data || {};
            var ref = '';
            angular.forEach(data.items, function(item) {
              angular.forEach(item.links, function(link) {
                if (link.rel === 'enclosure') {
                  ref = link.href;
                }
              });
            });
            return $http(
              {
                url: defaultConstant.ENDPOINT_URL + ref.substring(ref.indexOf(defaultConstant.BLOB_DOWNLOAD_URL)),
                responseType: 'arraybuffer'
              })
              .then(function(response) {
                download(response.data, item.FileName);
              });
          });
      }
    };
  }
]);
