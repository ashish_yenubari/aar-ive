angular.module('app.components').component('dashboard', {
  templateUrl: 'dashboard/dashboard.html',
  // TODO: Remove or document unused dependencies
  controller: [
    '$state',
    '$stateParams',
    '$transitions',
    '$timeout',
    'defaultConstant',
    'Store',
    'PoolOrderCount',
    'PoolOrderList',
    'PoolOrderFilters',
    function(
      $state,
      $stateParams,
      $transitions,
      $timeout,
      defaultConstant,
      Store,
      PoolOrderCount,
      PoolOrderList,
      PoolOrderFilters
    ) {
      var self = this;
      self.flag = true;
      var scrollPositions = {};

      this.setSort = function(sortCol, sortAsc) {
        self.sortCol = sortCol;
        self.sortAsc = sortAsc;
        self.onStoreChange();
      };

      this.$onInit = function() {
        self.sortCol = defaultConstant.DEFAULT_SORT_COL;
        self.sortAsc = defaultConstant.DEFAULT_SORT_ASC;

        Store.subscribe({
          name: 'inSearchMode',
          scope: this
        });

        var setOverlay = function() {
          if (self.isPoolOrderDetailsOpen || self.isActionRequiredOpen) {
            Store.set('hasOverlay', true);
          } else {
            Store.set('hasOverlay', false);
          }
        };

        Store.subscribe({
          name: 'isPoolOrderDetailsOpen',
          scope: this,
          onDataChanged: setOverlay
        });

        Store.subscribe({
          name: 'isActionRequiredOpen',
          scope: this,
          onDataChanged: setOverlay
        });

        Store.subscribe({
          name: 'hasOverlay',
          scope: this
        });

        Store.subscribe({
          name: 'searchType',
          scope: this
        });

        Store.subscribe({
          name: 'searchValue',
          scope: this
        });

        Store.subscribe({
          name: 'tabName',
          scope: this,
          onDataChanged: this.onStoreChange
        });

        PoolOrderFilters.subscribe('filterTabs', self, {
          onDataChanged: function() {
            // TODO: Refactor away the unsubscribe here, or document why it's necessary
            Store.unsubscribe(self, 'filterTabs');
            self.onStoreChange();
          }
        });

        // Reset the filters (go to the "all" tab) when clicking on the logo
        $transitions.onSuccess({ to: 'root.dashboard' }, function() {
          Store.set({ name: 'tabName', value: 'all' });
          Store.set({ name: 'searchType', value: null });
          Store.set({ name: 'searchValue', value: null });
        });
      };

      this.onStoreChange = function() {
        // Debounce
        if (self.onStoreChanging) {
          $timeout.cancel(self.onStoreChanging);
        }
        self.onStoreChanging = $timeout(self.subscribePoolOrders, 0);
      };

      this.subscribePoolOrders = function() {
        if (!self.filterTabs) return;

        if (!self.tabName) {
          self.tabName = 'all';
        }

        // Get the tab index from the current tab (filter) information (from the URL)
        angular.forEach(self.filterTabs.items, function(item, i) {
          if (item.key === self.tabName) {
            self.tabIndex = i;
            self.tabFilter = item;
          }
        });

        var countParams = {
          countType: (self.searchType) ? self.searchType : 'ALL'
        };

        if (self.searchValue) countParams.countValue = self.searchValue;
        PoolOrderCount.subscribe('counts', self, {
          params: countParams
        });

        PoolOrderList.subscribe({
          name: self.tabName,
          scope: self,
          params: {
            priority: (self.tabFilter) ? self.tabFilter.key : null,
            search: (self.searchValue) ? {
              type: self.searchType,
              value: self.searchValue
            } : null,
            sortCol: self.sortCol,
            sortAsc: self.sortAsc,
            limit: (scrollPositions[self.tabName] || 0) + 20
          }
        });
      };

      this.onTabChange = function(tab) {
        var state = (self.searchValue) ? 'root.dashboard.tab.search' : 'root.dashboard.tab';
        $state.go(state, { tabName: self.filterTabs.items[tab.index].key });
      };

      this.handleScroll = function(tabName, index) {
        scrollPositions[tabName] = index;
        this.onStoreChange();
      };

      // To get the tabs count from all the tabs
      self.handleTabUpdate = function(count) {
        // self.dataCount = count;
      };
    }
  ]
});
