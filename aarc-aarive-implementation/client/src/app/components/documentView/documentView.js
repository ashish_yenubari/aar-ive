angular.module('app.components').component('documentView', {
  bindings: {
    items: '<'
  },
  templateUrl: 'documentView/documentView.html',
  controller: ['$window', 'defaultConstant', 'DocList', 'DocUpload', 'BlobDownload', 'AclService',
    function($window, defaultConstant, DocList, DocUpload, BlobDownload, AclService) {
      var self = this;

      this.$onInit = function() {
        // Document upload should be disabled in read-only mode
        self.can = AclService.can;
        self.actionAddCustomerDocuments = defaultConstant.ACTION_ADD_CUSTOMER_DOCUMENTS;

        DocList.subscribe('customerDocData', this, {
          params: { OrditmId: this.items.OrditmId, Type: 'C' }
        });

        if ($window.File && $window.FileReader && $window.FileList && $window.Blob) {
          var el = angular.element('#filePicker');
          el.on('change', self.fileUpload);
        } else {
          console.error('File API not available.');
        }
      };

      this.fileUpload = function(evt) {
        var files = evt.target.files;
        var file = files[0];

        if (files && file) {
          var reader = new FileReader();

          reader.onload = function(readerEvt) {
            var binaryString = readerEvt.target.result;
            // console.log(btoa(binaryString));
            var fileContentIndex = binaryString.lastIndexOf(',');
            binaryString = binaryString.substring(fileContentIndex + 1);
            if (binaryString) {
              self.uploadMessage = '';
              self.uploadPending = true;
              DocUpload.upload(
                {
                  media: binaryString,
                  orderId: self.items.OrditmId.toString(),
                  fileName: file.name,
                  docType: 'C'
                })
                .then(function(msg) {
                  DocList.subscribe('customerDocData', self, {
                    // HOTFIX - RANDOM TO REFRESH THE LIST OF DOC // NEED TO REWORK //
                    params: { OrditmId: self.items.OrditmId, Type: 'C', Refresh: Math.random() }
                  });

                  if (msg.indexOf('ERR(90050)') > -1) {
                    self.uploadMessage = 'Attachment not uploaded due to unsupported file type.';
                  }
                })
                .catch(function() {
                  self.uploadMessage = 'Error uploading file!';
                })
                .finally(function() {
                  self.uploadPending = false;
                });
            }
          };
          reader.readAsDataURL(file);
        }
      };

      this.removeDoc = function(x) {
        this.customerDocData.items.splice(x, 1);
      };

      this.openDoc = function(item) {
        BlobDownload.download(item);
      };
    }
  ]
});
