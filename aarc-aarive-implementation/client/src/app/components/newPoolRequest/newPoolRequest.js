angular.module('app.components').component('newPoolRequest', {
  templateUrl: 'newPoolRequest/newPoolRequest.html',
  controller: [
    '$timeout',
    '$state',
    '$sce',
    'setFocus',
    'NewPoolOrder',
    'TimeZones',
    'ListOfValues',
    'PoolOrderFilters',
    'PartVerification',
    function(
      $timeout,
      $state,
      $sce,
      setFocus,
      NewPoolOrder,
      TimeZones,
      ListOfValues,
      PoolOrderFilters,
      PartVerification
    ) {
      var self = this;
      this.newPoolRequestModel = {};
      this.$onInit = function() {
        self.timeZones = TimeZones.timeZones;
        self.newPoolRequestModel.timeReqByTimeZone = self.timeZones[0];
        self.modal = {};
        self.modal.showModal = false;
        self.modal.closeAndGoBack = self.closeAndGoBack;
        self.priorityCodeRPL = "RPL";
        self.priorityCodeDPA = "DPA";
        // self.newPoolRequestModel.senderInfo = "";
        // self.newPoolRequestModel.receiverInfo = "";
        self.isDST = moment().isDST();
        var currentUTCOffset = 0;
        self.isPartVeriLoading = false;

        var apiToCall = {
          newPoolTailLov: {lovType: 'Tail_Lov', callBackMethod: self.tailCallBack}
          /* ,
           newPoolAtaLov: {lovType: 'Ata_Lov', callBackMethod: self.ataCallBack}*/
        };
        angular.forEach(apiToCall, function(value, key) {
          ListOfValues.subscribe(key, self, {
            params: {lov: value.lovType},
            onDataChanged: value.callBackMethod
          });
        });

        try {
          currentUTCOffset = moment().utcOffset(moment().format())._offset / 60;
        } catch (err) {
          currentUTCOffset = 0;
        }

        for (var i = self.timeZones.length - 1; i >= 0; i--) {
          if (angular.isDefined(self.timeZones[i].isDST) && self.timeZones[i].isDST !== self.isDST) {
            self.timeZones.splice(i, 1);
          } else if (self.timeZones[i].offset === currentUTCOffset) {
            self.newPoolRequestModel.timeReqByTimeZone = self.timeZones[i];
          }
        }

        PoolOrderFilters.subscribe('filters', self, {
          onDataChanged: self.callBackFilter
        });
      };

      this.openDatePicker = function(id) {
        setFocus(id);
      };

      self.autoCompleteOptions = {
        minimumChars: 3,
        data: function(term) {
          self.isPartVeriLoading = true;
          self.partNumberDetails = {};
          return PartVerification
            .verify({ partNumber: term, acTail: self.newPoolRequestModel.acTail})
            .then(function(result) {
              if (result && result.length === 1 && result[0].Part === term) {
                self.partNumberDetails = {
                  items: { partName: result[0].Description, um: result[0].Um, isValid: true }, isLoading: false,
                  pspecId: result[0].PspecId };

                self.setPriorityLov({ lov: 'Priority_Lov', PspecKnown: 'Y', Tail: self.newPoolRequestModel.acTail,
                  PspecId: self.partNumberDetails.pspecId});
                self.setInstSerialLov({lov: 'Inst_Serial_Lov', part: self.newPoolRequestModel.part});
              } else if (!result || result.length === 0) {
                self.partNumberDetails = { items: { partName: 'Cannot verify', isValid: false }, isLoading: false};
                self.setPriorityLov({lov: 'Priority_Lov', PspecKnown: 'N'});
                self.setInstSerialLov({lov: 'Inst_Serial_Lov', part: self.newPoolRequestModel.part});
              } else {
                return result;
              }
            })
            .finally(function() {
              self.isPartVeriLoading = false;
              angular.element("#part").focus();
            });
        },
        renderItem: function(item) {
          return {
            value: item.Part,
            label: $sce.trustAsHtml(
              "<table class='auto-complete'>" +
              "<tbody>" +
              "<tr>" +
              "<td style='width: 90%'>" + item.Part + "</td>" +
              "</tr>" +
              "</tbody>" +
              "</table>")
          };
        },
        itemSelected: function(item) {
          self.partNumberDetails = {};
          self.partNumberDetails.items = {partName: item.item.Description, um: item.item.Um, isValid: true};
          self.partNumberDetails.isLoading = false;
          self.partNumberDetails.pspecId = item.item.PspecId;
          self.setPriorityLov({ lov: 'Priority_Lov', PspecKnown: 'Y', Tail: self.newPoolRequestModel.acTail,
            PspecId: self.partNumberDetails.pspecId});
          self.setInstSerialLov({lov: 'Inst_Serial_Lov', part: self.newPoolRequestModel.part});
        }
      };

      this.setPriorityLov = function(params) {
        ListOfValues.subscribe('newPoolPriorityLov', self, {
          params: params,
          onDataChanged: self.priorityCallBack
        });
      };

      this.setInstSerialLov = function(params) {
        ListOfValues.subscribe('newPoolInstSerialLov', self, {
          params: params,
          onDataChanged: self.instSerialCallBack
        });
      };

      this.closeNewPoolRequestPage = function() {
        self.modal = {};
        self.modal.showModal = true;
        self.modal.exclamation = true;
        self.modal.title = "Are you sure you want to close?";
        self.modal.isConfModal = true;
        self.modal.closeAndGoBack = self.closeAndGoBack;
      };

      this.closeAndGoBack = function() {
        self.modal = {};
        setTimeout(function() {
          history.back();
        }, 0);
      };

      this.validateTime = function() {
        var timeReqBy = this.newPoolRequestModel.timeReqBy;
        if (timeReqBy === '') {
          this.newPoolRequestModel.timeReqBy = "00:00";
        } else if (timeReqBy.indexOf(':') < 0 && timeReqBy.indexOf('.') < 0) {
          if (timeReqBy < 10) {
            this.newPoolRequestModel.timeReqBy = "0" + Math.floor(timeReqBy) + ":00";
          } else if (timeReqBy > 10 && timeReqBy < 24) {
            this.newPoolRequestModel.timeReqBy = Math.floor(timeReqBy) + ":00";
          } else if (timeReqBy > 24) {
            if (timeReqBy.length === 2) {
              this.newPoolRequestModel.timeReqBy = "00:00";
            } else {
              var timeHours = timeReqBy.substring(0, 2);
              var timeMinutes = timeReqBy.substring(2, 4);
              if (timeReqBy.length === 3) {
                this.newPoolRequestModel.timeReqBy = timeHours + ":0" + timeMinutes;
              } else {
                this.newPoolRequestModel.timeReqBy = timeHours + ":" + timeMinutes;
              }
            }
          }
        } else {
          if (timeReqBy.indexOf('.'))
            timeReqBy = timeReqBy.replace(".", ":");
          var sHours = timeReqBy.split(':')[0];
          var sMinutes = timeReqBy.split(':')[1];

          if (sHours === "" || isNaN(sHours) || parseInt(sHours, 10) > 23 || parseInt(sHours, 10) === 0) {
            sHours = "00";
          } else if (sHours < 10) {
            sHours = "0" + Math.floor(sHours);
          } else {
            sHours = Math.floor(sHours);
          }
          if (sMinutes === "" || isNaN(sMinutes) || parseInt(sMinutes, 10) > 59 || parseInt(sMinutes, 10) === 0) {
            sMinutes = "00";
          } else if (sMinutes < 10) {
            sMinutes = "0" + Math.floor(sMinutes);
          } else {
            sMinutes = Math.floor(sMinutes);
          }
          this.newPoolRequestModel.timeReqBy = sHours + ":" + sMinutes;
        }
      };
      this.oldTimeZone = '';
      this.setContractualDelivery = function() {
        if (!self.newPoolRequestModel.priority || !self.newPoolRequestModel.priority.priorityCode ||
            !self.newPoolPriorityLov.items) {
          self.newPoolRequestModel.contractualDelivery = '';
          self.newPoolRequestModel.dateReqBy = '';
          self.newPoolRequestModel.timeReqBy = '';
          self.newPoolDestLov = '';
          self.newPoolRequestModel.svPartDestination = '';
        } else {
          if (moment(self.newPoolRequestModel.dateReqBy + " " +
                  self.newPoolRequestModel.timeReqBy).isValid() &&
              moment(self.newPoolRequestModel.deliverDateAndTime).format("DD MMM YY HH:mm") !==
              moment(self.newPoolRequestModel.dateReqBy + " " +
                  self.newPoolRequestModel.timeReqBy).format("DD MMM YY HH:mm")) {
            self.newOffset =
              self.newPoolRequestModel.timeReqByTimeZone.offset - angular.toJson(self.oldTimeZone.offset);
          }
          if (self.newPoolRequestModel.priority.priorityCode) { // set dest lov only when priority is set
            ListOfValues.subscribe('newPoolDestLov', self, {
              params: {lov: 'Dest_Lov', Mbk: self.newPoolRequestModel.priority.Mbk},
              onDataChanged: self.destCallBack
            });
          }
          self.newPoolRequestModel.deliverDateAndTime = moment().utc();
          self.newPoolRequestModel.deliverDateAndTime = self.newPoolRequestModel.deliverDateAndTime
              .add(self.newPoolRequestModel.timeReqByTimeZone.offset, 'hours');

          if (self.newPoolRequestModel.priority.hoursToDeliver) {
            self.newPoolRequestModel.deliverDateAndTime = self.newPoolRequestModel.deliverDateAndTime
                .add(self.newPoolRequestModel.priority.hoursToDeliver, 'hours');
          }

          self.newPoolRequestModel.contractualDelivery = self.newPoolRequestModel.deliverDateAndTime
                  .format('DD MMM YYYY - HH:mm') + " " + self.newPoolRequestModel.timeReqByTimeZone.text
                  .substr(self.newPoolRequestModel.timeReqByTimeZone.text.indexOf(")") + 1);
          if (!self.newOffset) {
            self.newPoolRequestModel.dateReqBy =
              moment(self.newPoolRequestModel.deliverDateAndTime).format("DD MMM YY");
            self.newPoolRequestModel.timeReqBy = moment(self.newPoolRequestModel.deliverDateAndTime).format("HH:mm");
          } else {
            var tempDate = moment(self.newPoolRequestModel.dateReqBy + " " +
                self.newPoolRequestModel.timeReqBy).add(self.newOffset, 'hours');
            self.newPoolRequestModel.dateReqBy = tempDate.format("DD MMM YY");
            self.newPoolRequestModel.timeReqBy = tempDate.format("HH:mm");
            self.newOffset = null;
          }
          self.oldTimeZone = self.newPoolRequestModel.timeReqByTimeZone;
        }
      };

      this.tailCallBack = function(value) {
      };

      this.instSerialCallBack = function(value) {
      };

      this.priorityCallBack = function(value) {
        self.newPoolRequestModel.priority = {};
        self.arrangePriority();
        self.setContractualDelivery();
      };

      this.callBackFilter = function(value) {
        self.arrangePriority();
      };

      this.arrangePriority = function() {
        if (self.newPoolPriorityLov && self.filters) {
          var value = angular.copy(self.newPoolPriorityLov.items);
          var tempArr = [];
          angular.forEach(self.filters.items, function(filterObj) {
            angular.forEach(value, function(valueObj, valueIndex) {
              if (valueObj.priorityCode === filterObj.label) {
                tempArr.push(value.splice(valueIndex, 1)[0]);
              }
            });
          });
          // if (value.items.length)
          // tempArr.push.apply(tempArr, value.items.splice(0, value.items.length));
          self.newPoolPriorityLov.items = tempArr;
        }
      };

      this.destCallBack = function(value) {
      };

      this.ataCallBack = function(value) {
      };

      this.verifyPartNumber = function() {
        if (self.newPoolRequestModel.part) {
          self.newPoolRequestModel.part = self.newPoolRequestModel.part.trim();
          if ((!self.partNumberDetails || !self.partNumberDetails.items || !self.partNumberDetails.items.isValid) &&
            !self.isPartVeriLoading) {
            self.partNumberDetails = {};
            self.partNumberDetails.isLoading = false;
            self.partNumberDetails.items = {isValid: false, partName: 'Cannot verify'};
          }

          if (self.newPoolRequestModel.part.length < 3) {
            self.setPriorityLov({lov: 'Priority_Lov', PspecKnown: 'N'});
            self.setInstSerialLov({lov: 'Inst_Serial_Lov', part: self.newPoolRequestModel.part});
          }
        } else {
          self.partNumberDetails = {};
        }
      };

      this.goToDashboard = function() {
        self.modal = {};
        setTimeout(function() {
          $state.go('root.dashboard');
        }, 0);
      };

      this.clearPartDetails = function() {
        self.newPoolRequestModel.part = "";
        self.partNumberDetails = {};
      };

      this.callbackSubmitNewPoolReq = function(value) {
        self.modal = {};
        if (value && !value.toUpperCase().includes('FAIL')) {
          self.modal.checkCircle = true;
          self.modal.title = "Your request has been submitted";
          self.modal.showModal = true;
          $timeout(self.goToDashboard, 3000);
        } else {
          self.modal.exclamation = true;
          self.modal.title = "There was an error with your request";
          self.modal.msg = "Please try again";
          self.modal.isErrorModal = true;
          self.modal.showModal = true;
        }
        self.modal.closeAndGoBack = self.closeAndGoBack;
      };

      this.submitNewPoolReq = function() {
        var deliverDateAndTimeMoment = moment(self.newPoolRequestModel.deliverDateAndTime);
        var dateReqByMoment = moment(self.newPoolRequestModel.dateReqBy, 'DD MMM YY', true);
        var timeReqByMoment = moment(self.newPoolRequestModel.timeReqBy, 'HH:mm', true);
        var isValid = true;
        if (self.newPoolReqForm.$valid) {
          // if ((!self.newPoolRequestModel.ataChapter) &&
          if ((!self.newPoolRequestModel.ataChapter || !self.newPoolRequestModel.ataChapter.trim()) &&
              (!self.partNumberDetails || !self.partNumberDetails.items ||
              self.partNumberDetails.items.isValid === false)) {
            isValid = false; // ataChapter is required
          } else if (self.newPoolRequestModel.priority &&
              (self.newPoolRequestModel.priority.priorityCode === self.priorityCodeRPL ||
              self.newPoolRequestModel.priority.priorityCode === self.priorityCodeDPA) &&
              (!self.newPoolRequestModel.installedSerial)) {
            isValid = false; // Installed Serial is required
          } else if (self.newPoolRequestModel.dateReqBy && !dateReqByMoment.isValid()) {
            isValid = false; // req by date is not valid
          } else if (self.newPoolRequestModel.dateReqBy &&
              !moment(dateReqByMoment.format('MM-DD-YYYY'))
                .isSameOrAfter(deliverDateAndTimeMoment.format('MM-DD-YYYY'))) {
            isValid = false; // req by date should same or after deliver date
            self.modal.msg = "Date/Time Required By must be after Contractual Delivery Date";
          } else if (self.newPoolRequestModel.timeReqBy && !timeReqByMoment.isValid()) {
            isValid = false; // req by time is not valid
          } else if (moment(dateReqByMoment.format('MM-DD-YYYY'))
              .isSame(deliverDateAndTimeMoment.format('MM-DD-YYYY')) &&
              !timeReqByMoment.isSameOrAfter(moment(deliverDateAndTimeMoment.format('HH:mm'), 'HH:mm', true))) {
            isValid = false; // req by time should same or after deliver time
            self.modal.msg = "Date/Time Required By must be after Contractual Delivery Date";
          } else if (!self.newPoolRequestModel.contractualDelivery && (self.newPoolRequestModel.timeReqBy ||
              self.newPoolRequestModel.dateReqBy)) {
            isValid = false;
          }
        } else {
          isValid = false; // required field are missing
        }
        if (!isValid) {
          self.modal.showModal = true;
          self.modal.exclamation = true;
          self.modal.isErrorModal = true;
          self.modal.title = "There was an error submitting your request";
          self.modal.msg = (self.modal.msg) ? self.modal.msg : "Please make sure you fill in all the required fields";
          self.modal.closeAndGoBack = self.closeAndGoBack;
        } else {
          /* eslint-disable camelcase */
          var params = {
            customer_order: self.newPoolRequestModel.customerPO,
            part: self.newPoolRequestModel.part,
            warehouse: (self.newPoolRequestModel.svPartDestination.Shortname) ?
              self.newPoolRequestModel.svPartDestination.Shortname : '',
            priority: (!self.newPoolRequestModel.priority || !self.newPoolRequestModel.priority.priorityCode) ? '' :
              self.newPoolRequestModel.priority.priorityCode,
            serial_no_installed: (!self.newPoolRequestModel.installedSerial) ? '' :
              self.newPoolRequestModel.installedSerial,
            tail: self.newPoolRequestModel.acTail,
            alternate_part: self.newPoolRequestModel.alternatePart || '',
            ata_chapter: self.newPoolRequestModel.ataChapter || '',
            notes: self.newPoolRequestModel.comments || '',
            required_date: dateReqByMoment.format('YYYY-MM-DDTHH:mm:ssz'),
            required_time: self.newPoolRequestModel.timeReqBy,
            required_time_zone: self.newPoolRequestModel.timeReqByTimeZone.name,
            uom: self.partNumberDetails.items.um || ''
          };
          /* eslint-enable camelcase */
          NewPoolOrder.createPool(params).
           then(this.callbackSubmitNewPoolReq);
        }
      };
    }
  ]
});
