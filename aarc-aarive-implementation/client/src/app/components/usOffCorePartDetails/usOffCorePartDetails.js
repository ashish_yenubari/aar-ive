angular.module('app.components').component('usOffCorePartDetails', {
  bindings: {
    items: '<'
  },
  templateUrl: 'usOffCorePartDetails/usOffCorePartDetails.html',
  controller: ['defaultConstant', 'Store', 'DocList', 'BlobDownload', 'PoolOrderDetails', 'AclService',
    function(defaultConstant, Store, DocList, BlobDownload, PoolOrderDetails, AclService) {
      var self = this;
      this.$onInit = function() {
        // In read-only mode, disable action required links
        self.can = AclService.can;
        self.actionActionRequired = defaultConstant.ACTION_ACTION_REQUIRED;
      };

      this.$onChanges = function() {
        PoolOrderDetails.subscribe('usData', this, {
          params: { index: '1', ordValue: this.items.OrdschId}
        });
        // Calling the doc api
        DocList.subscribe('usDocData', this, {
          params: { OrditmId: this.items.OrditmId, Type: 'US'}
        });

        this.needsOffCoreReturn = false;
        this.needsOffCoreAirwayBill = false;
        if (this.items.UsStatus && this.items.ActionRequired === 'Y') {
          if (this.items.UsStatus.indexOf('U/S Requested') === 0) {
            this.needsOffCoreReturn = true;
          } else if (this.items.UsStatus.indexOf('R/O Created') === 0) {
            this.needsOffCoreAirwayBill = true;
          }
        }
      };

      this.openActionRequired = function() {
        Store.set({ name: 'actionPart', value: this.items });
        Store.set({ name: 'isActionRequiredOpen', value: true });
        Store.set({ name: 'isPoolOrderDetailsOpen', value: false });
      };

      this.openDoc = function(item) {
        BlobDownload.download(item);
      };
    }
  ]
});
