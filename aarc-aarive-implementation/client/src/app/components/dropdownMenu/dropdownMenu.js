angular.module('app.components').component('dropdownMenu', {
  bindings: {
    menuItem: "<"
  },
  transclude: true,
  templateUrl: 'dropdownMenu/dropdownMenu.html',
  controller: ['Store', function(Store) {
    this.showDropDown = false;
    var self = this;
    this.$onInit = function() {
      this.menuItem = [
        {
          name: "PO-S5 number",
          value: "CustPo",
          selected: false
        },
        {
          name: "Tail number",
          value: "Tail",
          selected: true
        },
        {
          name: "Part number",
          value: "Part",
          selected: false
        },
        {
          name: "Serial number",
          value: "SerialNumber",
          selected: false
        }
      ];
      self.itemSelected = {
        name: "Tail number",
        value: "Tail",
        selected: true
      };
      Store.set({ name: 'searchType', value: self.itemSelected.value });
    };

    this.selectitem = function(item) {
      self.itemSelected = item;
      item.selected = true;
      for (var i in this.menuItem) {
        if (this.menuItem[i].value !== item.value)
          this.menuItem[i].selected = false;
      }
      if (this.showDropDown === true) {
        this.showDropDown = false;
      }
      Store.set({ name: 'searchType', value: item.value });
    };
    this.clickitem = function() {
      if (this.showDropDown === false) {
        this.showDropDown = true;
      } else {
        this.showDropDown = false;
      }
    };
  }]
});
