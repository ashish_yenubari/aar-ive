angular.module('app.components').component('home', {
  templateUrl: 'home/home.html',
  controller: ['$state', 'defaultConstant', 'PoolOrderCount', 'PoolOrderFilters',
    function($state, defaultConstant, PoolOrderCount, PoolOrderFilters) {
      var self = this;

      this.$onInit = function() {
        self.filterTiles = [];
        PoolOrderFilters.subscribe('filters', this, {
          onDataChanged: function(filters) {
            self.filterTiles = angular.copy(filters.items).filter(function(item) {
              return (defaultConstant.EXCLUDE_FROM_POOL.indexOf(item.key) === -1);
            });
          }
        });

        PoolOrderCount.subscribe('counts', this, {
          params: { countType: 'ALL'}
        });
      };

      this.openDashboard = function(tabName) {
        var state = (tabName) ? 'root.dashboard.tab' : 'root.dashboard';
        $state.go(state, { tabName: tabName, searchType: null, searchValue: null });
      };
    }
  ]
});
