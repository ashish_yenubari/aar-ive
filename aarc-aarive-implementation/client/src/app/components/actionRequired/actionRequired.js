angular.module('app.components').component('actionRequired', {
  templateUrl: 'actionRequired/actionRequired.html',
  controller: ['$state', '$stateParams', '$transitions', '$timeout', 'Store', 'ActionRequiredDetail', '$window',
    function($state, $stateParams, $transitions, $timeout, Store, ActionRequiredDetail, $window) {
      var self = this;

      this.$onInit = function() {
        self.showModal = false;
        self.modal = {};
        self.actionRequired = {};
        self.modal.closeAndGoBack = self.close;

        this.actionType = {
          OFFCORE_AIRWAYBILL: 'Airway_Bill',
          OFFCORE_RETURN: 'Off_Core'
        };

        Store.subscribe({
          name: 'actionPart',
          scope: this,
          onDataChanged: function(part) {
            if (part.UsStatus) {
              if (part.UsStatus.indexOf('U/S Requested') === 0) {
                self.actionRequiredParams = self.actionType.OFFCORE_RETURN;
              } else if (part.UsStatus.indexOf('R/O Created') === 0) {
                self.actionRequiredParams = self.actionType.OFFCORE_AIRWAYBILL;
              }
            }
            ActionRequiredDetail.subscribe({
              name: 'actionRequiredDetails',
              params: {action: self.actionRequiredParams, OrdschId: part.OrdschId},
              scope: this,
              onDataChanged: function(value) {
                self.value = value;
                if (value.items) {
                  var actionRequiredContent = value.items.items[0];
                  self.actionRequired.name = actionRequiredContent.ActionRequiredNm;
                  self.actionRequired.description = actionRequiredContent.Description;
                }
              }
            });
          }
        });
      };

      this.close = function() {
        Store.set({
          name: 'isActionRequiredOpen',
          value: false
        });

        if (Store.get('isResponseSuccess') === true) {
          $window.location.reload();
        }
      };

      this.dispClosePopup = function() {
        self.modal = {};
        self.modal.showModal = true;
        self.modal.exclamation = true;
        self.modal.title = "Are you sure you want to close?";
        self.modal.isConfModal = true;
        self.modal.closeAndGoBack = self.close;
      };
    }
  ]
});
