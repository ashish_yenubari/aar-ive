angular.module('app.components').component('poolOrderList', {
  bindings: {
    count: '<',
    items: '<',
    message: '<',
    isLoading: '<',
    actioncount: '<',
    onScroll: '&',
    onSort: '='
  },
  templateUrl: 'poolOrderList/poolOrderList.html',
  controller: ['Store', 'defaultConstant', 'AclService',
    function(Store, defaultConstant, AclService) {
      var rowHeight = 70; // TODO: Get this dynamically
      var self = this;

      this.$onInit = function() {
        this.defaultSortCol = defaultConstant.DEFAULT_SORT_COL;
        this.defaultSortAsc = defaultConstant.DEFAULT_SORT_ASC;
        this.itemsPadded = [];
        Store.subscribe('part', this);
      };

      this.$onChanges = function() {
        this.updateScroll();
        this.updateItems();
      };

      this.updateItems = function() {
        this.tabCount = this.actioncount;
        this.itemsPadded = [];
        if (this.items && this.items.length > 0) {
          for (var i = 0; i < Math.max(this.count || 0, this.items.length); i++) {
            if (this.items[i]) {
              this.itemsPadded.push(this.items[i]);
            } else {
              this.itemsPadded.push({});
            }
          }
        }
      };

      this.updateScroll = function() {
        this.scrollSliderStyle = {
          transform: 'translateY(' + (this.count * rowHeight - 1) + 'px)'
        };
      };

      this.handleScroll = function(pos) {
        var num = Math.ceil(pos / rowHeight);
        if (self.onScroll) {
          self.onScroll({ index: num });
        }
      };

      this.openPoolOrderDetails = function(item) {
        Store.set({ name: 'part', value: item }); // TODO: Consider renaming to activePart or something to be more clear
        Store.set({ name: 'isPoolOrderDetailsOpen', value: true });
        Store.set({ name: 'isActionRequiredOpen', value: false });
      };

      this.openActionRequired = function(item, $event) {
        // In read-only mode, ignore clicks on the action required icon and allow event propagation
        if (!AclService.can(defaultConstant.ACTION_ACTION_REQUIRED)) return;

        $event.stopPropagation();
        Store.set({ name: 'actionPart', value: item });
        Store.set({ name: 'isActionRequiredOpen', value: true });
        Store.set({ name: 'isPoolOrderDetailsOpen', value: false });
      };
    }
  ]
});
