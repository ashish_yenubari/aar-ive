angular.module('app.components').component('tab', {
  bindings: {
    label: '<',
    count: '<',
    scrollable: '<'
  },
  require: {
    tabs: '^^tabs'
  },
  transclude: true,
  templateUrl: 'tab/tab.html',
  controller: ['$attrs',
    function($attrs) {
      this.$onInit = function() {
        this.updateScrollable();
        this.tabs.addTab(this);
      };

      this.$onChanges = function() {
        this.updateScrollable();
      };

      this.$onDestroy = function() {
        this.tabs.removeTab(this);
      };

      this.updateScrollable = function() {
        // Handle binding to presence of attribute in addition to true/false value
        if ($attrs.scrollable === undefined) {
          this.scrollable = false;
        } else if ($attrs.scrollable === '') {
          this.scrollable = true;
        } else if (this.scrollable === undefined) {
          this.scrollable = false;
        }
      };
    }
  ]
});
