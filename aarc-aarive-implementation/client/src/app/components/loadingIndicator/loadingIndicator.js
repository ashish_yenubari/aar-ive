angular.module('app.components').component('loadingIndicator', {
  bindings: {
    message: '<'
  },
  templateUrl: 'loadingIndicator/loadingIndicator.html',
  controller: [function() {
    this.$onChanges = function() {
      this.loadingMessage = (typeof this.message === 'undefined') ? 'Loading your data' : this.message;
    };
  }]
});
