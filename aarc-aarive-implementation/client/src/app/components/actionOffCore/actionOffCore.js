angular.module('app.components').component('actionOffCore', {
  bindings: {
    items: '<'
  },
  templateUrl: 'actionOffCore/actionOffCore.html',
  controller: ['ActionRequired', 'defaultConstant', '$timeout', 'ListOfValues', 'Store', 'ActionRequiredDetail',
    function(ActionRequired, defaultConstant, $timeout, ListOfValues, Store, ActionRequiredDetail) {
      var self = this;

      this.$onInit = function() {
        self.showModal = false;
        self.modal = {};
        ListOfValues.subscribe('freightForwarderLov', self, {
          params: {lov: 'CarrierMethod_Lov'},
          onDataChanged: self.freightForwarderLovCallback
        });

        ActionRequiredDetail.subscribe({
          name: 'waybillDetails',
          params: {action: 'Airway_Bill', OrdschId: Store.get('actionPart').OrdschId},
          scope: this,
          onDataChanged: function(value) {
            if (value.items) {
              self.resolution = value.items.items[0].Resolution;
            }
          }
        });
      };

      this.offCore = {
        airwayBill: ''
      };

      this.submitOffCore = function() {
        var actionPart = Store.get('actionPart');
        self.sendMessage = '';
        self.sendPending = true;
        self.sendSuccess = false;
        ActionRequired.submitAction({
          action: 'Airway_Bill',
          data: {  /* eslint-disable camelcase */
            customer_order: actionPart.CustPo,
            ordsch_id: actionPart.OrdschId,
            ship_date: actionPart.ShipDate,
            waybill: self.offCore.airwayBill,
            carrier: (!self.offCore.forwarder) ? '' : self.offCore.forwarder.CarrName,
            carrier_method: (!self.offCore.forwarder) ? '' : self.offCore.forwarder.CarrmethName
          }/* eslint-enable camelcase */
        // the below fields are appilicable only when we use email api instead of Action required
        /* ,
          senderInfo: defaultConstant.MAIL.SENDER_INFO,
          receiverInfo: defaultConstant.MAIL.RECEIVER_OFFCORE,
          emailSubject: defaultConstant.MAIL.SUBJECT_OFFCORE
          emailSubject: defaultConstant.MAIL.SUBJECT_OFFCORE */
        })
          .then(function(value) {
            self.sendSuccess = true;
            self.callbackSubmit(value);
          })
          .catch(function() {
            self.callbackSubmit();
          })
          .finally(function() {
            self.sendPending = false;
          });
      };

      this.freightForwarderLovCallback = function(value) {
        if (value.items) {
          angular.forEach(self.freightForwarderLov.items, function(obj, index) {
            if (obj.CarrName === 'OTHER') {
              self.freightForwarderLov.items.splice(index, 1);
              self.freightForwarderLov.items.push(obj);
            }
          });
        }
      };

      this.callbackSubmit = function(value) {
        self.modal = {};
        if (value && value.result === 'Completed Successfully') {
          self.modal.checkCircle = true;
          self.modal.title = "Your request has been submitted";
          self.modal.showModal = true;
          Store.set({ name: 'isResponseSuccess', value: true });
          $timeout(self.closeModal, 3000);
        } else {
          self.modal.exclamation = true;
          self.modal.title = "There was an error with your request";
          self.modal.msg = "Please try again";
          self.modal.isErrorModal = true;
          self.modal.showModal = true;
          Store.set({ name: 'isResponseSuccess', value: false });
        }
      };

      this.closeModal = function() {
        self.modal = {};
      };
    }
  ]
});
