angular.module('app.components').component('svPartDetails', {
  bindings: {
    items: '<'
  },
  templateUrl: 'svPartDetails/svPartDetails.html',
  controller: ['$state', 'PoolOrderDetails', 'DocList', 'BlobDownload',
    function($state, PoolOrderDetails, DocList, BlobDownload) {
      var self = this;
      this.$onChanges = function() {
        if (this.items !== undefined) {
          PoolOrderDetails.subscribe('svData', this, {
            params: { index: '3', ordValue: this.items.OrdschId},
            onDataChanged: self.PoolOrderDtlCallBack
          });

          // Calling the doc api
          DocList.subscribe('aarData', this, {
            params: { OrditmId: this.items.OrditmId, Type: 'SV'}
          });
        }
      };

      this.openDoc = function(item) {
        BlobDownload.download(item);
      };

      this.PoolOrderDtlCallBack = function(value) {
        if (value.items) {
          if (moment(value.items[0].ContractualDelivery).format("HH:mm") === '00:00') {
            self.svData.items[0].ContractualDelivery = moment(value.items[0].ContractualDelivery).format("YYYY-MM-DD");
          } else {
            self.svData.items[0].ContractualDelivery =
               moment(value.items[0].ContractualDelivery).format("YYYY-MM-DD HH:mm");
          }
        }
      };
    }
  ]
});
