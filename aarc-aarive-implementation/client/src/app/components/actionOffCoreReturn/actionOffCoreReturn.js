angular.module('app.components').component('actionOffCoreReturn', {
  bindings: {
    items: '<'
  },
  templateUrl: 'actionOffCoreReturn/actionOffCoreReturn.html',
  controller: ['ActionRequired', 'ActionRequiredDetail', 'Store', 'defaultConstant', '$timeout',
    function(ActionRequired, ActionRequiredDetail, Store, defaultConstant, $timeout) {
      var self = this;
      this.$onChanges = function() {
        self.showModal = false;
        self.modal = {};
      };

      ActionRequiredDetail.subscribe({
        name: 'offCoreReturnDetails',
        params: {action: 'Off_Core', OrdschId: this.items.OrdschId},
        scope: this,
        onDataChanged: function(value) {
          if (value.items) {
            var offCoreData = value.items.items[0];
            self.TailNo = offCoreData.AcTailNum;
            self.PartNo = offCoreData.SvPartNum;
            self.resolution = offCoreData.Resolution;
          }
        }
      });

      this.SerialSub = {
        TSN: '', CSN: '', DSN: '',
        TSO: '', CSO: '', DSO: '',
        TSR: '', CSR: '', DSR: '',
        TSI: '', CSI: '', DSI: '',
        TR: '', CR: '', DR: ''
      };
      this.files = {
        list: []
      };

      this.submitOffCoreReturn = function() {
        if (!self.offCoreReturnForm.$valid) {
          self.modal.showModal = true;
          self.modal.exclamation = true;
          self.modal.isErrorModal = true;
          self.modal.title = "There was an error submitting your request";
          self.modal.msg = (self.modal.msg) ? self.modal.msg : "Please make sure you fill in all the required fields";
        } else {
          // Combining all files into a single list of array
          this.list = [];
          for (var h in self.files) {
            if (self.files.hasOwnProperty(h)) {
              for (var i = 0; i < self.files[h].length; i++) {
                this.list.push(self.files[h][i]);
              }
            }
          }

          self.sendMessage = '';
          self.sendPending = true;
          self.sendSuccess = false;
          /* eslint-disable camelcase */
          var action_Part = Store.get('actionPart');

          ActionRequired.submitAction({
            action: 'Off_Core',
            data: {
              ordsch_id: action_Part.OrdschId,
              tail_number: self.TailNo,
              part: self.PartNo || action_Part.Part,
              serial_no: self.SerialNo,
              time_since_new: self.SerialSub.TSN,
              cycles_since_new: self.SerialSub.CSN,
              days_since_new: self.SerialSub.DSN,
              time_since_overhaul: self.SerialSub.TSO,
              cycles_since_overhaul: self.SerialSub.CSO,
              days_since_overhaul: self.SerialSub.DSO,
              time_since_repair: self.SerialSub.TSR,
              cycles_since_repair: self.SerialSub.CSR,
              days_since_repair: self.SerialSub.DSR,
              time_since_install: self.SerialSub.TSI,
              cycles_since_install: self.SerialSub.CSI,
              days_since_install: self.SerialSub.DSI,
              time_remaining: self.SerialSub.TR,
              cycles_remaining: self.SerialSub.CR,
              removal_reason: self.removalReason
            }/* eslint-enable camelcase */
             // the below fields are appilicable only when we use email api instead of Action required
            /* files: this.list,
            senderInfo: defaultConstant.MAIL.SENDER_INFO,
            receiverInfo: defaultConstant.MAIL.RECEIVER_OFFCORERETURN,
            emailSubject: defaultConstant.MAIL.SUBJECT_OFFCORERETURN */
          })
            .then(function(value) {
              self.callbackSubmit(value);
              self.sendSuccess = true;
            })
            .catch(function() {
              self.callbackSubmit();
            })
            .finally(function() {
              self.sendPending = false;
            });
        }
      };

      this.callbackSubmit = function(value) {
        self.modal = {};
        if (value && value.result === 'Created Successfully') {
          self.modal.checkCircle = true;
          self.modal.title = "Your request has been submitted";
          self.modal.showModal = true;
          Store.set({ name: 'isResponseSuccess', value: true });
          $timeout(self.closeModal, 3000);
        } else {
          self.modal.exclamation = true;
          self.modal.title = "There was an error with your request";
          self.modal.msg = "Please try again";
          self.modal.isErrorModal = true;
          self.modal.showModal = true;
          Store.set({ name: 'isResponseSuccess', value: false });
        }
      };

      this.closeModal = function() {
        self.modal = {};
      };

      this.removeDoc = function(id, x) {
        this.files[id].splice(x, 1);
      };
    }
  ]
});
