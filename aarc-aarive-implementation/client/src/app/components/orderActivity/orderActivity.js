angular.module('app.components').component('orderActivity', {
  bindings: {
    items: '<',
    onScroll: '&'
  },
  templateUrl: 'orderActivity/orderActivity.html',
  controller: ['PoolOrderDetails',
    function(PoolOrderDetails) {
      this.$onChanges = function() {
        PoolOrderDetails.subscribe('ordData', this, {
          params: { index: '2', ordValue: this.items.OrdschId }
        });
      };
    }
  ]
});
