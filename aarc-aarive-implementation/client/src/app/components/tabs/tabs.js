angular.module('app.components').component('tabs', {
  bindings: {
    selectedIndex: '<',
    onClose: '=',
    onSelect: '='
  },
  transclude: true,
  templateUrl: 'tabs/tabs.html',
  controller: [function() {
    this.$onInit = function() {
      this.tabs = [];
      this.setActiveTab(this.selectedIndex);
    };

    this.$onChanges = function() {
      this.setActiveTab(this.selectedIndex);
    };

    this.addTab = function(tab) {
      tab.index = this.tabs.length;
      if (tab.index === (this.selectedIndex || 0)) {
        tab.selected = true;
      }
      this.tabs.push(tab);
    };

    this.removeTab = function(tab) {
      var index = -1;
      angular.forEach(this.tabs, function(t, i) {
        if (t.label === tab.label) {
          index = i;
        }
      });
      this.tabs = this.tabs.splice(index, 1);
    };

    this.setActiveTab = function(index) {
      if (this.tabs) {
        this.tabs.forEach(function(tab) {
          tab.selected = false;
        });
        if (this.tabs[index]) {
          this.tabs[index].selected = true;
        }
      }
    };

    this.handleSelect = function(index) {
      // Whenever possible, the on-select and selected-index attributes should be used to manage the
      // current tab index externally, in a declarative and unidirectional data flow style
      if (this.onSelect) {
        this.onSelect(this.tabs[index]);
      } else {
        this.setActiveTab(index);
      }
    };

    this.close = function() {
      (this.onClose || angular.noop)();
    };
  }]
});
