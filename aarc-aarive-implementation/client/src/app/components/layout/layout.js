angular.module('app.components').component('layout', {
  transclude: true,
  templateUrl: 'layout/layout.html',
  controller: [
    '$state',
    '$transitions',
    'Store',
    'AzureAuth',
    'setFocus',
    'defaultConstant',
    'AclService',
    '$timeout',
    function(
      $state,
      $transitions,
      Store,
      AzureAuth,
      setFocus,
      defaultConstant,
      AclService,
      $timeout
    ) {
      var self = this;
      this.isLandingPage = false;
      this.isNewPoolPage = false;
      var searchBoxId = 'search-box';

      this.$onInit = function() {
        // In read-only mode, the "new pool request" button should be hidden
        this.outside = false;
        this.searchCounts = {};
        this.isLandingPage = ($state.current.name === 'root.defaultLandingPage');
        this.isNewPoolPage = ($state.current.name === 'root.newPoolRequest');
        self.can = AclService.can;
        self.actionNewPoolReq = defaultConstant.ACTION_NEW_POOL_REQ;
        self.modal = {};
        self.modal.showModal = false;

        angular.forEach($state.params, function(value, key) {
          if (key !== '#') {
            Store.set({
              name: key,
              value: value
            });
          }
        });
        $transitions.onSuccess({ to: '**' }, function(transition) {
          angular.forEach($state.params, function(value, key) {
            if (key !== '#') {
              Store.set({
                name: key,
                value: value
              });
            }
          });
        });

        $transitions.onSuccess({ to: 'root.newPoolRequest' }, function() {
          self.isNewPoolPage = true;
        });
        $transitions.onSuccess({ to: 'root.home' }, function() {
          self.isNewPoolPage = false;
        });
        $transitions.onSuccess({ to: 'root.dashboard' }, function() {
          self.isNewPoolPage = false;
        });
        $transitions.onSuccess({ to: 'root.dashboard.tab' }, function() {
          self.isNewPoolPage = false;
        });
        $transitions.onSuccess({ to: 'root.defaultLandingPage' }, function() {
          self.isNewPoolPage = false;
        });
        $transitions.onStart({ to: '**' }, function() {
          Store.set({ name: 'isMenuOpen', value: false });
          Store.set({ name: 'isPoolOrderDetailsOpen', value: false });
          Store.set({ name: 'isActionRequiredOpen', value: false });
        });

        Store.subscribe({
          name: 'searchType',
          scope: this,
          onDataChanged: function(searchType) {
            if (searchType) {
              Store.set({ name: 'inSearchMode', value: true });
              self.searchCategories = searchType;
            } else {
              self.searchCategories = 'CustPo';
            }
          }
        });

        Store.subscribe({
          name: 'searchValue',
          scope: this,
          onDataChanged: function(searchValue) {
            if (searchValue) Store.set({ name: 'inSearchMode', value: true });
            self.searchData = searchValue;
          }
        });

        Store.subscribe({
          name: 'tabName',
          scope: this
        });

        Store.subscribe({
          name: 'counts',
          scope: self
        });

        Store.subscribe({
          name: 'isMenuOpen',
          scope: self
        });

        Store.subscribe({
          name: 'isMenuDisabled',
          scope: self
        });

        var setMenuState = function(val) {
          if (self.inSearchMode) {
            Store.set('isMenuDisabled', true);
          } else {
            Store.set('isMenuDisabled', false);
          }
          Store.set('isMenuOpen', false);
        };

        Store.subscribe({
          name: 'inSearchMode',
          scope: this,
          onDataChanged: setMenuState
        });

        Store.subscribe({
          name: 'hasOverlay',
          scope: this,
          onDataChanged: setMenuState
        });

        Store.subscribe('appError', this, {
          onDataChanged: function(msg) {
            if (msg) {
              self.modal.exclamation = true;
              self.modal.title = msg;
              self.modal.showModal = true;
              $timeout(self.closeModal, 3000);
            }
          }
        });
      };

      this.closeModal = function() {
        self.modal = {};
      };

      this.onFocusSearchBox = function() {
        Store.set({ name: 'inSearchMode', value: true });
      };

      this.exitSearchMode = function() {
        this.searchData = '';
        // Stay on the same state routing tree, but go up a level (or more) if the nesting is too
        // deep, e.g. 'root.dashboard.tab.search' should redirect to 'root.dashboard.tab'
        var state = $state.current.name.split('.').slice(0, 3).join('.');
        $state.go(state, { searchType: null, searchValue: null });
        Store.set({ name: 'inSearchMode', value: false });
        Store.set({ name: 'searchFilter', value: null });
      };

      this.onCategoryChange = function() {
        var pos = angular.element('#' + searchBoxId).caret('pos');
        setFocus(searchBoxId).then(function() {
          angular.element('#' + searchBoxId).caret('pos', pos);
        });
      };

      this.doSearch = function() {
        var pos = angular.element('#' + searchBoxId).caret('pos');
        if (!this.inSearchMode) {
          Store.set({ name: 'inSearchMode', value: true });
          return;
        }

        // There's nothing to do if there are no search terms
        if (!this.searchData) {
          return;
        }

        return $state.go('root.dashboard.tab.search', {
          tabName: 'all',
          searchType: this.searchCategories,
          searchValue: this.searchData
        }).then(function() {
          return setFocus(searchBoxId).then(function() {
            angular.element('#' + searchBoxId).caret('pos', pos);
          });
        });
      };

      this.uiOnParamsChanged = function(newValues) {
        angular.forEach(newValues, function(value, key) {
          Store.set({
            name: key,
            value: value
          });
        });
      };

      this.overview = false;
      this.dashboard = false;
      this.settings = false;

      this.toggleMenu = function() {
        // To Set Menu Selection
        if ($state.current.name.lastIndexOf('root.dashboard') === 0) {
          this.dashboard = true;
          this.overview = false;
          this.settings = false;
        } else if ($state.current.name === 'root.home') {
          this.overview = true;
          this.dashboard = false;
          this.settings = false;
        } else {
          this.settings = true;
          this.overview = false;
          this.dashboard = false;
        }

        Store.set({ name: 'isMenuOpen', value: !self.isMenuOpen });
      };

      this.changeState = function(index) {
        if (index === 0) {
          $state.go('root.home');
        } else if (index === 1) {
          $state.go('root.dashboard');
        } else if (index === 2) {
          $state.go('root.settings');
        }
      };

      this.signout = function() {
        AzureAuth.unsetUserId();
        location.replace(defaultConstant.LOGOUT_URL);
      };

      this.openUserDropdown = function() {
        this.isUserDropdownOpen = true;
      };

      this.closeUserDropdown = function() {
        this.isUserDropdownOpen = false;
      };
    }
  ]
});
