angular.module('app.components').component('modalPopup', {
  bindings: {
    modalValue: '='
  },
  templateUrl: 'modalPopup/modalPopup.html',
  controller: [function() {
    var self = this;

    this.$onInit = function() {
      self.showModal = false;
    };

    this.closeModal = function() {
      self.modalValue = {};
    };
  }]
});
