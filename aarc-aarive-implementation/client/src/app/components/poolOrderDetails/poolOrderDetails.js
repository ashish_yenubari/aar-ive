angular.module('app.components').component('poolOrderDetails', {
  templateUrl: 'poolOrderDetails/poolOrderDetails.html',
  controller: ['$state', '$stateParams', '$transitions', '$timeout', 'Store',
    function($state, $stateParams, $transitions, $timeout, Store) {
      var self = this;
      this.$onInit = function() {
        Store.subscribe({
          name: 'part',
          scope: this,
          onDataChanged: self.setTabOrder
        });
      };

      this.close = function() {
        Store.set({
          name: 'isPoolOrderDetailsOpen',
          value: false
        });
      };

      this.setTabOrder = function() {
        // Default Detail Page to 2nd tab if opening from Core Page
        if (self.part.Core.toUpperCase() === 'Y') {
          self.tabIndex = 1;
        } else {
          self.tabIndex = 0;
        }
      };
    }]
});
