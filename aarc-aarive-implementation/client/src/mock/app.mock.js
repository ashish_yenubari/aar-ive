angular.module('app.mock', ['ngMockE2E', 'app.constants']);

angular.module('app').requires.push('app.mock');
