angular.module('app.mock').config(['$provide', function($provide) {
  $provide.decorator('$httpBackend', function($delegate, $timeout) {
    var proxy = function(method, url, data, callback, headers) {
      var interceptor = function() {
        var _this = this;
        var _arguments = arguments;
        $timeout(function() {
          callback.apply(_this, _arguments);
        }, 1000);
      };
      return $delegate.call(this, method, url, data, interceptor, headers);
    };
    Object.keys($delegate).forEach(function(key) {
      proxy[key] = $delegate[key];
    });
    return proxy;
  });
}]);
