angular.module('app.mock').run([
  '$httpBackend',
  'defaultConstant',
  'Uri',
  'PoolOrderListData',
  'PoolOrderCountData',
  'PoolOrderDetailsSvPart',
  'PoolOrderDetailsUsOffCore',
  'dataset',
  'PoolOrderDetailsOrderActivity',
  'AarDocs',
  'AarBlobFile',
  'PartVerificationData',
  'AuthorizationMatrixData',
  function(
      $httpBackend,
      defaultConstant,
      Uri,
      PoolOrderListData,
      PoolOrderCountData,
      PoolOrderDetailsSvPart,
      PoolOrderDetailsUsOffCore,
      dataset,
      PoolOrderDetailsOrderActivity,
      AarDocs,
      AarBlobFile,
      PartVerificationData,
      AuthorizationMatrixData
  ) {
    var baseMatch = '^((http)(s?):\\/)?\\/[a-zA-Z0-9._\\-\\:]*(\\/pitss)?(\\/api)?(\\/v[0-9]+)?';
    var queryMatch = '(\\?[a-zA-Z0-9.=&+*%-:;!@]*)?';
    var sampledata = [];
    var matchUrl = function(endpoint, hasQueryString) {
      endpoint = endpoint || '';
      var query = (hasQueryString) ? queryMatch : '';
      var regex = baseMatch + '(' + endpoint.replace(/[.?*+^$[\]\\(){}|-]/g, '\\$&') + ')' + query + '$';
      return new RegExp(regex);
    };
    $httpBackend.whenGET(matchUrl(defaultConstant.POOL_ORDER_LIST_URL, true)).respond(function(method, url, data) {
      var components = Uri.parseUri(url);
      sampledata = dataset.getData();
      return [200, PoolOrderListData.getData(components.params, sampledata), {}];
    });

    $httpBackend.whenPOST(matchUrl(defaultConstant.POOL_ORDER_COUNT_URL, true)).respond(function(method, url, data) {
      var components = Uri.parseUri(url);
      sampledata = dataset.getData();
      return [200, PoolOrderCountData.getData(components.params, angular.fromJson(data), sampledata), {}];
    });

    $httpBackend.whenGET(matchUrl(defaultConstant.POOL_ORDER_DETAIL_SV_URL, true)).respond(function(method, url, data) {
      var components = Uri.parseUri(url);
      return [200, PoolOrderDetailsSvPart.getData(components.params, sampledata), {}];
    });

    $httpBackend.whenGET(matchUrl(defaultConstant.POOL_ORDER_ACTIVITY_URL, true)).respond(function(method, url, data) {
      var components = Uri.parseUri(url);
      return [200, PoolOrderDetailsOrderActivity.getData(components.params, sampledata), {}];
    });

    $httpBackend.whenGET(matchUrl(defaultConstant.POOL_ORDER_DETAIL_US_URL, true)).respond(function(method, url, data) {
      var components = Uri.parseUri(url);
      return [200, PoolOrderDetailsUsOffCore.getData(components.params, sampledata), {}];
    });

    $httpBackend.whenGET(matchUrl(defaultConstant.DOC_LIST_URL, true)).respond(function(method, url, data) {
      var components = Uri.parseUri(url);
      return [200, AarDocs.getData(components.params, sampledata), {}];
    });

    $httpBackend.whenGET(matchUrl(defaultConstant.BLOB_DOWNLOAD_URL, true)).respond(function(method, url, data) {
      var components = Uri.parseUri(url);
      return [200, AarBlobFile.getData(components.params, sampledata), {}];
    });

    var blobDownloadMatch = new RegExp(baseMatch + defaultConstant.BLOB_DOWNLOAD_URL + '/[A-Z0-9]+/enclosure/Blob$');
    $httpBackend.whenGET(blobDownloadMatch).respond(function(method, url, data) {
      return [200, AarBlobFile.getFileContents(), {}];
    });

    $httpBackend.whenGET(matchUrl(defaultConstant.PART_VERIFY, true)).respond(function(method, url, data) {
      var components = Uri.parseUri(url);
      return [200, PartVerificationData.getData(components.params), {}];
    });

    $httpBackend.whenGET(matchUrl(defaultConstant.AUTH_MATRIX, true)).respond(function(method, url, data) {
      var components = Uri.parseUri(url);
      return [200, AuthorizationMatrixData.getData(components.params), {}];
    });

    var authMeMatch = new RegExp(baseMatch + '(\\/\\.auth\\/me)$');
    $httpBackend.whenGET(authMeMatch).respond(function(method, url, data) {
      /* eslint-disable camelcase */
      var response = [{
        id_token: '',
        provider_name: 'aad',
        user_id: 'anz@aarcorp.com'
      }];
      /* eslint-enable camelcase */
      return [200, response, {}];
    });
  }
]);
