'use strict';
angular.module('app.mock').factory('PoolOrderListData', function() {
  var dataSort = function(sort, Jdata) {
    var orderby = sort.split(':');
    var sortBy = orderby[0];
    return Jdata.items.sort(function(a, b) {
      if (a[sortBy] < b[sortBy]) {
        if (orderby[1] === 'asc')
          return -1;
        return 1;
      }
      if (a[sortBy] > b[sortBy]) {
        if (orderby[1] === 'asc')
          return 1;
        return -1;
      }
      return 0;
    });
  };
  var searchData = function(type, value, json) {
    var jsonStr = [];
    value = value.substring(1, value.length - 1);
    angular.forEach(json, function(item) {
      switch (type) {
        case 'Tail':
          if (item.Tail.indexOf(value) > -1) {
            jsonStr.push(item);
          }
          break;
        case 'CustPo':
          if (item.CustPo.indexOf(value) > -1) {
            jsonStr.push(item);
          }
          break;
        case 'Part':
          if (item.Part.indexOf(value) > -1) {
            jsonStr.push(item);
          }
          break;
        case 'UsSerialNumber':
          if (item.UsSerialNumber.indexOf(value) > -1) {
            jsonStr.push(item);
          }
          break;
        default: jsonStr.push(item);
          break;
      }
    });
    return jsonStr;
  };
  var jsondata = function(query, searchBy, searchValue, sort, Jdata) {
    Jdata = dataSort(sort, Jdata);
    if (searchBy && searchValue)
      Jdata = searchData(searchBy, searchValue, Jdata);
    return Jdata;
  };
  return {
    getData: function(params, json) {
      var q = decodeURIComponent(params.q).replace(/\+/g, ' ').split(';');
      var UsStatus = decodeURIComponent(params.UsStatus).replace(/\+/g, ' ').split(';');
      var orderBy = decodeURIComponent(params.orderBy).replace(/\+/g, ' ').split(';');
      var priority = '';
      var searchBy = '';
      var searchValue = '';
      var core = '';
      var mbk = '';
      angular.forEach(q, function(part) {
        var keyValues = part.split('=');
        switch (keyValues[0]) {
          case 'Priority':
            priority = keyValues[1];
            break;
          case 'Core':
            core = keyValues[1];
            break;
          case 'Mbk':
            mbk = keyValues[1];
            break;
          default : break;
        }
        if (part.indexOf('LIKE') > -1) {
          var keyValue = part.split('LIKE');
          searchBy = keyValue[0];
          searchValue = keyValue[1];
        }
      });
      var actRequ = [];
      var filteredData = jsondata(q, searchBy.trim(), searchValue.trim(), orderBy.toString(), json);
      if (priority && priority !== 'all') {
        filteredData = filteredData.filter(function(item) {
          if (item.ActionRequired !== null)
            actRequ.push(item);
          return (item.Priority === priority.toUpperCase());
        });
      }
      if (priority.indexOf('ACT') > -1)
        filteredData = actRequ;
      if (core === 'Y') {
        filteredData = filteredData.filter(function(item) {
          if (item.Core === core)
            return item;
          return null;
        });
      }
      if (mbk === 'Y') {
        filteredData = filteredData.filter(function(item) {
          if (item.Mbk === mbk)
            return item;
          return null;
        });
      }
      if (UsStatus[0].indexOf('Delivered') > -1) {
        filteredData = filteredData.filter(function(item) {
          item.UsStatus = 'US Delivered';
          return item;
        });
      }
      return { items: filteredData.slice(Number(params.offset), Number(params.offset) + Number(params.limit)) };
    }
  };
});
