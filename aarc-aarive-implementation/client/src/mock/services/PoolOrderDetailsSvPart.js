'use strict';

angular.module('app.mock').factory('PoolOrderDetailsSvPart', ['MockGenerators', function(MockGenerators) {
  return {
    getData: function(params, json) {
      var q = decodeURIComponent(params.q).replace(/\+/g, ' ').split(';');
      var id = 0;
      angular.forEach(q, function(part) {
        var keyValues = part.split('=');
        id = parseInt(keyValues[1], 10);
      });
      var svitem = '';
      angular.forEach(json.items, function(item) {
        if (item.OrdschId === id) {
          svitem = item;
        }
      });
      var svData = [];
      var sampleSvData = {
        AarDocuments: MockGenerators.getRandomAarDocs(),
        AarOrderNum: "Z312H2000110",
        AcTailNum: svitem.Tail,
        AtaChapter: 73,
        ContractualDelivery: "1Oct2016-9:00CentralUS",
        CustomerPoNum: svitem.CustPo,
        DateRequiredBy: MockGenerators.getRandomDate(),
        Description: "PISA-PASSENGER INTERFACE AND SUPPLY ADAPTER",
        Email: "Christine.Defenain@sabena-aerospace.com",
        Fsc: "C9072",
        OrditmId: svitem.OrditmId,
        OrdschId: svitem.OrdschId,
        Priority: svitem.Priority,
        SerialNumber: svitem.SerialNumber,
        SvAirwayBill: "HAND CARRY",
        SvPartDestination: svitem.PartDest,
        SvPartNum: svitem.Part,
        SvShipmentStatus: svitem.ShipDate,
        TimeRequiredBy: "08:15",
        TimeZon: 'UTC+00'
      };
      svData.push(sampleSvData);
      return { items: svData };
    }
  };
}]);
