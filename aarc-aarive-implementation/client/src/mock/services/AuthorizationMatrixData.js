'use strict';

angular.module('app.mock').factory('AuthorizationMatrixData', ['defaultConstant', function(defaultConstant) {
  return {
    getData: function() {
      return {
        admin: [
          defaultConstant.ACTION_NEW_POOL_REQ,
          defaultConstant.ACTION_ACTION_REQUIRED,
          defaultConstant.ACTION_ADD_CUSTOMER_DOCUMENTS],
        creator: [
          defaultConstant.ACTION_NEW_POOL_REQ,
          defaultConstant.ACTION_ACTION_REQUIRED,
          defaultConstant.ACTION_ADD_CUSTOMER_DOCUMENTS],
        reader: []
      };
    }
  };
}]);
