'use strict';

angular.module('app.mock').factory('PoolOrderFiltersData', function() {
  return {
    getData: function() {
      return [
        {
          key: 'all',
          label: 'Pool'
        },
        {
          key: 'aog',
          label: 'AOG'
        },
        {
          key: 'critical',
          label: 'CRI'
        },
        {
          key: 'routine',
          label: 'SCH'
        },
        {
          key: 'rpl',
          label: 'RPL'
        },
        {
          key: 'dpa',
          label: 'DPA'
        },
        {
          key: 'core',
          label: 'Core'
        },
        {
          key: 'actionRequired',
          label: 'Action Required'
        },
        {
          key: 'poolHistory',
          label: 'Pool History'
        }
      ];
    }
  };
});
