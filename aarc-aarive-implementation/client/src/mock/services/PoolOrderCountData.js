'use strict';

angular.module('app.mock').factory('PoolOrderCountData', function() {
 // value has a prefix and suffix of * so we remove * from the top and bottom of string value
  var updateCount = function(type, value, json) {
    var jsonStr = [];
    value = value.substring(1, value.length - 1);
    angular.forEach(json.items, function(item) {
      switch (type) {
        case 'TAIL':
          if (item.Tail.indexOf(value) > -1) {
            jsonStr.push(item);
          }
          break;
        case 'CUSTOMER_PO':
          if (item.CustPo.indexOf(value) > -1) {
            jsonStr.push(item);
          }
          break;
        case 'PART':
          if (item.Part.indexOf(value) > -1) {
            jsonStr.push(item);
          }
          break;
        case 'SERIAL_NO':
          if (item.UsSerialNumber.indexOf(value) > -1) {
            jsonStr.push(item);
          }
          break;
        default:
          jsonStr.push(item);
          break;
      }
    });
    return jsonStr;
  };
  var counts = function(type, value, json) {
    json = updateCount(type, value, json);
    var priorities = { core: { total: 0, actionRequired: 0 } };
    angular.forEach(json, function(item) {
      switch (item.Priority) {
        case 'AOG':
          priorities.aog = priorities.aog || { total: 0, actionRequired: 0 };
          if (item.SvStatus.indexOf('Delivered') > -1 || item.Core === 'Y') {
            priorities.core.total++;
          } else if (item.ActionRequired !== 'N') {
            priorities.aog.total++;
            priorities.aog.actionRequired++;
          } else {
            priorities.aog.total++;
          }
          break;
        case 'CRITICAL':
          priorities.critical = priorities.critical || { total: 0, actionRequired: 0 };
          if (item.SvStatus.indexOf('Delivered') > -1 || item.Core === 'Y') {
            priorities.core.total++;
          } else if (item.ActionRequired !== 'N') {
            priorities.critical.total++;
            priorities.critical.actionRequired++;
          } else {
            priorities.critical.total++;
          }
          break;
        case 'ROUTINE':
          priorities.routine = priorities.routine || { total: 0, actionRequired: 0 };
          if (item.SvStatus.indexOf('Delivered') > -1 || item.Core === 'Y') {
            priorities.core.total++;
          } else if (item.ActionRequired !== 'N') {
            priorities.routine.total++;
            priorities.routine.actionRequired++;
          } else {
            priorities.routine.total++;
          }
          break;
        case 'MBK':
          priorities.mbk = priorities.mbk || { total: 0, actionRequired: 0 };
          if (item.SvStatus.indexOf('Delivered') > -1 || item.Core === 'Y') {
            priorities.core.total++;
          } else if (item.ActionRequired !== 'N') {
            priorities.mbk.total++;
            priorities.mbk.actionRequired++;
          } else {
            priorities.mbk.total++;
          }
          break;
        case 'RPL':
          priorities.rpl = priorities.rpl || { total: 0, actionRequired: 0 };
          if (item.SvStatus.indexOf('Delivered') > -1 || item.Core === 'Y') {
            priorities.core.total++;
          } else if (item.ActionRequired !== 'N') {
            priorities.rpl.total++;
            priorities.rpl.actionRequired++;
          } else {
            priorities.rpl.total++;
          }
          break;
        case 'DPA':
          priorities.dpa = priorities.dpa || { total: 0, actionRequired: 0 };
          if (item.SvStatus.indexOf('Delivered') > -1 || item.Core === 'Y') {
            priorities.core.total++;
          } else if (item.ActionRequired !== 'N') {
            priorities.dpa.total++;
            priorities.dpa.actionRequired++;
          } else {
            priorities.dpa.total++;
          }
          break;
        default : break;
      }
    });
    var data = [];
    angular.forEach(priorities, function(counts, key) {
      data.push([key.toUpperCase(), counts.total, counts.actionRequired].join(', '));
    });
    return data.join(' | ') + ' | ';
  };
  return {
    getData: function(params, data, json) {
      if (!data) {
        data = {name: "executeCount", parameters: [{countTypeVar: "ALL"}, {countValueVar: "***"}]};
      }
      return { result: counts(data.parameters[0].countTypeVar, data.parameters[1].countValueVar, json) };
    }
  };
});
