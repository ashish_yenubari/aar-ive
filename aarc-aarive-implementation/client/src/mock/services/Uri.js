// Ported to Angular service from parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

'use strict';

angular.module('app.mock').factory('Uri', function() {
  var options = {
    strictMode: false,
    key: [
      "source",
      "protocol",
      "authority",
      "userInfo",
      "user",
      "password",
      "host",
      "port",
      "relative",
      "path",
      "directory",
      "file",
      "query",
      "anchor"
    ],
    q: {
      name: "params",
      parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
      /* eslint-disable no-useless-escape, max-len */
      strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
      loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
      /* eslint-enable no-useless-escape, max-len */
    }
  };

  return {
    parseUri: function(str) {
      // Encode @ (which is not done by Angular) for purposes of decoding
      str = str.replace(/@/g, '%40');
      var m = options.parser[options.strictMode ? "strict" : "loose"].exec(str);
      var uri = {};
      var i = 14;

      while (i--) uri[options.key[i]] = m[i] || "";

      uri[options.q.name] = {};
      uri[options.key[12]].replace(options.q.parser, function($0, $1, $2) {
        if ($1) uri[options.q.name][$1] = $2;
      });

      // Remove the Email parameter
      if (uri.params && uri.params.q) {
        uri.params.q = uri.params.q.replace(/;?Email(%3D|=)[^;]+;?/, '');
      }

      return uri;
    }
  };
});
