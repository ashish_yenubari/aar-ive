'use strict';

angular.module('app.mock').factory('PoolOrderDetailsUsOffCore', ['MockGenerators', function(MockGenerators) {
  return {
    getData: function(params, json) {
      var q = decodeURIComponent(params.q).replace(/\+/g, ' ').split(';');
      var id = '';
      angular.forEach(q, function(part) {
        var keyValues = part.split('=');
        id = parseInt(keyValues[1], 10);
      });
      var usitem = '';
      angular.forEach(json.items, function(item) {
        if (item.OrdschId === id) {
          usitem = item;
        }
      });
      var sampleUsData = {
        AarDocuments: MockGenerators.getRandomAarDocs(),
        CustomerDocumentsUs: MockGenerators.getRandomCustDocs(),
        Email: "Christine.Defenain@sabena-aerospace.com",
        OrditmId: usitem.OrditmId,
        OrdschId: usitem.OrdschId,
        ShippingStatus: usitem.SvStatus,
        UsAirwayBill: MockGenerators.getRandomUsAirwayBill(),
        UsCsi: MockGenerators.getRandomNumber(),
        UsCsn: MockGenerators.getRandomNumber(),
        UsOrigin: usitem.PartDest,
        UsPartNum: usitem.Part,
        UsSerialNum: usitem.UsSerialNumber,
        UsTsi: MockGenerators.getRandomNumber(),
        UsTsn: MockGenerators.getRandomNumber(),
        UsTso: MockGenerators.getRandomNumber(),
        UsTsr: MockGenerators.getRandomNumber(),
        UsTr: MockGenerators.getRandomNumber(),
        UsCso: MockGenerators.getRandomNumber(),
        UsCsr: MockGenerators.getRandomNumber(),
        UsCr: MockGenerators.getRandomNumber(),
        UsDsn: MockGenerators.getRandomNumber(),
        UsDso: MockGenerators.getRandomNumber(),
        UsDsr: MockGenerators.getRandomNumber(),
        UsDsi: MockGenerators.getRandomNumber(),
        UsDr: MockGenerators.getRandomNumber()
      };
      var usData = [];
      usData.push(sampleUsData);
      return { items: usData };
    }
  };
}]);
