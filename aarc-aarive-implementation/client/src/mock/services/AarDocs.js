'use strict';

angular.module('app.mock').factory('AarDocs', ['MockGenerators', function(MockGenerators) {
  return {
    getData: function(params) {
      var q = MockGenerators.parseParameters(params.q);
      var filename = MockGenerators.getRandomNumber() + '.TIF';

      return {
        items: [{
          OrditmId: q.OrditmId,
          FileDescription: filename,
          FileName: filename,
          FileType: 'TIF',
          BlobId: MockGenerators.getRandomNumber(),
          Type: q.Type, // C or SV,
          Email: 'ARLETTE.T.SLAUGHTER@DELTA.COM'
        }]
      };
    }
  };
}]);
