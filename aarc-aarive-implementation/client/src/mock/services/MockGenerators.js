'use strict';

angular.module('app.mock').factory('MockGenerators', function() {
  return {
    getRandomNumber: function() {
      var num = Math.floor(Math.random() * 9999) + 10000;
      return num;
    },

    getRandomAarDocs: function() {
      var statusNames = ['Repair Order', 'Commercial Invoice', '8130', 'Workshop Report'];
      var randomDocs = [];
      var num = Math.floor(Math.random() * 4) + 1;
      for (var i = 0; i < num; i++) {
        randomDocs.push(statusNames[i]);
      }
      return randomDocs;
    },

    getRandomCustDocs: function() {
      var statusNames = ['NIS.pdf', 'RemovalTag.pdf', 'MovementHistory.pdf'];
      var randomDocs = [];
      var num = Math.floor(Math.random() * 3) + 1;
      for (var i = 0; i < num; i++) {
        randomDocs.push(statusNames[i]);
      }
      return randomDocs;
    },

    getRandomUsAirwayBill: function() {
      var airwayBill = null;
      var statusNames = ['HAND CARRY'];
      statusNames.push(airwayBill);
      var num = Math.floor(Math.random() * 2) + 0;
      var status = statusNames[num];
      return status;
    },

    getRandomDate: function() {
      function randomDate(start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
      }
      var dateObj = randomDate(new Date(2012, 0, 1), new Date());
      return dateObj;
    },

    parseParameters: function(param) {
      var data = {};
      var q = decodeURIComponent(param).replace(/\+/g, ' ').split(';');
      angular.forEach(q, function(part) {
        var keyValues = part.split('=');
        data[keyValues[0]] = keyValues[1];
      });
      return data;
    }
  };
});
