'use strict';

angular.module('app.mock').factory('PoolOrderDetailsOrderActivity', ['MockGenerators', function(MockGenerators) {
  return {
    getData: function(params, json) {
      var q = decodeURIComponent(params.q).replace(/\+/g, ' ').split(';');
      var id = '';
      angular.forEach(q, function(part) {
        var keyValues = part.split('=');
        id = parseInt(keyValues[1], 10);
      });
      var activityitem = '';
      angular.forEach(json.items, function(item) {
        if (item.OrdschId === id) {
          activityitem = item;
        }
      });
      var jsonStr = [];
      angular.forEach([
        'S/V Requested',
        'S/V Shipped',
        'S/V Ordered',
        'R/O Created',
        'U/S Shipped',
        'U/S Requested'
      ], function(status) {
        jsonStr.push({
          Dt: MockGenerators.getRandomDate(),
          OrditmId: activityitem.OrditmId,
          OrdschId: activityitem.OrdschId,
          Status: status
        });
      });
      return { items: jsonStr };
    }
  };
}]);
