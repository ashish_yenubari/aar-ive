'use strict';

angular.module('app.mock').factory('PartVerificationData', function() {
  return {
    getData: function(params) {
      var q = decodeURIComponent(params.q).replace(/\+/g, ' ').split(';');
      var partNumber = '';
      var partNumberArr = [{
        Part: 'part-1111111',
        Description: 'Fuel Metering Aerospace',
        Fsc: 'part-1111111'
      }, {
        Part: 'part-2222222',
        Description: 'Horizontal Stabiliser',
        Fsc: 'part-2222222'
      }];
      angular.forEach(q, function(part) {
        var keyValues = part.split(' LIKE ');
        if (keyValues[0] === 'Part') {
          partNumber = keyValues[1].replace(/\*/g, '');
        }
      });

      return {
        items: partNumberArr.filter(function(obj) {
          return (obj.Part.indexOf(partNumber) !== -1);
        })
      };
    }
  };
});
