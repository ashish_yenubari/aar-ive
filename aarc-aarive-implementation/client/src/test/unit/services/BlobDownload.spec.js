describe('BlobDownload factory', function() {
  var $httpBackend, AarDocs, BlobDownload, download;

  beforeEach(module('app.services'));
  beforeEach(module('app.constants'));
  beforeEach(module('app.mock'));

  beforeEach(inject(['$injector', function($injector) {
    BlobDownload = $injector.get('BlobDownload');
    AarDocs = $injector.get('AarDocs');
    $httpBackend = $injector.get('$httpBackend');
    var global = jasmine.getGlobal();
    download = spyOn(global, 'download');
  }]));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  afterAll(function() {
    BlobDownload = null;
  });

  it('should exist', function() {
    expect(BlobDownload).toBeDefined();
  });

  it('should call the download library with filename and data', function() {
    var files = AarDocs.getData({ q: 'OrditmId=1000000' });
    var file = files.items[0];
    BlobDownload.download(file);
    $httpBackend.flush();
    expect(download).toHaveBeenCalledWith(jasmine.any(String), file.FileName);
  });
});
