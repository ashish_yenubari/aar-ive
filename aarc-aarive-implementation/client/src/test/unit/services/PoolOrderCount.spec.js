describe('PoolOrderCount factory', function() {
  // TODO: PoolOrderFilters is loaded but not used here. Refactor and remove eslint-disable-line.
  var $httpBackend, Store, PoolOrderCount, PoolOrderFilters, $timeout, scope; // eslint-disable-line no-unused-vars

  beforeEach(module('app.services'));
  beforeEach(module('app.constants'));
  beforeEach(module('app.mock'));

  beforeEach(inject(['$injector', function($injector) {
    Store = $injector.get('Store');
    PoolOrderCount = $injector.get('PoolOrderCount');
    PoolOrderFilters = $injector.get('PoolOrderFilters');
    $httpBackend = $injector.get('$httpBackend');
    $timeout = $injector.get('$timeout');
    scope = {};
  }]));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $timeout.verifyNoPendingTasks();
  });

  afterAll(function() {
    Store.unsubscribe(scope);
    PoolOrderCount = null;
    PoolOrderFilters = null;
    Store = null;
  });

  it('should exist', function() {
    expect(PoolOrderCount).toBeDefined();
  });

  it('should bind and rebind updated details to scope', function() {
    spyOn(PoolOrderCount, 'init').and.callThrough();
    spyOn(PoolOrderCount, 'update').and.callThrough();

    // Initial binding
    PoolOrderCount.subscribe('counts', scope, {
      params: { countType: 'ALL'}
    });
    $timeout.flush();
    expect(scope.counts.isLoading).toBe(true);
    $httpBackend.flush();
    $timeout.flush();
    expect(scope.counts.isLoading).toBe(false);
    expect(scope.counts.all.total).toBe(209);
    // Action required is random, but should contain something and both props should match each other
    expect(scope.counts.actionRequired.total).toBeGreaterThan(0);
    expect(scope.counts.actionRequired.total).toBe(scope.counts.actionRequired.actionRequired);
    expect(PoolOrderCount.init).toHaveBeenCalled();
    expect(PoolOrderCount.update).not.toHaveBeenCalled();

    // Rebinding with updated params
    PoolOrderCount.init.calls.reset();
    PoolOrderCount.update.calls.reset();
    PoolOrderCount.subscribe('counts', scope, {
      params: { countType: 'TAIL', countValue: '*' }
    });
    $timeout.flush();
    expect(scope.counts.isLoading).toBe(true);
    $httpBackend.flush();
    $timeout.flush();
    expect(scope.counts.isLoading).toBe(false);
    expect(PoolOrderCount.init).not.toHaveBeenCalled();
    expect(PoolOrderCount.update).toHaveBeenCalled();
  });
});
