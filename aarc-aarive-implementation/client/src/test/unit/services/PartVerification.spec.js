describe('PartVerification factory', function() {
  var $httpBackend, PartVerification;

  beforeEach(module('app.services'));
  beforeEach(module('app.constants'));
  beforeEach(module('app.mock'));

  beforeEach(inject(['$injector', function($injector) {
    PartVerification = $injector.get('PartVerification');
    $httpBackend = $injector.get('$httpBackend');
  }]));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  afterAll(function() {
    PartVerification = null;
  });

  it('should exist', function() {
    expect(PartVerification).toBeDefined();
  });

  it('should verify a valid part number', function() {
    var items;
    PartVerification.verify({ partNumber: 'part-1111111' })
      .then(function(response) {
        items = response;
      });
    $httpBackend.flush();
    expect(items).toBeDefined();
    expect(items).toEqual(jasmine.any(Array));
    expect(items[0].Part).toBe('part-1111111');
    expect(items[0].Description).toBe('Fuel Metering Aerospace');
  });

  it('should not verify an invalid part number', function() {
    var response;
    PartVerification.verify({ partNumber: 'part-3333333' })
      .then(function(r) {
        response = r;
      });
    $httpBackend.flush();
    expect(response).toEqual([]);
  });
});
