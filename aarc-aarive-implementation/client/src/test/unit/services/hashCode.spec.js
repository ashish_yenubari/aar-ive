describe('hashCode factory', function() {
  var hashCode;

  beforeEach(module('app.services'));
  beforeEach(inject(['$injector', function($injector) {
    hashCode = $injector.get('hashCode');
  }]));

  afterAll(function() {
    hashCode = null;
  });

  it('should exist', function() {
    expect(hashCode).toBeDefined();
  });

  it('should be able to encode an empty string', function() {
    expect(hashCode('')).toBe(0);
  });

  it('should be able to encode undefined', function() {
    expect(hashCode()).toBe(0);
  });

  it('should be able to encode an empty object', function() {
    expect(hashCode({})).toBe(3938);
  });

  it('should be able to encode an empty array', function() {
    expect(hashCode([])).toBe(2914);
  });

  it('should be able to encode an non-empty object', function() {
    expect(hashCode({ foo: 'bar', limit: 3 })).toBe(599480141);
  });

  it('should be able to encode a null value', function() {
    expect(hashCode(null)).toBe(3392903);
  });
});
