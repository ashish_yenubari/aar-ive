describe('Store factory', function() {
  var Store, $timeout, scope, scope2, destroySpy;

  beforeEach(module('app.services'));

  beforeEach(inject(['$injector', function($injector) {
    Store = $injector.get('Store');
    $timeout = $injector.get('$timeout');
    destroySpy = jasmine.createSpy('destroySpy');
    scope = new function() {}();
    scope2 = new function() {
      this.$onDestroy = function() {
        destroySpy();
      };
    }();
  }]));

  afterEach(function() {
    $timeout.verifyNoPendingTasks();
    Store.unsubscribe(scope);
    Store.unsubscribe(scope2);
  });

  afterAll(function() {
    Store = null;
  });

  it('should exist', function() {
    expect(Store).toBeDefined();
  });

  it('should be able to store and subscribe to data', function() {
    Store.subscribe({
      name: 'foo',
      scope: scope
    });
    expect(scope.foo).toBe(undefined);
    Store.set({
      name: 'foo',
      value: 'bar'
    });
    $timeout.flush();
    expect(scope.foo).toBe('bar');
    Store.set({
      name: 'foo',
      value: 'baz'
    });
    $timeout.flush();
    expect(scope.foo).toBe('baz');
  });

  it('should be able to store and subscribe to data using shorthand', function() {
    Store.subscribe('foo', scope);
    expect(scope.foo).toBe(undefined);
    Store.set('foo', 'bar');
    $timeout.flush();
    expect(scope.foo).toBe('bar');
    Store.set('foo', 'baz');
    $timeout.flush();
    expect(scope.foo).toBe('baz');
  });

  it('should be able to unsubscribe from specific bindings or all bindings', function() {
    // Setup
    Store.subscribe('foo', scope);
    Store.subscribe('foo', scope2);
    Store.subscribe('bar', scope);
    Store.set('foo', 1);
    Store.set('bar', 2);
    $timeout.flush();
    expect(scope.foo).toBe(1);
    expect(scope2.foo).toBe(1);
    expect(scope.bar).toBe(2);

    // Unsubscribe from 'foo' and verify that the first scope isn't updated but that scope2 is
    Store.unsubscribe(scope, 'foo');
    Store.set('foo', 3);
    Store.set('bar', 4);
    $timeout.flush();
    expect(scope.foo).toBe(1);
    expect(scope.bar).toBe(4);
    expect(scope2.foo).toBe(3);

    // Unsubscribe from all and verify that our scope isn't updated but that scope2 still is
    Store.unsubscribe(scope);
    Store.set('foo', 5);
    Store.set('bar', 6);
    $timeout.flush();
    expect(scope.foo).toBe(1);
    expect(scope.bar).toBe(4);
    expect(scope2.foo).toBe(5);
  });

  it('should be not allow changing value types between objects, arrays, and primitives', function() {
    spyOn(console, 'warn');
    var initialObj = { isLoading: true, items: [] };
    var initialArr = ['foo', 'bar'];
    Store.set('foo', initialObj);
    expect(Store.get('foo')).toBe(initialObj);

    // Try to change the object value to a primitive
    Store.set('foo', 'baz');
    expect(console.warn).toHaveBeenCalled();
    expect(Store.get('foo')).toBe(initialObj);

    // Try again with an array
    console.warn.calls.reset();
    Store.set('foo', initialArr);
    expect(console.warn).toHaveBeenCalled();
    expect(Store.get('foo')).toBe(initialObj);

    // Try again with an object
    console.warn.calls.reset();
    Store.set('foo', { isLoading: false });
    expect(console.warn).not.toHaveBeenCalled();
    expect(Store.get('foo').isLoading).toBe(false);

    // Set another value with an array
    console.warn.calls.reset();
    Store.set('bar', initialArr);
    Store.set('bar', ['foo', 'baz']);
    expect(Store.get('bar')).toEqual(['foo', 'baz']);
    expect(console.warn).not.toHaveBeenCalled();
  });

  it('should call any configured callback functions', function() {
    var spy = jasmine.createSpy('spy');
    Store.subscribe('foo', scope, {
      onDataChanged: spy
    });
    expect(spy).not.toHaveBeenCalled();
    Store.set('foo', 'bar');
    $timeout.flush();
    expect(spy).toHaveBeenCalledWith('bar');
  });

  it('should automatically unsubscribe when a controller is destroyed', function() {
    spyOn(Store, 'unsubscribe');
    Store.subscribe('foo', scope);
    $timeout.flush();
    scope.$onDestroy();
    expect(Store.unsubscribe).toHaveBeenCalled();
  });

  it('should not overwrite controller $onDestroy lifecycle hook', function() {
    spyOn(Store, 'unsubscribe');
    Store.subscribe('foo', scope2);
    $timeout.flush();
    scope2.$onDestroy();
    expect(Store.unsubscribe).toHaveBeenCalled();
    expect(destroySpy).toHaveBeenCalled();
  });

  it('should be able to turn any object into a Store handler', function() {
    var handler = Store.createHandler({
      update: angular.noop
    });
    spyOn(angular, 'noop');
    spyOn(handler, 'update');
    handler.subscribe({
      name: 'foo',
      scope: scope
    });
    $timeout.flush();
    expect(angular.noop).toHaveBeenCalled();
    expect(handler.update).not.toHaveBeenCalled();
  });

  it('should warn if subscribing without valid name and scope', function() {
    spyOn(console, 'warn');
    Store.subscribe();
    expect(console.warn.calls.count()).toEqual(1);
    Store.subscribe('foo');
    expect(console.warn.calls.count()).toEqual(2);
    Store.subscribe({ name: 'foo' });
    expect(console.warn.calls.count()).toEqual(3);
    Store.subscribe({ scope: scope });
    expect(console.warn.calls.count()).toEqual(4);
    Store.subscribe(true, true);
    expect(console.warn.calls.count()).toEqual(5);
  });
});
