describe('PoolOrderList factory', function() {
  var $httpBackend, Store, PoolOrderList, $timeout, scope;

  beforeEach(module('app.services'));
  beforeEach(module('app.constants'));
  beforeEach(module('app.mock'));

  beforeEach(inject(['$injector', function($injector) {
    Store = $injector.get('Store');
    PoolOrderList = $injector.get('PoolOrderList');
    $httpBackend = $injector.get('$httpBackend');
    $timeout = $injector.get('$timeout');
    scope = {};
  }]));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $timeout.verifyNoPendingTasks();
  });

  afterAll(function() {
    Store.unsubscribe(scope);
    PoolOrderList = null;
    Store = null;
  });

  it('should exist', function() {
    expect(PoolOrderList).toBeDefined();
  });

  it('should bind and rebind updated data to scope using Store', function() {
    spyOn(PoolOrderList, 'findAll').and.callThrough();
    spyOn(PoolOrderList, 'init').and.callThrough();
    spyOn(PoolOrderList, 'update').and.callThrough();

    // Initial binding
    PoolOrderList.subscribe('data', scope, {
      params: {
        priority: 'aog',
        sortCol: 'ShipDate',
        sortAsc: true,
        limit: 20
      }
    });
    $timeout.flush();
    expect(scope.data.isLoading).toBe(true);
    $httpBackend.flush();
    $timeout.flush();
    expect(PoolOrderList.init).toHaveBeenCalled();
    expect(PoolOrderList.update).not.toHaveBeenCalled();
    expect(PoolOrderList.findAll).toHaveBeenCalledWith(jasmine.any(String), jasmine.any(Object), true);
    expect(scope.data.isLoading).toBe(false);
    expect(scope.data.items.length).toBe(20);
    expect(scope.data.items[0].Priority).toBe('AOG');

    // Rebinding with increased limit (pagination)
    PoolOrderList.init.calls.reset();
    PoolOrderList.update.calls.reset();
    PoolOrderList.findAll.calls.reset();
    PoolOrderList.subscribe('data', scope, {
      params: {
        priority: 'aog',
        sortCol: 'ShipDate',
        sortAsc: true,
        limit: 40
      }
    });
    $timeout.flush();
    $httpBackend.flush();
    $timeout.flush();
    expect(PoolOrderList.init).not.toHaveBeenCalled();
    expect(PoolOrderList.update).toHaveBeenCalled();
    expect(PoolOrderList.findAll).toHaveBeenCalledWith(jasmine.any(String), jasmine.any(Object), false);
    expect(scope.data.items.length).toBe(40);

    // Rebinding with updated params
    PoolOrderList.init.calls.reset();
    PoolOrderList.update.calls.reset();
    PoolOrderList.findAll.calls.reset();
    PoolOrderList.subscribe('data', scope, {
      params: {
        priority: 'mbk',
        sortCol: 'ShipDate',
        sortAsc: true,
        limit: 20
      }
    });
    $timeout.flush();
    expect(scope.data.isLoading).toBe(true);
    $httpBackend.flush();
    $timeout.flush();
    expect(PoolOrderList.init).not.toHaveBeenCalled();
    expect(PoolOrderList.update).toHaveBeenCalled();
    expect(PoolOrderList.findAll).toHaveBeenCalledWith(jasmine.any(String), jasmine.any(Object), true);
    expect(scope.data.isLoading).toBe(false);
    expect(scope.data.items[0].Priority).toBe('MBK');
  });

  it('should cancel the results of a previous pending request', function() {
    spyOn(Store, 'set').and.callThrough();
    PoolOrderList.subscribe('data', scope, {
      params: {
        priority: 'aog',
        sortCol: 'ShipDate',
        sortAsc: true,
        limit: 20
      }
    });
    // Store.set should have been called one time from the above with isLoading: true, but we're
    // not flushing $http before this request should be cancelled
    $timeout.flush();
    expect(Store.set).toHaveBeenCalledTimes(1);
    PoolOrderList.subscribe('data', scope, {
      params: {
        priority: 'mbk',
        sortCol: 'ShipDate',
        sortAsc: true,
        limit: 40
      }
    });
    $timeout.flush();
    $httpBackend.flush();
    $timeout.flush();
    expect(Store.set).toHaveBeenCalledTimes(4);
  });
});
