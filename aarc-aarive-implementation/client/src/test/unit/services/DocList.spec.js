describe('DocList factory', function() {
  var $httpBackend, DocList, $timeout, scope;

  beforeEach(module('app.services'));
  beforeEach(module('app.constants'));
  beforeEach(module('app.mock'));

  beforeEach(inject(['$injector', function($injector) {
    DocList = $injector.get('DocList');
    $httpBackend = $injector.get('$httpBackend');
    $timeout = $injector.get('$timeout');
    scope = {};
  }]));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $timeout.verifyNoPendingTasks();
  });

  afterAll(function() {
    DocList.unsubscribe(scope);
    DocList = null;
  });

  it('should exist', function() {
    expect(DocList).toBeDefined();
  });

  it('should bind and rebind updated details to scope', function() {
    spyOn(DocList, 'init').and.callThrough();
    spyOn(DocList, 'update').and.callThrough();

    // Initial binding
    DocList.subscribe('list', scope, {
      params: { Type: 'C', OrdschId: 100000 }
    });
    $timeout.flush();
    expect(scope.list.isLoading).toBe(true);
    $httpBackend.flush();
    $timeout.flush();
    expect(scope.list.isLoading).toBe(false);
    expect(DocList.init).toHaveBeenCalled();
    expect(DocList.update).not.toHaveBeenCalled();

    // Rebinding with updated params
    DocList.init.calls.reset();
    DocList.update.calls.reset();
    DocList.subscribe('list', scope, {
      params: { Type: 'SV', OrdschId: 100000 }
    });
    $timeout.flush();
    expect(scope.list.isLoading).toBe(true);
    $httpBackend.flush();
    $timeout.flush();
    expect(scope.list.isLoading).toBe(false);
    expect(DocList.init).not.toHaveBeenCalled();
    expect(DocList.update).toHaveBeenCalled();
  });
});
