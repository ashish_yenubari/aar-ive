describe('PoolOrderDetails factory', function() {
  var $httpBackend, Store, PoolOrderDetails, $timeout, scope;

  beforeEach(module('app.services'));
  beforeEach(module('app.constants'));
  beforeEach(module('app.mock'));

  beforeEach(inject(['$injector', function($injector) {
    Store = $injector.get('Store');
    PoolOrderDetails = $injector.get('PoolOrderDetails');
    $httpBackend = $injector.get('$httpBackend');
    $timeout = $injector.get('$timeout');
    scope = {};
  }]));

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $timeout.verifyNoPendingTasks();
  });

  afterAll(function() {
    Store.unsubscribe(scope);
    PoolOrderDetails = null;
    Store = null;
  });

  it('should exist', function() {
    expect(PoolOrderDetails).toBeDefined();
  });

  it('should bind and rebind updated details to scope', function() {
    spyOn(PoolOrderDetails, 'init').and.callThrough();
    spyOn(PoolOrderDetails, 'update').and.callThrough();

    // Initial binding
    PoolOrderDetails.subscribe('details', scope, {
      params: { index: '1', OrdschId: 100000 }
    });
    $timeout.flush();
    expect(scope.details.isLoading).toBe(true);
    $httpBackend.flush();
    $timeout.flush();
    expect(scope.details.isLoading).toBe(false);
    expect(PoolOrderDetails.init).toHaveBeenCalled();
    expect(PoolOrderDetails.update).not.toHaveBeenCalled();

    // Rebinding with updated params
    PoolOrderDetails.init.calls.reset();
    PoolOrderDetails.update.calls.reset();
    PoolOrderDetails.subscribe('details', scope, {
      params: { index: '2', OrdschId: 100000 }
    });
    $timeout.flush();
    expect(scope.details.isLoading).toBe(true);
    $httpBackend.flush();
    $timeout.flush();
    expect(scope.details.isLoading).toBe(false);
    expect(PoolOrderDetails.init).not.toHaveBeenCalled();
    expect(PoolOrderDetails.update).toHaveBeenCalled();
  });
});
