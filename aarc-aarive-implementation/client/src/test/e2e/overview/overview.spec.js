import Overview from './overview.po';
const OVERVIEW = browser.constants.OVERVIEW;
const NEW_POOL_REQUEST = browser.constants.NEW_POOL_REQUEST;
const DASHBOARD = browser.constants.DASHBOARD;

let navAssertions = function(page, data, index) {
  let count;
  it('should record the count on the current nav item', () => {
    count = (index > 0) ? page.nav.count(index) : '';
    expect(count).toBeDefined();
  });
  it('should open the dashboard to "${data.label}"', () => {
    page.nav.open(index);
    page.confirmUrl(DASHBOARD.url + data.path);
  });
  it('should have the "${data.label}" tab selected', () => {
    let label = (index > 0) ? data.label : '';
    expect(page.tabs.active.label.getText()).toMatch(label);
  });
  it(`should have the correct count (${data.label})`, () => {
    page.tabs.active.count.isPresent().then((presence) => {
      if (presence) {
        expect(page.tabs.active.count.getText()).toEqual(count);
      } else {
        expect($('status-message').getText()).toMatch(browser.constants.STATUS_MESSAGES.noResults);
      }
    });
  });
  it('should return to the overview page', () => {
    page.back();
    page.confirmUrl(OVERVIEW.url);
  });
};

describe('AARive overview page', () => {
  let page = new Overview();
  it('should open the page', () => {
    page.open();
    page.confirmUrl(OVERVIEW.url);
  });
  it('should display the header', () => {
    page.header.displayed();
  });
  it('should display the navigation', () => {
    page.nav.displayed();
  });
  it('should display the correct navigation labels', () => {
    expect(page.nav.confirmLabels(OVERVIEW.nav)).toBe(true);
  });
  it('should display numbers for the category counts', () => {
    expect(page.nav.confirmCounts()).toBe(true);
  });
  it('should open a new pool request', () => {
    page.header.create.click();
    page.confirmUrl(NEW_POOL_REQUEST.url);
  });
  it('should return to the overview page', () => {
    page.back();
    page.confirmUrl(OVERVIEW.url);
  });
  OVERVIEW.nav.forEach((data, index) => {
    navAssertions(page, data, index);
  });
});
