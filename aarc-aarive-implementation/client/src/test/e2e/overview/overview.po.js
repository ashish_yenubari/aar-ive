import BasePage from '../base/base.po';
import Header from '../fragments/header.fragment';
import Nav from '../fragments/nav.fragment';

class Overview extends BasePage {
  constructor() {
    super('home');
    this.header = new Header();
    this.nav = new Nav();
    this._activeTab = $('dashboard tabs .tabs li.active');
    this.tabs = {
      active: {
        label: this._activeTab.$('a'),
        count: this._activeTab.$('span')
      }
    };
  }
}

export default Overview;
