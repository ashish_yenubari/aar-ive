class BasePage {
  constructor(path = '') {
    this.path = path;
    this.url = browser.baseUrl + this.path;
  }

  open(url = this.url) {
    browser.driver.get(url);
    return this;
  }

  back() {
    browser.driver.navigate().back();
  }

  confirmUrl(url) {
    expect(browser.getCurrentUrl()).toBe(url);
  }
}

export default BasePage;
