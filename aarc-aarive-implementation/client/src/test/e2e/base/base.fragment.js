class Fragment {
  constructor(fragment) {
    this.fragment = fragment;
    this.elements = {};
  }

  reduceBoolean(array) {
    return array.every((item) => {
      return item === true;
    });
  }

  isFunction(elem) {
    return typeof elem === 'function';
  }

  present(expectation = true) {
    expect(this.fragment.isPresent()).toBe(expectation);
    for (var elem in this.elements) {
      if (this.elements.hasOwnProperty(elem)) {
        if (this.isFunction(this.elements[elem].isPresent)) {
          expect(this.elements[elem].isPresent()).toBe(expectation);
        } else {
          let items = this.elements[elem].map((e, i) => {
            return e.isPresent();
          });
          expect(items.then((itemArray) => {
            return this.reduceBoolean(itemArray);
          })).toBe(expectation);
        }
      }
    }
  }

  displayed(exclude = [], expectation = true) {
    expect(this.fragment.isDisplayed()).toBe(expectation);
    for (var elem in this.elements) {
      if (this.elements.hasOwnProperty(elem) && exclude.indexOf(elem) === -1) {
        this.elements[elem].isDisplayed().then((display) => {
          if (display instanceof Array) {
            display = this.reduceBoolean(display);
          }
          expect(display).toBe(expectation);
        });
      }
    }
  }
}

export default Fragment;
