import BasePage from '../base/base.po';
import Header from '../fragments/header.fragment';
import Tabs from '../fragments/tabs.fragment';
import InfiniteScrollTable from '../fragments/infiniteScrollTable.fragment';

class Dashboard extends BasePage {
  constructor() {
    super('dashboard');
    this.header = new Header();
    this.tabs = new Tabs($('dashboard ul.nav-tabs'));
    this.table = new InfiniteScrollTable($('pool-order-list'));
  }
}

export default Dashboard;
