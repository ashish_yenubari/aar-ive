import Dashboard from './dashboard.po';
const DASHBOARD = browser.constants.DASHBOARD;

let tabAssertions = function(page, label) {
  it('should activate the selected tab', () => {
    page.tabs.activate(label.index);
    expect(page.tabs.active).toHaveClass('active');
  });
  it('should have to correct label', () => {
    expect(page.tabs.activeLabel.getText()).toEqual(label.label);
  });
  it('should have the appropriate headers or status message', () => {
    page.table.bodyRows.count().then((count) => {
      if (count > 0) {
        page.table.validateColumnHeaders(DASHBOARD.tabs.headers);
      } else {
        expect($('status-message').getText()).toMatch(browser.constants.STATUS_MESSAGES.noResults);
      }
    });
  });
};

describe('AARive dashboard page', () => {
  let page = new Dashboard();
  it('should open the application', () => {
    page.open();
    page.confirmUrl(DASHBOARD.url);
  });
  DASHBOARD.tabs.labels.forEach((label) => {
    tabAssertions(page, label);
  });
});
