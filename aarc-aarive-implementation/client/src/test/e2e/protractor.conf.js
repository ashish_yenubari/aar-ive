const testConfigName = (process.env.TEST_CONFIG) ? process.env.TEST_CONFIG : 'test';
const timeoutLength = 60000;
const seleniumPath = '../../../node_modules/protractor/node_modules/webdriver-manager/selenium';
const seleniumVersion = '2.53.1';
const outputPath = `./src/test/e2e/results.${Date.now()}.json`;
const appUrl = (process.env.AARIVE_URL) ? process.env.AARIVE_URL : 'http://localhost:4444/';

exports.config = {
  framework: 'jasmine',
  seleniumServerJar: `${seleniumPath}/selenium-server-standalone-${seleniumVersion}.jar`,
  seleniumPort: 8675,
  baseUrl: appUrl,
  getPageTimeout: timeoutLength,
  allScriptsTimeout: timeoutLength,
  directConnect: true,
  resultJsonOutputFile: outputPath,
  capabilities: {
    browserName: 'chrome'
  },
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: timeoutLength
  },
  onPrepare: function() {
    browser.constants = require(`./config/${testConfigName}.constants.js`);
    require('babel-register');
    require('./helpers/matchers.js');
    setTimeout(function() {
      browser.driver.executeScript(function() {
        return {
          width: window.screen.availWidth,
          height: window.screen.availHeight
        };
      }).then(function(result) {
        browser.driver.manage().window().setPosition(0, 0);
        browser.driver.manage().window().setSize(result.width, result.height);
      });
    });
  }
};
