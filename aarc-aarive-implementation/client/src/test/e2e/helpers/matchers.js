let matchers = {
  toHaveClass: function() {
    return {

      compare: (element, cls) => {
        var ret = {
          pass: element.getAttribute('class').then((classes) => {
            var classesArr = classes.split(/\s/);
            ret.message = "Expected to have class " + cls;
            return classesArr.indexOf(cls) >= 0;
          })
        };
        return ret;
      },

      negativeCompare: (element, cls) => {
        var ret = {
          pass: element.getAttribute('class').then((classes) => {
            var classesArr = classes.split(/\s/);
            ret.message = "Expected to NOT have class " + cls;
            return classesArr.indexOf(cls) === -1;
          })
        };
        return ret;
      }
    };
  }
};

beforeEach(function() {
  jasmine.addMatchers(matchers);
});
