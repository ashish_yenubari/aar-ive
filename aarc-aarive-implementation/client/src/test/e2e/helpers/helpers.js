import Landing from '../landing/landing.po';
const LANDING = browser.constants.LANDING;

export const login = function(page = new Landing()) {
  let loginPage = new Landing();
  it('should open the application', () => {
    browser.ignoreSynchronization = true;
    loginPage.open(LANDING.url);
    loginPage.confirmUrl(LANDING.url);
  });
  it('should click the sign in button', () => {
    loginPage.signin.click();
    expect(browser.getCurrentUrl()).toContain(LANDING.buttons.signin.url);
  });
  it('should login to the application', () => {
    browser.ignoreSynchronization = false;
    page.open(browser.baseUrl);
    page.confirmUrl(browser.baseUrl);
  });
};
