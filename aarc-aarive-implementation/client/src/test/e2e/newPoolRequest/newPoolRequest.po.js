import BasePage from '../base/base.po';
import Header from '../fragments/header.fragment';
import Form from '../fragments/form.fragment';
import Nav from '../fragments/nav.fragment';
const waitForClickable = function(target) {
  let until = protractor.ExpectedConditions;
  browser.wait(
    until.elementToBeClickable(target),
    5000,
    'Target should be clickable within 5s'
  );
};

class NewPoolRequest extends BasePage {
  constructor() {
    super('newPoolRequest');
    this.header = new Header();
    this.nav = new Nav();
    this.form = new Form($('new-pool-request'));
    this.closeBtn = $('.newPool-close a');
    let modal = $('.modal-confirmation');
    this.modal = modal;
    this.modalLeave = modal.$('button.pod-save');
    this.modalStay = modal.$('button.pod-cancel');
    this.modalText = modal.$('h4');
    this.modalIcon = this.modalText.$('i.fa');
    // Part verification
    this.verificationText = $('.fieldinfo');
    this.verificationIcon = $('.part-status-result i.glyphicon');

    this.form.addInput('submit', false, 'input[name="submit"]');
    this.form.addInput('acTail');
    this.form.addInput('part');
    this.form.addInput('alternatePart');
    this.form.addInput('ataChapter');
    this.form.addInput('priority');
    this.form.addInput('contractualDelivery');
    this.form.addInput('required-date');
    this.form.addInput('timeReqBy');
    this.form.addInput('timeReqByTimeZone', false);
    this.form.addInput('installedSerial');
    this.form.addInput('customerPO');
    this.form.addInput('svPartDestination');
    this.form.addInput('comment', true, '.npr-textarea textarea', '.poolsupport');
  }

  close(confirm = true) {
    this.closeBtn.click();
    if (confirm) {
      waitForClickable(this.modalLeave);
      this.modalLeave.click();
    } else {
      waitForClickable(this.modalStay);
      this.modalStay.click();
    }
  }
}

export default NewPoolRequest;
