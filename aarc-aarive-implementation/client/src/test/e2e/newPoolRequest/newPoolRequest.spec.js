/* eslint protractor/no-browser-sleep: 0 */
import NewPoolRequest from './newPoolRequest.po';
const NPR = browser.constants.NEW_POOL_REQUEST;

const partVerifyInput = function(elm, input) {
  elm.clear().then(() => {
    elm.sendKeys(input);
  });
  elm.getSize().then((size) => {
    browser.actions()
      .mouseMove(elm, {x: 345})
      .click()
      .perform();
  });
  expect(elm.getAttribute('value')).toBe(input);
};

describe('New Pool Request form', () => {
  let page = new NewPoolRequest();
  let prevUrl;
  let partVerifyOutput = function(text, classes) {
    browser.sleep(2000);
    expect(page.verificationText.isDisplayed()).toBe(true);
    expect(page.verificationIcon.isDisplayed()).toBe(true);
    expect(page.verificationText.getText()).toBe(text);
    classes.forEach((cls) => {
      expect(page.verificationIcon).toHaveClass(cls);
    });
  };
  it('should open the application', () => {
    page.open(browser.baseUrl);
    page.header.displayed();
  });
  it('should open the new pool request page', () => {
    browser.getCurrentUrl().then((currentUrl) => {
      prevUrl = currentUrl;
    });
    page.header.create.click();
    page.confirmUrl(NPR.url);
    page.form.displayed();
  });
  it('should click the close button and display the confirmation modal', () => {
    page.closeBtn.click();
    expect(page.modal.isDisplayed()).toBe(true);
  });
  it('modal should display the leave button with correct label', () => {
    expect(page.modalLeave.isDisplayed()).toBe(true);
    expect(page.modalLeave.getText()).toBe(NPR.modal.buttons.leave.label);
  });
  it('modal should display the stay button with correct label', () => {
    expect(page.modalStay.isDisplayed()).toBe(true);
    expect(page.modalStay.getText()).toBe(NPR.modal.buttons.stay.label);
  });
  it('modal should display the correct icon and text', () => {
    expect(page.modalText.isDisplayed()).toBe(true);
    expect(page.modalText.getText()).toBe(NPR.modal.text);
    expect(page.modalIcon.isDisplayed()).toBe(true);
    expect(page.modalIcon).toHaveClass('fa-exclamation');
  });
  it('should click the stay button on the modal', () => {
    page.modalStay.click();
    page.confirmUrl(NPR.url);
    page.form.displayed();
  });
  it('should close the new pool request form', () => {
    page.close();
    page.confirmUrl(prevUrl);
  });
  it('should re-open the new pool request form', () => {
    browser.sleep(2000);
    page.header.create.click();
    page.confirmUrl(NPR.url);
  });
  it('should display the new pool request form', () => {
    page.form.displayed();
  });
  it('should have the expected labels', () => {
    page.form.confirmLabels(NPR.labels);
  });
  it('should verify a valid part number', () => {
    partVerifyInput(page.form.elements.part, NPR.fields.part.input.valid);
  });
  it('should show the verified part text and icon', () => {
    partVerifyOutput(NPR.fields.part.validText, ['glyphicon-ok', 'part-status-valid']);
  });
  it('should not verify an invalid part number', () => {
    partVerifyInput(page.form.elements.part, NPR.fields.part.input.invalid);
  });
  it('should show the unverified part text and icon', () => {
    partVerifyOutput(NPR.fields.part.invalidText, ['glyphicon-remove-circle', 'part-status-invalid']);
  });
});
