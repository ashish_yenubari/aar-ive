import Fragment from '../base/base.fragment';

class SearchMenu extends Fragment {
  constructor() {
    super($('header .dropdown'));
    this.elements.button = this.fragment.$('.dropbtn');
    this.options = this.fragment.$$('dropdown-content a');
    this.selected = this.options.get(1);
  }

  open() {
    this.elements.button.click();
  }

  change(index) {
    this.open();
    this.selected = this.options.get(index);
    this.selected.click();
  }

  currentValue() {
    return this.elements.button.getText();
  }
}

export default SearchMenu;
