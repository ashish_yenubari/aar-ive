import Fragment from '../base/base.fragment';

class Form extends Fragment {
  constructor(fragment) {
    super(fragment);
    this.labels = {};
  }

  addInput(name, label = true, selector = `#${name}`, labelSelector = `label[for="${name}"]`) {
    this.elements[name] = $(selector);
    if (label) {
      this.elements[`${name}Label`] = $(labelSelector);
      this.labels[name] = $(labelSelector);
    }
  }

  confirmLabels(expected) {
    Object.keys(this.labels).forEach((key, index) => {
      let label = this.labels[key];
      expect(label.getText()).toBe(expected[index]);
    });
  }
}

export default Form;
