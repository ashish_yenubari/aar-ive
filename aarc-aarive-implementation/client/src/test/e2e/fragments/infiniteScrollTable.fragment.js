/* eslint protractor/no-browser-sleep: 0 */
import {TABLE} from '../config/test.constants';
import Fragment from '../base/base.fragment';

class InfiniteScrollTable extends Fragment {
  constructor(fragment) {
    super(fragment);
    this.elements.scrollContainer = this.fragment.$('.scroll-container');
    this.elements.table = this.elements.scrollContainer.$('table');
    this.elements.scrollableArea = this.elements.table.$('tbody');
    this.elements.headerRow = this.fragment.$('table.clone thead tr');
    this.elements.columnHeaders = this.elements.headerRow.$$('th');
    this.elements.sortableHeaders = this.elements.headerRow.$$('span');
    this.elements.bodyRows = this.elements.scrollableArea.$$('tr');
    this.elements.first = this.elements.bodyRows.first();
  }

  get activeSort() {
    return this.elements.headerRow.$('.sort-active');
  }

  get detailsIcon() {
    return this.elements.first.$('.info-btn a');
  }

  get table() {
    return this.elements.table;
  }

  get sortableHeaders() {
    return this.elements.sortableHeaders;
  }

  get bodyRows() {
    return this.elements.bodyRows;
  }

  get first() {
    return this.elements.first;
  }

  validateColumnHeaders(expected) {
    this.elements.columnHeaders.each((header, index) => {
      expect(header.getText()).toBe(expected[index]);
    });
  }

  hasRows(expectedRows) {
    return this.elements.bodyRows.count().then((rows) => {
      if (expectedRows !== undefined) {
        return rows === expectedRows;
      }
      return rows > 0;
    });
  }

  getRows() {
    return this.elements.bodyRows.count();
  }

  highlight(mouseEvent) {
    if (mouseEvent === 'click') {
      this.elements.first.click();
    } else if (mouseEvent === 'hover') {
      browser.driver.actions().mouseMove(this.elements.first).perform();
    }
    browser.sleep(1000);
    expect(this.elements.first.getCssValue('background-color')).toBe(TABLE.colors[mouseEvent]);
  }

  scroll(direction = 'down') {
    return this.elements.scrollContainer.getSize().then((containerSize) => {
      return this.elements.scrollableArea.getSize().then((scrollSize) => {
        let scrollHeight = (direction === 'down') ? scrollSize.height : 0;
        return browser.executeScript(
          `arguments[0].scrollTop = ${scrollHeight};`,
          this.elements.scrollContainer.getWebElement()
        );
      });
    });
  }

  shouldScroll() {
    return this.elements.scrollContainer.getSize().then((containerSize) => {
      return this.elements.scrollableArea.getSize().then((scrollSize) => {
        return scrollSize.height > containerSize.height;
      });
    });
  }

  doesScroll(direction) {
    return this.elements.table.getLocation().then((initialLocation) => {
      return this.scroll(direction).then(() => {
        return this.elements.table.getLocation().then((newLocation) => {
          return initialLocation.y > newLocation.y;
        });
      });
    });
  }
}

export default InfiniteScrollTable;
