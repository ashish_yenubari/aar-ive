import Fragment from '../base/base.fragment';

class Tabs extends Fragment {
  constructor(fragment) {
    super(fragment);
    this.elements.all = this.fragment.$$('li');
    this._current = 0;
    this.elements.active = this.all.get(0);
  }

  activate(index) {
    this._current = index;
    this.elements.active = this.all.get(this._current);
    this.activeLabel.click();
  }

  set current(value) {
    this._current = value;
    this.elements.active = this.all.get(this._current);
  }

  get current() {
    return this._current;
  }

  get active() {
    return this.elements.active;
  }

  get activeLabel() {
    return this.elements.active.$('a');
  }

  get activeCount() {
    return this.elements.active.$('span.part-count-tab');
  }

  get all() {
    return this.elements.all;
  }
}

export default Tabs;
