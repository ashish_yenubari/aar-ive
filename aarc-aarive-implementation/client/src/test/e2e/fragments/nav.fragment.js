/* eslint protractor/no-by-xpath: 0 */
import Fragment from '../base/base.fragment';

class Nav extends Fragment {
  constructor() {
    super($('.db-bottom-nav'));
    this.elements.items = this.fragment.$$('ul li');
    this.elements.active = this.fragment.$('.activeitem');
    this.elements.counts = this.fragment.$$('div.count');
    this.elements.labels = this.fragment.all(
      // using xpath to get non-empty elements
      by.xpath('//span[string-length(text()) > 1]')
    );
  }

  count(index) {
    return this.elements.counts.get(index).getText().then((count) => {
      return count;
    });
  }

  open(index) {
    this.elements.items.get(index).click();
  }

  confirmLabels(data) {
    return this.elements.labels.reduce((confirmation, item, index) => {
      return item.getText().then((text) => {
        if (confirmation) return text === data[index].label;
        return false;
      });
    }, true);
  }

  confirmCounts() {
    return this.elements.counts.reduce((confirmation, item, index) => {
      return item.getText().then((text) => {
        let num = parseInt(text, 10);
        return !isNaN(num);
      });
    });
  }
}

export default Nav;
