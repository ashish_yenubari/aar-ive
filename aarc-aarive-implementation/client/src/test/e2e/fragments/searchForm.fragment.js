import Fragment from '../base/base.fragment';
import SearchMenu from './searchMenu.fragment';

class SearchForm extends Fragment {
  constructor() {
    super($('header form'));
    this.searchString = '';
    this.menu = new SearchMenu();
    this.elements.searchField = this.fragment.$('input#search-box');
    this.elements.searchButton = this.fragment.$('button#searchButton');
    this.clearSearchButton = this.fragment.$('a.search-text-close');
  }

  get searchButton() {
    return this.elements.searchButton;
  }

  clear() {
    this.clearSearchButton.click();
  }

  activate() {
    this.elements.searchField.click();
  }

  search(text, useButton = true) {
    this.searchString = text;
    this.elements.searchField.sendKeys(text);
    if (useButton) {
      this.elements.searchButton.click();
    } else {
      this.elements.searchField.sendKeys(protractor.Key.ENTER);
    }
  }
}

export default SearchForm;
