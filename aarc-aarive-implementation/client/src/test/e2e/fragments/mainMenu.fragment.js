import Fragment from '../base/base.fragment';

class MainMenu extends Fragment {
  constructor() {
    super($('header .main-menu'));
    this.elements.icon = this.fragment.$('#nav-icon2');
    this.items = this.fragment.$$('.overlay-content a');
  }

  toggle() {
    this.elements.icon.click();
  }

  open() {
    this.elements.icon.getAttribute('class').then((cls) => {
      if (cls.indexOf('open') > -1) this.elements.icon.click();
    });
  }

  change(index) {
    this.items.get(index).click();
  }
}

export default MainMenu;
