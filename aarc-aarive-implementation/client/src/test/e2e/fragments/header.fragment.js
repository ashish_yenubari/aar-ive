import Fragment from '../base/base.fragment';
import MainMenu from './mainMenu.fragment';
import SearchForm from './searchForm.fragment';

class Header extends Fragment {
  constructor() {
    super($('header'));
    this.menu = new MainMenu();
    this.searchForm = new SearchForm();
    this.elements.create = this.fragment.$('#createBtn');
    this.elements.login = this.fragment.$('.btn-login');
    this.elements.logo = this.fragment.$('.aarive-logo');
  }

  get create() {
    return this.elements.create;
  }

  displayed() {
    super.displayed();
    this.menu.displayed();
    this.searchForm.displayed();
  }
}

export default Header;
