# E2E Tests

These end-to-end tests are designed to be run against mock data to confirm that all parts of the client are operating correctly with one another.

These tests can be configured to run against variations of the mock data.

There are two ways to run the tests: against your local development environment, or against a remote deployment. The process for each varies slightly.

##Configuration and Mock data

In `client/src/test/e2e/config` there is a file called `test.constants.js`. This file contains the static data to be used when running tests. This base configuration can be overridden by adding a new file with the same naming scheme (there should also be a file called `anz.constants.js`, for example). In this new file, you can override the base constants as generally or specifically as necessary as needed.

To use these overrides, the `TEST_CONFIG` environment variable needs to be set before you run the test command:

`$ TEST_CONFIG=anz`

The value of the varaiable should be the first part of the constants file name. So if your file is named `mydata.constants.js`, your `TEST_CONFIG` variable should be set to `mydata`.

## Running all tests
### Local
First, we need to make sure the client is running. Open your console, navigate to the `client` folder, and run:

`$ npm run mock`

This will start the client with mock data. In a second console, again navigate to the `register` folder, and run:

`$ mvn tomcat:run`

This will allow for the landing page to be tested. Finally, in a third console, navigate to the `client` folder again, and run:

`$ TEST_CONFIG=anz npm run e2e`

This will run all tests against the currently running version of the client.

#### Notes

We are setting the `TEST_CONFIG` environment variable to `anz` since our mock data is Air New Zealand specific.

### Remote

First, we need to set the `AARIVE_URL` environment variable, then run the same test command we used for local:

`$ AARIVE_URL=http://aar-dev-mock.wirestone.net/ npm run e2e`

***The URL should always have a trailing slash***.

#### Notes
After setting the `AARIVE_URL`, you may want to run tests against your local environment again. If so, either close and re-open your console session, or unset the `AARIVE_URL` variable (`unset AARIVE_URL` in bash, `SET AARIVE_URL=` in Windows cmd).

## Running single or multiple (but not *all*) tests

To run one or more, but not *all* tests, we need to change our command slightly:

`$ npm run e2e -- --specs specFileName`

Notice the first `--` after e2e. This allows `npm` to forward any flags after it to `gulp`.

This will run a single spec against the local environment. Simply set the `AARIVE_URL` to run the single spec remotely.

Due to a [known issue with yargs](https://github.com/yargs/yargs/issues/264), to run multiple specs, we must use the `--specs` flag multiple times, like so:

`$ npm run e2e -- --specs specOne --specs specTwo --specs specThree`

### Check if `protractor` is installed globally

**Make sure `protractor` is not installed as a global npm module.** If it is, you will not be able to run the tests. The `postinstall` script will download the Selenium dependencies to your global version, rather than the local version. To check, run:

`$ npm list -g --depth=0 | grep protractor`

If you get a result from the above command, run:

`$ npm uninstall -g protractor`

This will remove the global version and allow you to run the tests locally without issue.