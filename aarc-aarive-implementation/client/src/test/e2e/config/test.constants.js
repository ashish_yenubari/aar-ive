module.exports = {
  STATUS_MESSAGES: {
    noResults: 'No results found'
  },

  MAIN_MENU: {
    options: [
      {
        label: 'Overview'
      },
      {
        label: 'Dashboard'
      }
    ]
  },

  LANDING: {
    url: 'http://localhost:5555/',
    buttons: {
      register: {
        label: 'Register',
        url: 'http://localhost:5555/register.html'
      },
      signin: {
        label: 'Sign in',
        url: `${browser.baseUrl}`
      }
    },
    text: 'The right part right now'
  },

  LOGIN: {
    validUser: 'asladmin@wsaarive.onmicrosoft.com',
    validPass: 'PASS@123',
    invalidUser: '1asladmin@wsaarive.onmicrosoft.com',
    invalidPass: '321@SSAP',
    errors: {}
  },

  OVERVIEW: {
    url: `${browser.baseUrl}`,
    nav: [
      {
        label: 'Pool',
        path: '',
        action: false
      },
      {
        label: 'AOG',
        path: '/aog',
        action: true
      },
      {
        label: 'Critical',
        path: '/critical',
        action: false
      },
      {
        label: 'Routine',
        path: '/routine',
        action: true
      },
      {
        label: 'MBK',
        path: '/mbk',
        action: false
      }
    ]
  },

  DASHBOARD: {
    url: `${browser.baseUrl}dashboard`,
    tabs: {
      headers: [
        'Date', 'Priority', 'PO #', 'Tail #', 'Part #', 'S/V Status', 'ESD',
        'U/S Status', 'E/M', 'Action'
      ],
      labels: [
        {
          label: 'AOG',
          index: 1
        },
        {
          label: 'Critical',
          index: 2
        },
        {
          label: 'Routine',
          index: 3
        },
        {
          label: 'MBK',
          index: 4
        },
        {
          label: 'Core',
          index: 5
        },
        {
          label: 'Action Required',
          index: 6
        }
      ]
    }
  },

  NEW_POOL_REQUEST: {
    url: `${browser.baseUrl}newPoolRequest`,
    labels: [
      '*A/C Tail#', '*Part#', 'Alternate Part#', 'ATA Chapter', 'Priority',
      'Contractual Delivery', 'Date required by', 'Time required by',
      'Installed Serial#', '*Customer PO-S5#', 'S/V Part Destination', 'Comments'
    ],
    fields: {
      part: {
        input: {
          valid: 'part',
          invalid: '4059'
        },
        validText: 'Fuel Metering Unit(FMU)-UTC Aerospace',
        invalidText: 'Cannot verify'
      }
    },
    modal: {
      text: 'Are you sure you want to cancel?',
      buttons: {
        leave: {
          label: 'Ok'
        },
        stay: {
          label: 'Cancel'
        }
      }
    }
  },

  SEARCH: {
    options: [
      {
        label: 'PO Number'
      },
      {
        label: 'Tail Number'
      },
      {
        label: 'Part Number'
      },
      {
        label: 'Serial Number'
      }
    ]
  },

  TABLE: {
    colors: {
      hover: 'rgba(4, 4, 4, 1)',
      click: 'rgba(119, 119, 119, 1)'
    }
  }
};
