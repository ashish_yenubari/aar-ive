const base = require('./test.constants.js');

base.OVERVIEW.nav = [
  {
    label: 'Pool',
    path: '',
    action: false
  },
  {
    label: 'AOG',
    path: '/aog',
    action: true
  },
  {
    label: 'CRI',
    path: '/critical',
    action: false
  },
  {
    label: 'SCH',
    path: '/routine',
    action: true
  },
  {
    label: 'RPL',
    path: '/rpl',
    action: false
  },
  {
    label: 'DPA',
    path: '/dpa',
    action: false
  }
];

base.DASHBOARD.tabs.labels = [
  {
    label: 'AOG',
    index: 1
  },
  {
    label: 'CRI',
    index: 2
  },
  {
    label: 'SCH',
    index: 3
  },
  {
    label: 'RPL',
    index: 4
  },
  {
    label: 'DPA',
    index: 5
  },
  {
    label: 'Core',
    index: 6
  },
  {
    label: 'Action Required',
    index: 7
  }
];

base.SEARCH.options = [
  {
    label: 'PO Number'
  },
  {
    label: 'RO Number'
  },
  {
    label: 'Tail Number'
  },
  {
    label: 'Part Number'
  },
  {
    label: 'Serial Number'
  },
  {
    label: 'Serial Number'
  },
  {
    label: 'Serial Number'
  }
];

module.exports = base;
