import Landing from './landing.po';
const LANDING = browser.constants.LANDING;

describe('AARive landing page, before logging in', () => {
  let page = new Landing();
  it('should open the application', () => {
    browser.ignoreSynchronization = true;
    page.open(LANDING.url);
    page.confirmUrl(LANDING.url);
  });
  it('should display the AAR logo in the upper left', () => {
    expect(page.topLogo.isDisplayed()).toBe(true);
  });
  it('should display the register button', () => {
    expect(page.register.isDisplayed()).toBe(true);
    expect(page.register.getAttribute('value')).toBe(LANDING.buttons.register.label);
  });
  it('should display the sign in button', () => {
    expect(page.signin.isDisplayed()).toBe(true);
    expect(page.signin.getAttribute('value')).toBe(LANDING.buttons.signin.label);
  });
  it('should display the AARive logo', () => {
    expect(page.appLogo.isDisplayed()).toBe(true);
  });
  it('should display the AARive slogan', () => {
    expect(page.landingText.isDisplayed()).toBe(true);
    expect(page.landingText.getText()).toBe(LANDING.text);
  });
  it('should let the user access the register page', () => {
    browser.ignoreSynchronization = true;
    page.register.click();
    page.confirmUrl(LANDING.buttons.register.url);
  });
  it('should return to the landing page', () => {
    page.back();
    page.confirmUrl(LANDING.url);
  });
});
