import BasePage from '../base/base.po';

class Landing extends BasePage {
  constructor() {
    super();
    let main = $('.logo-class');
    this.topLogo = $('.aarLogo-landing');
    this.appLogo = main.$('img');
    this.landingText = main.$('span');
    this.register = $('input.btn-register');
    this.signin = $('input.btn-signIn');
  }
}

export default Landing;
