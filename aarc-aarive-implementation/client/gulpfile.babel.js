'use strict';

import gulp from "gulp";
import gulpIf from "gulp-if";
import sass from "gulp-sass";
import sassGlob from "gulp-sass-glob";
import sourcemaps from "gulp-sourcemaps";
import uglify from "gulp-uglify";
import zip from "gulp-zip";
import inject from "gulp-inject";
import clean from "gulp-clean";
import util from "gulp-util";
import path from "path";
import express from "express";
import morgan from "morgan";
import minimatch from "minimatch";
import http from "http";
import httpProxy from "http-proxy";
import tinyLiveReload from "tiny-lr";
import connectLiveReload from "connect-livereload";
import eventStream from "event-stream";
import yargs from "yargs";
import eslint from "gulp-eslint";
import stylelint from "gulp-stylelint";
import concat from "gulp-concat";
import templateCache from "gulp-angular-templatecache";
import gulpNgConfig from "gulp-ng-config";
import gulpMergeJson from "gulp-merge-json";
import gulpJsonEditor from "gulp-json-editor";
import runSequence from "run-sequence";
import protractor from "gulp-protractor";
import karma from "karma";

const argv = yargs.argv;
const liveReload = tinyLiveReload();

const project = require('./package.json');

function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

function proxy(logPrefix, options, proxyPort) {
  let httpServer = http.createServer((req, res) => {
    let option = options.find((option) => {
      return (minimatch(req.url, option.pattern));
    });

    if (option) {
      option.proxy.web(req, res);
    } else {
      util.log(logPrefix, 'option is null or undefined!');
    }
  });

  httpServer.on('error', (err, req, res) => {
    res.status(503).end();
  });

  util.log(logPrefix, 'Proxy listening on port', util.colors.green(proxyPort));

  httpServer.listen(proxyPort);
}

function proxyOptions(expressPort) {
  return [
    {
      proxy: httpProxy.createProxyServer({
        target: argv['api-url'] || process.env.GULP_API_URL || 'http://localhost:8080/'
      }),
      pattern: "/api/**"
    },
    {
      proxy: httpProxy.createProxyServer({
        target: argv['express-url'] || process.env.GULP_EXPRESS_URL || 'http://localhost:' + expressPort + '/'
      }),
      pattern: "**"
    }
  ];
}

function sendStream(res, stream) {
  return stream.pipe(eventStream.map((file, callback) => {
    let contentType = express.static.mime.lookup(file.path);
    let charset = express.static.mime.charsets.lookup(contentType);

    res.set('Content-Type', contentType + (charset ? '; charset=' + charset : ''));

    return callback(null, file.contents);
  })).pipe(res);
}

function expressIndex() {
  const enableMockData = argv['enable-mock-data'] || process.env.GULP_ENABLE_MOCK_DATA !== undefined;
  const files = [
    'target/gulp/app/js/vendor.js',
    'target/gulp/app/js/app.js',
    'target/gulp/app/js/constants.js',
    'target/gulp/app/js/templates.js',
    'target/gulp/app/css/style.css'
  ];
  if (enableMockData) {
    files.push('target/gulp/app/js/mock.js');
  }

  return (req, res) => {
    sendStream(res, gulp.src('src/app/index.html')
      .pipe(inject(gulp.src(files), {
        ignorePath: 'target/gulp/app'
      })));
  };
}

function expressStatic(path) {
  return (req, res, next) => {
    let staticServer = express.static(path);

    return staticServer(req, res, () => {
      res.status(404).send('Not Found');
    });
  };
}

function compileVendor() {
  const enableSourceMaps = argv['enable-source-maps'] || process.env.GULP_ENABLE_SOURCE_MAPS !== undefined;
  const disableUglify = argv['disable-uglify'] || process.env.GULP_DISABLE_UGLIFY !== undefined;

  return gulp.src(
    [
      require.resolve('jquery/dist/jquery.js'),
      require.resolve('angular/angular.js'),
      require.resolve('angular-animate/angular-animate.js'),
      require.resolve('angular-cookies/angular-cookies.js'),
      require.resolve('angular-ui-router/release/angular-ui-router.js'),
      require.resolve('jquery.caret/dist/jquery.caret.js'),
      require.resolve('angular-fixed-table-header/src/fixed-table-header.js'),
      require.resolve('angularjs-datepicker/dist/angular-datepicker.min.js'),
      require.resolve('moment/min/moment.min.js'),
      require.resolve('angular-sanitize/angular-sanitize.min.js'),
      require.resolve('angular-auto-complete/angular-auto-complete.js'),
      require.resolve('downloadjs/download.js'),
      require.resolve('angular-acl/angular-acl.js')
    ])
    .pipe(gulpIf(enableSourceMaps, sourcemaps.init()))
    .pipe(gulpIf(!disableUglify, uglify({
      mangle: false
    })))
    .on('error', handleError)
    .pipe(concat('vendor.js'))
    .pipe(gulpIf(enableSourceMaps, sourcemaps.write()))
    .pipe(gulp.dest('target/gulp/app/js'));
}

function compileJs() {
  const enableSourceMaps = argv['enable-source-maps'] || process.env.GULP_ENABLE_SOURCE_MAPS !== undefined;
  const disableUglify = argv['disable-uglify'] || process.env.GULP_DISABLE_UGLIFY !== undefined;

  return gulp.src(
    [
      'src/app/*.js',
      'src/app/config/**/*.js',
      'src/app/**/*.js'
    ])
    .pipe(gulpIf(enableSourceMaps, sourcemaps.init()))
    .pipe(gulpIf(!disableUglify, uglify({
      mangle: false
    })))
    .on('error', handleError)
    .pipe(concat('app.js'))
    .pipe(gulpIf(enableSourceMaps, sourcemaps.write()))
    .pipe(gulp.dest('target/gulp/app/js'));
}

function compileMock() {
  const enableSourceMaps = argv['enable-source-maps'] || process.env.GULP_ENABLE_SOURCE_MAPS !== undefined;
  const disableUglify = argv['disable-uglify'] || process.env.GULP_DISABLE_UGLIFY !== undefined;

  return gulp.src([
    require.resolve('angular-mocks/angular-mocks.js'),
    'src/mock/*.js',
    'src/mock/**/*.js'
  ])
  .pipe(gulpIf(enableSourceMaps, sourcemaps.init()))
  .pipe(gulpIf(!disableUglify, uglify({
    mangle: false
  })))
  .on('error', handleError)
  .pipe(concat('mock.js'))
  .pipe(gulpIf(enableSourceMaps, sourcemaps.write()))
  .pipe(gulp.dest('target/gulp/app/js'));
}

function compileTemplates() {
  return gulp.src('src/app/components/**/*.html')
    .pipe(templateCache({ module: 'app.templates', standalone: true, base: '' }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest('target/gulp/app/js'));
}

function compileConstants() {
  let endpointUrl = argv['endpoint-url'] || process.env.GULP_ENDPOINT_URL;
  let deployEnv = argv['deploy-env'] || '';

  return gulp.src('src/app/config/*.json')
    .pipe(gulpMergeJson('app.constants.json'))
    .pipe(gulpJsonEditor(function(json) {
      if (endpointUrl) {
        json.defaultConstant.ENDPOINT_URL = endpointUrl;
      }
      if (deployEnv.toUpperCase() === 'DEV') {
        json.defaultConstant = Object.assign(json.defaultConstant, json.devDefaultConstant);
      } else if (deployEnv.toUpperCase() === 'STAGE') {
        json.defaultConstant = Object.assign(json.defaultConstant, json.stageDefaultConstant);
      } else if (deployEnv.toUpperCase() === 'QA') {
        json.defaultConstant = Object.assign(json.defaultConstant, json.qaDefaultConstant);
      } else if (deployEnv.toUpperCase() === 'RELEASE') {
        json.defaultConstant = Object.assign(json.defaultConstant, json.releaseDefaultConstant);
      } else if (deployEnv.toUpperCase() === 'PROD') {
        json.defaultConstant = Object.assign(json.defaultConstant, json.prodDefaultConstant);
      } else if (deployEnv.toUpperCase() === 'LOCAL') {
        json.defaultConstant = Object.assign(json.defaultConstant, json.localDefaultConstant);
      } else {
        // Throw error if nothing is specified or invalid argument is specified.
        throw String(util.colors.red('Invalid or missing argument for --deploy-env='));
      }
      // util.log('Finished', util.colors.green('creating constants'));
      return { defaultConstant: json.defaultConstant };
    }))
    .pipe(gulpNgConfig('app.constants'))
    .pipe(concat('constants.js'))
    .pipe(gulp.dest('target/gulp/app/js'));
}

function compileScss(errLogToConsole) {
  const enableSourceMaps = argv['enable-source-maps'] || process.env.GULP_ENABLE_SOURCE_MAPS !== undefined;

  return gulp
    .src('src/app/app.scss')
    .pipe(sassGlob())
    .pipe(gulpIf(enableSourceMaps, sourcemaps.init()))
    .pipe(sass({
      errLogToConsole: errLogToConsole,
      outputStyle: argv['sass-output-style'] || process.env.GULP_SASS_OUTPUT_STYLE || 'compressed',
      includePaths: [
        'node_modules/bootstrap-sass/assets/stylesheets',
        'node_modules/font-awesome/scss',
        'node_modules/angularjs-datepicker/src/css',
        'node_modules',
        'src/app'
      ],
      sourceMap: '' // Required to prevent gulp-sass from crashing.
    }))
    .on('error', handleError)
    .pipe(concat('style.css'))
    .pipe(gulpIf(enableSourceMaps, sourcemaps.write()))
    .pipe(gulp.dest('target/gulp/app/css'));
}

// ************************************ //
// ************** TASKS *************** //
// ************************************ //

gulp.task('karma', function(done) {
  new karma.Server({
    configFile: path.resolve(__dirname, 'karma.conf.js'),
    singleRun: true
  }, done()).start();
});

gulp.task('karma:watch', function(done) {
  new karma.Server({
    configFile: path.resolve(__dirname, 'karma.conf.js'),
    singleRun: false,
    autoWatch: true
  }, done()).start();
});

gulp.task('test', function() {
  runSequence('compile-vendor', 'compile-constants', 'karma');
});

gulp.task('test-watch', function() {
  runSequence('compile-vendor', 'compile-constants', 'karma:watch');
});

gulp.task('protractor-run', () => {
  if (argv.specs && typeof(argv.specs) === 'string') { // is a single spec defined
    argv.specs = [argv.specs];
  } else if (!argv.specs) { // if there is no spec, define wildcard
    argv.specs = ['*'];
  }
  let specFiles = argv.specs.map((spec) => {
    return `./src/test/e2e/**/${spec}.spec.js`; // build spec paths
  });
  gulp.src(specFiles)
    .pipe(protractor.protractor({
      configFile: './src/test/e2e/protractor.conf.js'
    }))
    .on('error', handleError);
});

gulp.task('compile-font', () => {
  return gulp.src([
    'node_modules/bootstrap-sass/assets/fonts/**/**.*',
    'node_modules/font-awesome/fonts/**.*',
    'src/app/fonts/**/**.*'
  ])
  .pipe(gulp.dest('target/gulp/app/fonts'));
});

gulp.task('compile-js', () => {
  return compileJs();
});

gulp.task('compile-vendor', () => {
  return compileVendor();
});

gulp.task('compile-mock', () => {
  return compileMock();
});

gulp.task('compile-templates', () => {
  return compileTemplates();
});

gulp.task('compile-constants', () => {
  return compileConstants();
});

gulp.task('watch-js', ['compile-js', 'compile-vendor', 'compile-mock'], () => {
  let logPrefix = '[' + util.colors.blue('watch-js') + ']';

  gulp.watch('src/app/**/*.js', () => {
    util.log(logPrefix, 'Recompiling app JS');
    compileJs();
  });

  gulp.watch('src/mock/**/*.js', () => {
    util.log(logPrefix, 'Recompiling mock JS');
    compileMock();
  });

  gulp.watch('target/gulp/app/js/*.js', (event) => {
    util.log(logPrefix, 'Reloading', path.relative('target/gulp/app/js', event.path));

    liveReload.changed({
      body: {
        files: [
          path.relative('target/gulp/app', event.path)
        ]
      }
    });
  });
});

gulp.task('compile-scss', () => {
  return compileScss(false);
});

gulp.task('watch-scss', ['compile-scss'], () => {
  let logPrefix = '[' + util.colors.blue('watch-scss') + ']';

  gulp.watch('src/app/**/*.scss', () => {
    util.log(logPrefix, 'Recompiling SCSS');

    compileScss(true);
  });

  gulp.watch('target/gulp/app/css/*.css', (event) => {
    util.log(logPrefix, 'Reloading', path.relative('target/gulp/app/css', event.path));

    liveReload.changed({
      body: {
        files: [
          path.relative('target/gulp/app', event.path)
        ]
      }
    });
  });
});

gulp.task('watch-html', ['compile-templates'], () => {
  let logPrefix = '[' + util.colors.blue('watch-html') + ']';

  gulp.watch(['src/app/**/*.html'], (event) => {
    util.log(logPrefix, 'Reloading', path.relative('src/app', event.path));
    return compileTemplates();
  });
});

gulp.task('zip', () => {
  runSequence('clean', [
    'compile-font',
    'compile-js',
    'compile-vendor',
    'compile-mock',
    'compile-templates',
    'compile-scss',
    'compile-constants'
  ], zipBuild);
});

function zipBuild() {
  let buildNumber = argv['build-number'] || process.env.GULP_BUILD_NUMBER;
  let filename = argv['zip-filename'] ||
    process.env.GULP_ZIP_FILENAME ||
    project.name + '-' + project.version + (buildNumber !== undefined ? '+build.' + buildNumber : '') + '.zip';
  const enableMockData = argv['enable-mock-data'] || process.env.GULP_ENABLE_MOCK_DATA !== undefined;
  const files = [
    'target/gulp/app/js/vendor.js',
    'target/gulp/app/js/app.js',
    'target/gulp/app/js/constants.js',
    'target/gulp/app/js/templates.js',
    'target/gulp/app/css/style.css'
  ];
  if (enableMockData) {
    files.push('target/gulp/app/js/mock.js');
  }

  util.log('Creating', util.colors.magenta(filename));

  return eventStream.merge(
    gulp.src([
      'src/app/img/**'
    ], {base: 'src/app'}),

    gulp.src('target/gulp/app/**'),

    gulp.src('src/app/index.html')
      .pipe(inject(gulp.src(files), {
        addSuffix: '?v=' + Math.round(new Date().getTime() / 1000),
        ignorePath: 'target/gulp/app'
      })))
    .pipe(zip(filename))
    .pipe(gulp.dest('target'));
}

gulp.task('run', ['compile-font', 'compile-constants', 'lint', 'watch-html', 'watch-js', 'watch-scss'], () => {
  let logPrefix = '[' + util.colors.blue('run') + ']';

  let proxyPort = argv['proxy-port'] || process.env.GULP_PROXY_PORT || 4444;
  let expressPort = argv['express-port'] || process.env.GULP_EXPRESS_PORT || 7777;
  let liveReloadPort = argv['live-reload-port'] || process.env.GULP_LIVE_RELOAD_PORT || 35729;

  liveReload.listen(liveReloadPort);

  proxy(logPrefix, proxyOptions(expressPort), proxyPort);
  let allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', 'example.com');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
  };
  return express()
    .use(morgan('combined'))
    .use('/css', expressStatic('target/gulp/app/css'))
    .use('/fonts', expressStatic('target/gulp/app/fonts'))
    .use('/html', expressStatic('src/app/'))
    .use('/img', expressStatic('src/app/img'))
    .use('/js', expressStatic('target/gulp/app/js'))
    .use(connectLiveReload({
      port: liveReloadPort
    }))
    .use(allowCrossDomain)
    .use(expressIndex())
    .listen(expressPort, function() {
      util.log(logPrefix, 'Express listening on port', util.colors.green(expressPort));
    });
});

gulp.task('clean', () => {
  return gulp.src('target/gulp', {read: false})
    .pipe(clean());
});

gulp.task('lint-js', () => {
  return gulp.src(['src/app/**/*.js', 'src/mock/**/*.js', 'src/test/**/*.js'])
    .pipe(eslint())
    .pipe(eslint.format());
});

gulp.task('lint-css', () => {
  return gulp.src(['src/app/**/*.scss'])
    .pipe(stylelint({
      failAfterError: false,
      reporters: [
        { formatter: 'string', console: true }
      ]
    }));
});

gulp.task('lint', ['lint-js', 'lint-css']);

gulp.task('default', ['zip']);
